<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Payments extends Admin_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payments_model');
    }

    /* In case if user go only on /payments*/
    public function index()
    {
        $this->list_payments();
    }
    public function delete_bank($nam)
    {
        
        $CI = &get_instance();
        $CI->db->delete('btlbank',array('id' => $nam));
        redirect('/admin/payments/bank', 'refresh');
    } 
    public function delete_infor($nam)
    {
        
        $CI = &get_instance();
        $CI->db->delete('tbl_infor',array('id' => $nam));
        redirect('/admin/payments/infor', 'refresh');
    }
    public function bank(){


        if($this->input->post()){

        if($_POST['add']){
          
              $name_bank = $_POST['name_bank'];  
         $number_id = $_POST['number_id'];  
         $branch = $_POST['branch'];  
        

        $CI = &get_instance();
        $CI->db->insert('btlbank', [
                    
                    'name_bank' => $name_bank,
                    'number_id'              => $number_id,
                    'branch'             => $branch,
                   
                ]);  
        }  
        if($_POST['update']){
            
            $name_bank = $_POST['name_bank'];  
         $number_id = $_POST['number_id'];  
         $branch = $_POST['branch'];  
        $id = $_POST['id'];

        $CI = &get_instance();
       $this->db->where('id',$id);
       $this->db->update('btlbank', [
                    
                    'name_bank' => $name_bank,
                    'number_id'              => $number_id,
                    'branch'             => $branch,
                   
                ]);  
        }
     
        }
         $this->load->view('admin/payments/bank', $data);
        }
    
    /* List all invoice paments */
    public function infor(){
        if($this->input->post()){

        if($_POST['add']){
          
         $company = $_POST['company'];  
         $address = $_POST['address'];  
         $email = $_POST['email'];  
         $tax_code = $_POST['tax_code'];  
         $phone_number = $_POST['phone_number'];  
        

        $CI = &get_instance();
        $CI->db->insert('tbl_infor', [
                    
                    'company' => $company,
                    'address'              => $address,
                    'email'             => $email,
                    'phone_number'             => $phone_number,
                    'tax_code'             => $tax_code,
                   
                ]);  
        }  
        if($_POST['update']){
            
          $company = $_POST['company'];  
         $address = $_POST['address'];  
         $email = $_POST['email'];  
         $tax_code = $_POST['tax_code'];  
         $phone_number = $_POST['phone_number'];   
        $id = $_POST['id'];

        $CI = &get_instance();
       $this->db->where('id',$id);
       $this->db->update('tbl_infor', [
                    
                    'company' => $company,
                    'address'              => $address,
                    'email'             => $email,
                    'phone_number'             => $phone_number,
                    'tax_code'             => $tax_code,
                   
                ]);  
        }
     
        }
     $this->load->view('admin/payments/infor', $data);   
    }
    public function list_payments()
    {
        if (!has_permission('payments', '', 'view') && !has_permission('invoices', '', 'view_own') && get_option('allow_staff_view_invoices_assigned') == '0') {
            access_denied('payments');
        }

        $data['title'] = _l('payments');
        $this->load->view('admin/payments/manage', $data);
    }

    public function table($clientid = '')
    {
        if (!has_permission('payments', '', 'view') && !has_permission('invoices', '', 'view_own') && get_option('allow_staff_view_invoices_assigned') == '0') {
            ajax_access_denied();
        }

        $this->app->get_table_data('payments', [
            'clientid' => $clientid,
        ]);
    }

    /* Update payment data */
    public function payment($id = '')
    {
        if (!has_permission('payments', '', 'view') && !has_permission('invoices', '', 'view_own') && get_option('allow_staff_view_invoices_assigned') == '0') {
            access_denied('payments');
        }

        if (!$id) {
            redirect(admin_url('payments'));
        }
        if ($this->input->post()) {
            $a =$_POST['amount'];
            $string = str_replace(',', '', $a);
            $b = (float)$string;
            $_POST['amount']=$b;
            if (!has_permission('payments', '', 'edit')) {
                access_denied('Update Payment');
            }
            $success = $this->payments_model->update($this->input->post(), $id);
            if ($success) {
                set_alert('success', _l('updated_successfully', _l('payment')));
            }
            redirect(admin_url('payments/payment/' . $id));
        }
        $data['payment'] = $this->payments_model->get($id);
        if (!$data['payment']) {
            blank_page(_l('payment_not_exists'));
        }
        $this->load->model('invoices_model');
        $data['invoice'] = $this->invoices_model->get($data['payment']->invoiceid);
        $this->load->model('payment_modes_model');
        $data['payment_modes'] = $this->payment_modes_model->get('', [], true, true);
        $i                     = 0;
        foreach ($data['payment_modes'] as $mode) {
            if ($mode['active'] == 0 && $data['payment']->paymentmode != $mode['id']) {
                unset($data['payment_modes'][$i]);
            }
            $i++;
        }
        $data['title'] = _l('payment_receipt') . ' - ' . format_invoice_number($data['payment']->invoiceid);
        $this->load->view('admin/payments/payment', $data);
    }

    /**
     * Generate payment pdf
     * @since  Version 1.0.1
     * @param  mixed $id Payment id
     */
    public function pdf($id)
    {
        if (!has_permission('payments', '', 'view') && !has_permission('invoices', '', 'view_own') && get_option('allow_staff_view_invoices_assigned') == '0') {
            access_denied('View Payment');
        }
        $payment = $this->payments_model->get($id);

        if (!has_permission('payments', '', 'view') && !has_permission('invoices', '', 'view_own') && !user_can_view_invoice($payment->invoiceid)) {
            access_denied('View Payment');
        }

        $this->load->model('invoices_model');
        $payment->invoice_data = $this->invoices_model->get($payment->invoiceid);

        try {
            $paymentpdf = payment_pdf($payment);
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            if (strpos($message, 'Unable to get the size of the image') !== false) {
                show_pdf_unable_to_get_image_size_error();
            }
            die;
        }

        $type = 'D';

        if ($this->input->get('output_type')) {
            $type = $this->input->get('output_type');
        }

        if ($this->input->get('print')) {
            $type = 'I';
        }

        $paymentpdf->Output(mb_strtoupper(slug_it(_l('payment') . '-' . $payment->paymentid)) . '.pdf', $type);
    }

    /* Delete payment */
    public function delete($id)
    {
        if (!has_permission('payments', '', 'delete')) {
            access_denied('Delete Payment');
        }
        if (!$id) {
            redirect(admin_url('payments'));
        }
        $response = $this->payments_model->delete($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('payment')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('payment_lowercase')));
        }
        redirect(admin_url('payments'));
    }
}
