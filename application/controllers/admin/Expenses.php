<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Expenses extends Admin_controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('expenses_model');
		$this->load->model('invoices_model');
		$this->load->model('Bytesoft_notify_model');
		$this->load->model('Cron_model');
	}

	public function index($id = '') {
		$this->list_expenses($id);
	}
	public function check_sms() {
		$this->Cron_model->run();
	}
	public function check_hdthue(){
		$this->load->view('admin/expenses/test');
	}
	function get_expire_contract()
	{
		$CI = &get_instance();
		$where_own = array();
		$today = date('Y-m-d');
// $today = "2019-03-05";
		$plus_7_days = date('Y-m-d', strtotime("+30 days"));
// $plus_7_days = "2019-03-16";
		$total=total_rows('tblcontracts','dateend BETWEEN "'.$today.'" AND "'.$plus_7_days.'" AND trash=0 AND dateend is NOT NULL');
		$contract = $CI->db->query('SELECT * FROM tblcontracts WHERE dateend BETWEEN "'.$today.'" AND "'.$plus_7_days.'" AND trash=0 AND dateend is NOT NULL')->result_array();
$staff=$CI->db->query('SELECT * FROM tblstaff WHERE  role=2')->result_array();
		if(sizeof($contract)>0){
			foreach ($contract as $key => $value) {
				$add= add_notification([
					'description' => 'contract_expired',
					'fromcompany' => 1,
					'fromuserid' => null,
					'touserid'=>$value['addedfrom'],
					'link' => 'contracts/contract/' . $value['id'],
					'additional_data' => serialize([
						'<b>' .$value['subject']. '</b>',
					]),
				]);
				if($value['sale_agent']!==0 && $value['sale_agent']!==$value['addedfrom']){
					$add= add_notification([
						'description' => 'contract_expired',
						'fromcompany' => 1,
						'fromuserid' => null,
						'touserid'=>$value['sale_agent'],
						'link' => 'contracts/contract/' . $value['id'],
						'additional_data' => serialize([
							'<b>' .$value['subject']. '</b>',
						]),
					]);

				}
					if(sizeof($staff)>0){
					
					foreach ($staff as $key => $value2) {
					
						$add= add_notification([
						'description' => 'contract_expired',
						'fromcompany' => 1,
						'fromuserid' => null,
						'touserid'=>$value2['staffid'],
						'link' => 'contracts/contract/' . $value['id'],
						'additional_data' => serialize([
							'<b>' .$value['subject']. '</b>',
						]),
					]);
					}
				}
			}
		}


		return $total;
	}
		function get_expire_invoice()
	{
		$CI = &get_instance();
		$where_own = array();
		$today = date('Y-m-d');
// $today = "2019-03-05";
		$plus_7_days = date('Y-m-d', strtotime("+3 days"));
// $plus_7_days = "2019-03-16";
		$total=total_rows('tblinvoices','date BETWEEN "'.$today.'" AND "'.$plus_7_days.'"');
		$contract = $CI->db->query('SELECT * FROM tblinvoices WHERE date BETWEEN "'.$today.'" AND "'.$plus_7_days.'"')->result_array();
$staff=$CI->db->query('SELECT * FROM tblstaff WHERE  role=3')->result_array();
		if(sizeof($contract)>0){
			foreach ($contract as $key => $value) {
				
					if(sizeof($staff)>0){
					
					foreach ($staff as $key => $value2) {
					
						$add= add_notification([
						'description' => 'Đến kỳ thanh toán Đề nghị thanh toán '.$value['title_payment'],
						'fromcompany' => 1,
						'fromuserid' => null,
						'touserid'=>$value2['staffid'],
						'link' => 'invoices/list_invoices/' . $value['id'],
						'additional_data' => serialize([
							'<b>' .$value['title_payment']. '</b>',
						]),
					]);
					}
				}
			}
		}


		return $total;
	}
	public function check_sv(){

		$date = date('Y-m-d');

		$data = $this->db->where('dateend >',$date)->get('tblcontracts')->result_array();
		$t =0;

		foreach ($data as $value) {
			print_r($value['sale_agent']);
			echo '</br>';
			$id = $value['id'];
			$floor = $value['floor'];
			$ngay = new DateTime($value['datestart']);
			$ngay1 = new DateTime($value['dateend']);
			$interval = date_diff($ngay, $ngay1);
			$date_y = $interval->format('%R%Y');
			$date_m = $interval->format('%R%m');
			$month = ($date_y * 12) + $date_m;
			$curr = $value['recurring'];
			$lan = ceil($month / $curr);
			$add = get_staff_user_id();
			$invoice_data = [
				'clientid' => $value['client'],
				'sale_agent' => $value['sale_agent'],
				'datecreated' => $value['datestart'],
				'date' => $value['datestart'],
				'currency' => '1',
				'name_building' => $floor,
				'recurring' => $value['recurring'],
				'subtotal' => 0,
				'total' => 0,
				'total_tax' => 0,
			];
			$cycles = $lan;
			for ($i=0; $i <$lan ; $i++) {
				$cycles--;
				$next_invoice_number = get_option('next_invoice_number');
				$ko = Date("Y-m-d", strtotime($value['datestart'] . "+" . $curr*$i . " Month"));
				$koo = Date("Y-m-d", strtotime($value['datestart'] . "+" . $curr*($i+1) . " Month"));
				$ko1 = Date("Y-m-d", strtotime($value['dateend'] . "+ 3 day"));
				$nko= new DateTime($ko);
				$nko1= new DateTime($ko1);
				$month_new = date('m',strtotime($value['datestart']))+$curr*$i;
				$month_new_koo = date('m',strtotime($value['datestart']))+$curr*($i+1);

				if($month_new>12&&$month_new<24){
					$month_new = $month_new -12;
				}
				else if($month_new>24&&$month_new<35){
					$month_new = $month_new -24;
				}
				else if($month_new>36&&$month_new<48){
					$month_new = $month_new -36;
				}else if($month_new==24||$month_new==36||$month_new==48){
					$month_new = 12;
				}

				if($month_new_koo>12&&$month_new_koo<24){
					$month_new_koo = $month_new_koo -12;
				}
				else if($month_new_koo>24&&$month_new_koo<35){
					$month_new_koo = $month_new_koo -24;
				}
				else if($month_new_koo>36&&$month_new_koo<48){
					$month_new_koo = $month_new_koo -36;
				}else if($month_new_koo==24||$month_new_koo==36||$month_new_koo==48){
					$month_new_koo = 12;
				}
				$month_old = date('m',strtotime($ko));
				$day_old = date('d',strtotime($ko));
				$year_old = date('Y',strtotime($ko));
				$day_new = date('d',strtotime($ngay_start));
				$year_new = date('Y',strtotime($ko));
				$month_old_koo = date('m',strtotime($koo));
				$year_old_koo = date('Y',strtotime($koo));
				$day_old_koo = date('d',strtotime($koo));

				if(checkdate($month_new, $day_new, $year_old)){

					if(checkdate($month_new_koo, $day_new, $year_old_koo)){
						$date_old = $koo;
					}else{
						$date_old = '01-'.$month_old_koo.'-'.$year_old_koo;
						$date_old = Date("Y-m-d", strtotime($date_old . "- 1 day"));
					}
					$date_new = $ko;

				}else{
				$date_new = '01-'.$month_old.'-'.$year_old;

				$date_new = Date("Y-m-d", strtotime($date_new . "- 1 day"));
				if(checkdate($month_new_koo, $day_new, $year_old_koo)){
						$date_old = $koo;
					}else{
						$date_old = '01-'.$month_old_koo.'-'.$year_old_koo;
						$date_old = Date("Y-m-d", strtotime($date_old . "- 1 day"));
					}

				}
			$so_sanh = date_diff($nko, $nko1);
			$mm = $so_sanh->format('%R%m');
			$yy = $so_sanh->format('%R%Y');
			$con_dem = ($yy * 12) + $mm;
			if($con_dem<$_POST['recurring']){
				$date_old = $_POST['dateend'];
			}
			if($date_new<'2019-09-01'){
				$status=2;

			}else{
				$status=1;

			}
			$re=0;
			$ndv = $this->db->where('id',$_POST['group'])->get('tblitems')->result_array();
			$nd = $this->vn_to_str($ndv[0][description])."_TN";
			$item = count($_POST['name_dv']);
			$client = $value['client'];
			$recurring = $value['recurring'];
			$invoice_data['datecreated'] = date('Y-m-d');
				$invoice_data['date'] = $date_new;
				$invoice_data['duedate'] = $date_old;
				$invoice_data['invoi_tye'] = 1;
				$invoice_data['status'] = $status;
				$invoice_data['cycles'] = $cycles;
				$invoice_data['number'] = $next_invoice_number;
				$invoice_data['allowed_payment_modes'] = ["1", "2", "stripe"];
				$idt = $this->invoices_model->add($invoice_data);

			$thue_in = 0;
			$tien_in = 0;
			$item = $this->db->where('rel_id',$id)->get('tbl_contracts_item')->result_array();
			foreach ($item as $key ) {
				$long_description = $key['long_description'];
				$rate = $key['rate'];
				$group_id = $key['group_id'];
				$item = $key['item'];
				$description = $key['description'];
				if ($key['group_id']==$key['item']) {
					$rate = $key['rate'];
					if($con_dem<$value['recurring']){
								$qty = $con_dem;
					}else{
							$qty = $value['recurring'];
						}
				if($value['address_vp']){
					$thue_in = $thue_in + ($rate*$qty)*0.1;
				}
				$tien_in = $tien_in + ($rate*$qty);
				$CI = &get_instance();
						$CI->db->insert('tblitems_in', [
						'rel_id' => $idt,
						'rel_type' => "invoice",
						'description' => $description,
						'long_description' => $long_description,
						'qty' => $qty,
						'rate' => $rate,
						'group_id' => $group_id,
						'item' => $item,

					]);
				}
			}
			$bp = $this->db->where('rel_id',$idt)->get('tblitems_in')->result_array();
				if($value['address_vp']){
					foreach ($bp as $key) {
					if($key['item']>50&&$key['item']<1000){
						$k = $key['id'];
						$CI = &get_instance();
					$CI->db->insert('tblitemstax', [
						'rel_id' => $idt,
						'rel_type' => "invoice",
						'itemid' => $k,
						'taxrate' => 10,
						'taxname' => "VAT",

					]);
					}
				}
				}
				$time_invoi = $this->db->where('id',$idt)->get('tblinvoices')->result_array();

				$tong = $tien_in+$thue_in;
				if($time_invoi[0]['date']<date('Y-m-d')){
					$this->db->where('id',$idt);
					$this->db->update('tblinvoices', [
						'subtotal' => $tien_in,
						'total_tax' => $thue_in,
						'total' => $tong,
						'status'=>2,
					]);
				}else{
					$this->db->where('id',$idt);
					$this->db->update('tblinvoices', [
						'subtotal' => $tien_in,
						'total_tax' => $thue_in,
						'total' => $tong,
						'status'=>1,
					]);

			}
				}

		}
	}
	function get_expire_pile_contract()
	{
		$CI = &get_instance();
		$where_own = array();
		$today = date('Y-m-d');
// $today = "2019-03-05";
		$plus_7_days = date('Y-m-d', strtotime("+30 days"));
// $plus_7_days = "2019-03-16";
		$total=total_rows('btl_coc','han_coc BETWEEN "'.$today.'" AND "'.$plus_7_days.'" AND han_coc is NOT NULL');
		$contract = $CI->db->query('SELECT * FROM btl_coc WHERE han_coc BETWEEN "'.$today.'" AND "'.$plus_7_days.'" AND han_coc is NOT NULL')->result_array();
		$staff=$CI->db->query('SELECT * FROM tblstaff WHERE  role=2')->result_array();
	
		if(sizeof($contract)>0){
			foreach ($contract as $key => $value) {

				$add= add_notification([
					'description' => 'pile_contract_expired',
					'fromcompany' => 1,
					'fromuserid' => null,
					'touserid'=>$value['nguoi_ban'],
					'link' => 'expenses/tien_coc/' . $value['id_coc'],
					'additional_data' => serialize([
						'<b>' .$value['hopdong']. '</b>',
					]),
				]);
				if(sizeof($staff)>0){
					
					foreach ($staff as $key => $value2) {
						$add= add_notification([
					'description' => 'pile_contract_expired',
					'fromcompany' => 1,
					'fromuserid' => null,
					'touserid'=>$value2['staffid'],
					'link' => 'expenses/tien_coc/' . $value['id_coc'],
					'additional_data' => serialize([
						'<b>' .$value['hopdong']. '</b>',
					]),
				]);
					}
				}
			}
		}
		//kiem tra hop dong coc het han ngay hom truoc va doi trang thai

		$ex_contracts=$CI->db->query('SELECT * FROM btl_coc WHERE han_coc= "'.date('Y-m-d', strtotime("-1 days")).'" AND han_coc is NOT NULL AND trang_thai =1')->result_array();
		if(sizeof($ex_contracts)>0){
			foreach ($ex_contracts as $key => $value) {
					$this->db->where('id_coc',$value['id_coc']);
					$this->db->update('btl_coc', [
						'trang_thai' => 4,
						'date_update' => date('Y-m-d'),
					]);

			}
		}
		return $total;
	}
	public function word($id = '') {
		$data['data'] = $this->db->select('*')->from('tblcontracts')->join('tblclients','`tblcontracts`.`client`=`tblclients`.`userid`')->join('tblitems','`tblcontracts`.`name_building`= `tblitems`.`id`')->where('tblcontracts.id',$id)->get()->result_array();
		$data['client'] = $this->db->where('userid',$data['data'][0]['client'])->get('tblclients')->result_array();
		$data['item'] = $this->db->where('rel_id',$id)->get('tbl_contracts_item')->result_array();

		$this->load->view('admin/expenses/word', $data);
	}
	public function word_coc($id = '') {
		$data['data'] = $this->db->select('*')->from('btl_coc')->join('tblclients','`btl_coc`.`khach_hang`=`tblclients`.`userid`')->where('btl_coc.id_coc',$id)->get()->result_array();
		$data['item'] = $this->db->where('id_coc',$id)->get('btl_coc_item')->result_array();

		$this->load->view('admin/expenses/word_coc', $data);
	}
	public function bao_cao($id = '') {

		$this->load->view('admin/expenses/bao_cao', $data);
	}
	public function thanh_toan() {
		$this->load->model('Invoice_items_model');
		if($this->input->post()){
			$dem = count($_POST['id']);

			for ($i=0; $i <$dem ; $i++) {
				$id = $_POST['id'][$i];
				$thanh_toan = $_POST['thanh_toan'][$i];
				$data= $this->Invoice_items_model->get_thanh_toan_item($id);
				$tien = $data[0]['thanh_toan']+$thanh_toan;

				$this->db->where('id_item',$id);
				$this->db->update('tbl_item_hd', [
					'thanh_toan' => $tien,

				]);
			}
		}
		$data['builds']=$this->db->get('tblitems_groups')->result_array();
		$this->load->view('admin/expenses/thanh_toan', $data);
	}
	public function bao_cao_hd() {

		$this->load->view('admin/expenses/bao_cao_hd', $data);
	}
	public function test() {

		$date = date('Y-m-d');
$data = $this->db->where('dateend >=',$date)->get('tblcontracts')->result_array();
foreach ($data as $data1) {

   $description = $data1['description'];
      $client = $data1['client'];
      $contract_value = $data1['contract_value'];
      $name_building = $data1['name_building'];
      $deposit = $data1['deposit'];
      $thue = $data1['contract_value'];
      $recurring = $data1['recurring'];
      $datestart = $data1['datestart'];
      $dateend = $data1['dateend'];
      $address_vp = $data1['address_vp'];
      $sale_agent = $data1['sale_agent'];
      $floor = $data1['floor'];

      $cycle = $data1['tota'];
      $id = $data1['id'];
      $hom = date('Y-m-d');
      $ngay = new DateTime($data1['datestart']);
      $ngay1 = new DateTime($hom);
      $interval = date_diff($ngay, $ngay1);
      $date_y = $interval->format('%R%Y');
      $date_m = $interval->format('%R%m');
      $month = ($date_y * 12) + $date_m;

      $invoi_tye = 2;
      $cycle = 1;
      $data_item = $this->db->where('rel_id',$id)->get('tbl_contracts_item')->result_array();

      $number = get_option('next_invoice_number');
      foreach ($data_item as $data1_item) {
        $tien_invoi = 0;
        $ndv = $this->db->where('id',$data1_item['group_id'])->get('tblitems')->result_array();
        $nd = $this->vn_to_str($ndv[0][description])."_DV";

        if ($data1_item['item']>50&&$data1_item['item']<1000) {
          $number++;
          $id_it = $data1_item['item'];
          $invoice_data = [
				'clientid' => $client,
				'datecreated' => $date,
				'date' => $date,
				'currency' => '1',
				'recurring' => $recurring,
				'cycles' => 0,
				'subtotal' => 0,
				'total' => 0,
				'name_building' => $floor,
				'total_tax' => 0,
				'duedate' => $date,
				'number' => $number,
				'invoi_tye' => 2,
				'status' => 1,
				'title_payment' => $nd,
				'allowed_payment_modes' => ["1", "2", "stripe"],
			];
         $idt = $this->invoices_model->add($invoice_data);
        $this->db->where('id',$idt);
		$this->db->update('tblinvoices', [
			'status' => 1,
		]);
          $data_id =$this->db->order_by('id','desc')->limit(1)->get('tblinvoices')->result_array();
          $data1_id = $data_id[0]['id'];
          $data_invoice = $this->db->where('rel_id',$id)->get('tbl_contracts_item')->result_array();
          foreach ($data_invoice as $data1_invoice) {

           if($data1_invoice['item']<50||$data1_invoice['item']>1000){
        // print_r($data1_invoice['item']);
        $rel_id = $data1_id;
        $rel_type = "invoice";
        $description = $data1_invoice['description'];
        $long_description = $data1_invoice['long_description'];
        $date = date('Y-m-d');
        $rate = $data1_invoice['rate'];
        $group_id = $id_it;
        $item = $data1_invoice['item'];
        if($data1_invoice['item']<3){
           $qty = 0;
        }else{
           $qty = 1;
        }
        $tien_invoi = $tien_invoi + $data1_invoice['rate']*$qty;
        $CI = &get_instance();
		$CI->db->insert('tblitems_in', [
				'rel_id' => $rel_id,
				'rel_type' => $rel_type,
				'description' =>$description,
				'long_description' => $long_description,
				'qty' => $qty,
				'rate' => $rate,
				'group_id' => $group_id,
				'item' => $item,
				'date_in' => $date,
			]);

       }
          }


       $ii = $data1_id;

     		$this->db->where('id', $ii);
			$this->db->update('tblinvoices', [
				'subtotal' => $tien_invoi,
				'total' => $tien_invoi,
				'status' => 1,
			]);

    $ta = $this->db->where('rel_id',$ii)->get('tblitems_in')->result_array();
    if($ta){
    }else{
    $this->db->query("DELETE FROM `tblinvoices` WHERE `id` = $ii");
    }
        }



      }
}

$date_time = date('Y-m-d');
$time = getDate();
$thang = $time['mon']-1;
$nam = $time['year'];
if($thang==2){
  $thang_truoc = 12;
  $nam_truoc = $time['year']-1;
}elseif($thang==1){
 $thang_truoc = 11;
  $nam_truoc = $time['year']-1;
}
else{
  $thang_truoc = $thang-1;
  $nam_truoc = $time['year'];
}

$value= $this->db->where('nam',$nam)->where('thang',$thang)->get('tblmoney_sv')->result_array();

foreach ( $value as $value1) {
    $id_name_building=$value1['id_name_building'];


    $value_truoc= $this->db->where('nam',$nam_truoc)->where('thang',$thang_truoc)->where('id_name_building',$id_name_building)->get('tblmoney_sv')->result_array();
  foreach ( $value_truoc as $data_truoc) {
    $dien = $value1['dien']-$data_truoc['dien'];
    $nuoc = $value1['nuoc']-$data_truoc['nuoc'];
    $update_dien = $this->db->where('date_in',$date_time)->where('group_id',$id_name_building)->where('item',1)->update('tblitems_in', [
				'qty' => $dien,
			]);
    $update_nuoc = $this->db->where('date_in',$date_time)->where('group_id',$id_name_building)->where('item',2)->update('tblitems_in', [
				'qty' => $nuoc,
			]);
    $data_invoi= $this->db->where('group_id',$id_name_building)->where('date_in',$date_time)->limit(1)->get('tblitems_in')->result_array();
   echo '<pre>';
    $rel = $data_invoi[0]['rel_id'];
    print_r($rel);
    $data_data = $this->db->where('id',$rel)->get('tblinvoices')->result_array();

 $select_invoi_1= $this->db->where('date_in',$date_time)->where('group_id',$id_name_building)->where('rel_id',$rel)->get('tblitems_in')->result_array();
 $tien_thue =0;
 $tien_tong=0;
 foreach ($select_invoi_1 as $data_data_1) {
   $gro = $data_data_1['group_id'];
    $itm = $data_data_1['item'];

      if($gro==$itm){
        $tien_thue=$data_data_1['qty']*$data_data_1['rate']*0.1;
      }

    $tien_tong = $tien_tong+($data_data_1['qty']*$data_data_1['rate']);
 }
 $mo = $tien_tong+$tien_thue;
$up_voi= $this->db->where('id',$rel)->update('tblinvoices', [
				'subtotal' => $tien_tong,
				'total' => $mo,
				'status' => 1,
			]);

  }

}
	}
	private function vn_to_str($str)
    {

        $unicode = array(

            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

            'd' => 'đ',

            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

            'i' => 'í|ì|ỉ|ĩ|ị',

            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

            'y' => 'ý|ỳ|ỷ|ỹ|ỵ',

            'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'D' => 'Đ',

            'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'I' => 'Í|Ì|Ỉ|Ĩ|Ị',

            'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );

        foreach ($unicode as $nonUnicode => $uni) {

            $str = preg_replace("/($uni)/i", $nonUnicode, $str);

        }
//        $str = str_replace(' ', ' ', $str);

        return $str;

    }
	public function huy_coc_co($id) {
		$date = date('Y-m-d');
		$this->db->where('id_coc',$id);
		$this->db->update('btl_coc', [
				'trang_thai' => 2,
				'tien_tra' => 1,
				'date_update' => $date,
			]);
		$data = $this->db->where('id_coc',$id)->get('btl_coc')->result_array();
		// print_r($data);die;
		$next_invoice_number = get_option('next_invoice_number');
		$ttt = (int)-$data[0]['tien_coc'];
					$CI = &get_instance();
						$CI->db->insert('tblinvoices', [
							'clientid' => $data[0]['khach_hang'],
							'number' => $next_invoice_number,
							'datecreated' =>date('Y-m-d'),
							'date' => date('Y-m-d'),
							'duedate' => date('Y-m-d'),
							'currency' => 1,
							'subtotal' => $ttt,
							'total' => $ttt,
							'status' =>2,
							'invoi_tye' =>3,
						]);
		redirect('/admin/expenses/bao_cao_coc', 'refresh');
	}
	public function dang_ky($id = '') {
		$date = date('Y-m-d');
		$this->db->where('id_coc',$id);
		$this->db->update('btl_coc', [
			'trang_thai' => 1,
			'date_update' => $date,
		]);
	}
	public function hd_chuyen($id = '') {
		$this->load->model('contracts_model');
		$data['im'] = $this->db->where('id_coc',$id)->get('btl_coc_item')->result_array();
		$ma = $this->db->where('id_coc',$id)->get('btl_coc')->result_array();
		$data['contract']=$ma[0];
		$data['staff'] = $this->staff_model->get('', ['active' => 1]);
		$data['coc'] = $this->db->get('btl_coc')->result_array();
		$this->load->model('Invoices_model');
		$this->load->model('currencies_model');
		$this->load->model('Invoice_items_model');
		$this->load->model('Taxes_model');
		$data['invoi'] = $this->Invoice_items_model->get_item_vp();
		$data['dich_vu'] = $this->Invoice_items_model->get_dich_vu();
		$data['taxd'] = $this->Taxes_model->get();
		$data['types'] = $this->contracts_model->get_contract_types();
		$data['group'] = $this->Invoice_items_model->get_groups();
		$data['base_currency'] = $this->currencies_model->get_base_currency();


		$this->load->view('admin/expenses/hd_chuyen', $data);
	}
	public function huy_coc_khong($id) {
		$date = date('Y-m-d');
		$this->db->where('id_coc',$id);
		$this->db->update('btl_coc', [
			'trang_thai' => 3,
			'tien_tra' => 0,
			'date_update' => $date,
		]);
		redirect('/admin/expenses/bao_cao_coc', 'refresh');

	}
	public function coppy_hd($id='') {
		$data['id_contract'] = $id;
		$item = $this->db->where('rel_id',$id)->get('tbl_contracts_item')->result_array();
		$s=0;
		foreach ($item as $value) {
		$data['im'][$s]['namedv']=$value['description'];
		$data['im'][$s]['iditem']=$value['item'];
		$data['im'][$s]['rate']=$value['rate'];
		$s++;
		}
		$ma = $this->db->where('id',$id)->get('tblcontracts')->result_array();
		$data['contract']['ngay_coc']=$ma[0]['datestart'];
		$data['contract']['han_coc']=$ma[0]['dateend'];
		$data['contract']['id_building']=$ma[0]['floor'];
		$data['contract']['khach_hang']=$ma[0]['client'];
		$data['contract']['nguoi_ban']=$ma[0]['sale_agent'];
		// print_r($data['contract']);die;
		$cli = $this->db->where('userid',$ma[0]['client'])->get('tblclients')->result_array();
		$data['contract']['dia_chi']=$cli[0]['address'];
		$data['contract']['phone']=$cli[0]['phonenumber'];
		$data['contract']['cmnd']=$cli[0]['vat'];
		$this->load->model('Invoice_items_model');
		$this->load->model('Taxes_model');
		$data['check'] = $this->db->where('deposit',$id)->get('tblcontracts')->result_array();
		$data['staff'] = $this->staff_model->get('', ['active' => 1]);
		$data['dich_vu'] = $this->Invoice_items_model->get_dich_vu();
		$data['taxd'] = $this->Taxes_model->get();
		$data['data'] = $this->Invoice_items_model->get_data($id);
		$data['group'] = $this->Invoice_items_model->get_groups();
		$this->load->view('admin/expenses/tien_coc',$data);
	}
	public function bao_cao_coc() {

		$this->load->view('admin/expenses/bao_cao_coc', $data);
	}
	
	public function tien_coc( $id = '') {
		$data['id_contract']=$id;
		$data['error']="";
		if($id){
			$ma = $this->db->where('id_coc',$id)->get('btl_coc')->result_array();
			$data['contract']=$ma[0];
			$data['im'] = $this->db->where('id_coc',$id)->get('btl_coc_item')->result_array();
		}

		if($this->input->post()){
			$this->load->helper(array('form', 'url'));

			$this->load->library('form_validation');

		// $this->form_validation->set_rules('name_building', 'name_building', 'required');
			$this->form_validation->set_rules('start_date', 'start_date', 'required');
			$this->form_validation->set_rules('end_date', 'end_date', 'required');

 $data['validate'] = array(
        'name_building' => $_POST['name_building']
        
    );
			if ($this->form_validation->run() == FALSE)
			{
				// print_r("<pre/>");
				// print_r( $data);die;
				// $data=$this->input->post();
				 $this->load->view('admin/expenses/tien_coc','refresh');
				set_alert('danger', _l('added_failed', _l('service')));
				// redirect('/admin/expenses/tien_coc', $data);
			}
			else
			{
				$startDate = strtotime($_POST['start_date']);
				$endDate = strtotime($_POST['end_date']);

				if ($startDate > $endDate){
					$data['error']=_l('error_date');
				}else{
					$dem = count($_POST['name_dv']);
					$a =$_POST['tien_coc'];
					$string = str_replace(',', '', $a);
					$b = (float)$string;
					$floor=$_POST['floor'];
					$name_building=$_POST['name_building'];
					$group=$_POST['group'];
					$service=$_POST['service'];
					$address=$_POST['address'];
					$start_date=$_POST['start_date'];
					$end_date=$_POST['end_date'];
					$client=$_POST['client'];
					$dia_chi=$_POST['dia_chi'];
					$phone=$_POST['phone'];
					$cmnd=$_POST['cmnd'];
					$sale_agent=$_POST['sale_agent'];
					$hopdong=$_POST['hopdong'];
					$next_invoice_number = get_option('next_invoice_number');
					$dat = date('Y-m-d');

					if($id){
						$this->db->where('id_coc',$id);
						$this->db->update('btl_coc', [
							'id_building' => $floor,
							'tien_coc' => $b,
							'nguoi_ban' => $sale_agent,
							'ngay_coc' => $start_date,
							'khach_hang' => $client,
							'trang_thai' => 0,
							'id_hop_dong' => "",
							'adress' => $address,
							'dia_chi' => $dia_chi,
							'phone' => $phone,
							'cmnd' => $cmnd,
							'han_coc' => $end_date,
							'hopdong' => $hopdong,

						]);
						$maxid=$this->db->query("DELETE FROM `btl_coc_item` WHERE `btl_coc_item`.`id_coc` = $id");

						for ($i=0; $i <$dem ; $i++) {
							$name_dv=$_POST['name_dv'][$i];
							$a =$_POST['rate_dv'][$i];
							$string = str_replace(',', '', $a);
							$rate_dv = (float)$string;
							$id_item=$_POST['id_item'][$i];
							$CI = &get_instance();
							$CI->db->insert('btl_coc_item', [
								'id_coc' => $id,
								'rate' => abs($rate_dv),
								'qty' => 1,
								'iditem' => $id_item,
								'namedv' => $name_dv,
							]);
						}
						redirect('/admin/expenses/tien_coc/'.$id, 'refresh');
					}else{
						$CI = &get_instance();
						$CI->db->insert('tblinvoices', [
							'clientid' => $client,
							'number' => $next_invoice_number,
							'datecreated' =>$dat,
							'date' => $dat,
							'duedate' => $dat,
							'currency' => 1,
							'subtotal' => $b,
							'total' => $b,
							'status' =>1,
							'invoi_tye' =>3,
						]);
						$maxid_in=$this->db->query("SELECT MAX(id) FROM `tblinvoices`")->result_array();
						$idmax_in = $maxid_in[0]['MAX(id)'];
						// $CI = &get_instance();
						// $CI->db->insert('tblinvoicepaymentrecords', [
						// 	'invoiceid' => $idmax_in,
						// 	'amount' => $b,
						// 	'date' =>$dat,
						// 	'daterecorded' => $dat,
						// 	'bank_id' => 0,
						// 	'user_id' => $sale_agent,
						// ]);
						$CI = &get_instance();
						$CI->db->insert('btl_coc', [
							'id_building' => $floor,
							'tien_coc' => $b,
							'nguoi_ban' => $sale_agent,
							'ngay_coc' => $start_date,
							'khach_hang' => $client,
							'trang_thai' => 0,
							'id_hop_dong' => "",
							'adress' => $address,
							'dia_chi' => $dia_chi,
							'phone' => $phone,
							'cmnd' => $cmnd,
							'han_coc' => $end_date,
							'hopdong' => $hopdong,

						]);
						$maxid=$this->db->query("SELECT MAX(id_coc) FROM `btl_coc`")->result_array();
						$idmax = $maxid[0]['MAX(id_coc)'];
						if($_POST['id_contract']>0){
							$id_contract = $_POST['id_contract'];
							$up = $this->db->query("UPDATE `tblcontracts` SET `deposit` = $idmax WHERE `tblcontracts`.`id` = $id_contract ");
						}
						for ($i=0; $i <$dem ; $i++) {
							$name_dv=$_POST['name_dv'][$i];
							$string = str_replace(',', '', $_POST['rate_dv'][$i]);
							$rate_dv = (float)$string;
							$id_item=$_POST['id_item'][$i];
							$CI = &get_instance();
							$CI->db->insert('btl_coc_item', [
								'id_coc' => $idmax,
								'rate' => abs($rate_dv),
								'qty' => 1,
								'iditem' => $id_item,
								'namedv' => $name_dv,
							]);
							$CI = &get_instance();

						}
						$CI->db->insert('tblitems_in', [
			'rel_id' => $maxid_in[0]['MAX(id)'],
			'rel_type' => "invoice",
			'description' => "Tiền đặt cọc",
			'long_description' => "Tiền đặt cọc",
			'qty' => 1,
			'rate' => $b,
			'group_id' => $_POST['group'],
			'item' => 10,

		]);
						redirect('/admin/invoices/list_invoices#'.$maxid_in[0]['MAX(id)'], 'refresh');
					}
				}
			}

		}
		$this->load->model('Invoice_items_model');
		$this->load->model('Taxes_model');
		$data['check'] = $this->db->where('deposit',$id)->get('tblcontracts')->result_array();
		$data['staff'] = $this->staff_model->get('', ['active' => 1]);
		$data['dich_vu'] = $this->Invoice_items_model->get_dich_vu();
		$data['taxd'] = $this->Taxes_model->get();
		$data['data'] = $this->Invoice_items_model->get_data($id);
		$data['group'] = $this->Invoice_items_model->get_groups();
		$this->load->view('admin/expenses/tien_coc', $data);
	}
	public function money_sv($id = '') {
		$this->load->model('Invoice_items_model');
		// $data['items_groups'] = $this->Invoice_items_model->get_groups();
		$ids = get_staff_user_id();
		$data['items_groups'] = $this->Invoice_items_model->get_groups2($ids);
		$data['invoi'] = $this->Invoice_items_model->get_item_vp();
		if($this->input->post()){
			$id_name_building = $_POST['id_name_building'];
			$nam = $_POST['nam'];
			$thang = $_POST['thang'];

			$dem = count($_POST['id_name_building']);
			for ($t=0; $t <$dem ; $t++) {
				$dien = $_POST['dien'][$t];
				$nuoc = $_POST['nuoc'][$t];
				$id_name_building = $_POST['id_name_building'][$t];

				$this->db->where('nam',$nam);
				$this->db->where('thang',$thang);
				$this->db->where('id_name_building',$id_name_building);
				$dat = $this->db->get('tblmoney_sv')->result_array();
				if($dat){
					$id = $dat[0]['id'];

					$this->db->where('id',$id);
					$this->db->update('tblmoney_sv', [
						'dien' => $dien,
						'nuoc' => $nuoc,
					]);
				}else{

					$CI = &get_instance();
					$CI->db->insert('tblmoney_sv', [
						'id_name_building' => $id_name_building,
						'nam' => $nam,
						'thang' => $thang,
						'dien' => $dien,
						'nuoc' => $nuoc,

					]);
				}
			}

		}



		$this->load->view('admin/expenses/money_dich_vu', $data);
	}
	public function group_chi(){
		$data['data'] = $this->db->join('tblitems_groups', 'tblitems_groups.id = tbl_group_item.building', 'left')->get('tbl_group_item')->result_array();
		if ($this->input->post()) {
			if(isset($_POST['save-btn'])){
			$name_building = $_POST['name_building'];
			// $this->db->where('building', $name_building);
			// $this->db->delete('tbl_group_item');
			$dem = count($_POST['id_item']);
			for ($i=0; $i <$dem ; $i++) {
				$name_dv = $_POST['name_dv'][$i];
				$id_item = $_POST['id_item'][$i];
				$rate_dv = $_POST['rate_dv'][$i];
				$name_building = $_POST['name_building'];
				$CI = &get_instance();
				$check= $CI->db->query('SELECT COUNT(*) FROM tbl_group_item WHERE building= '.$name_building.' AND id_itemp ='.$id_item)->result_array();
				if($check[0]['COUNT(*)']==0){
					$CI->db->insert('tbl_group_item', [
					'building' => $name_building,
					'rate_itemp' => $rate_dv,
					'name_itemp' => $name_dv,
					'id_itemp' => $id_item,
				]);
				}
				
			}
			redirect('/admin/expenses/group_chi', 'refresh');
		}
	}
		if(isset($_GET['search-btn'])){
			$build=$_GET['name_building2'];
			$data['data'] = $this->db->join('tblitems_groups', 'tblitems_groups.id = tbl_group_item.building', 'left')->where('id',$build)->get('tbl_group_item')->result_array();
		}
			
		
		$this->load->model('Invoice_items_model');
		$data['items_groups'] = $this->Invoice_items_model->get_groups();
		$data['ims'] = $this->db->get('tlbdich_vu_chi')->result_array();
		$this->load->view('admin/expenses/group_chi', $data);
		
	}
	public function dich_vu() {
		$data['dich_vu'] = $this->db->get('tlbdich_vu')->result_array();

		if ($this->input->post()) {

			$description = $_POST['description'];
			$long_description = $_POST['long_description'];
			$rate = $_POST['rate'];
			$unit = $_POST['unit'];

			$CI = &get_instance();
			$CI->db->insert('tlbdich_vu', [
				'description' => $description,
				'long_description' => $long_description,
				'rate' => $rate,
				'unit' => $unit,

			]);

			redirect('/admin/expenses/dich_vu', 'refresh');
		}
		$this->load->view('admin/expenses/dich_vu', $data);
	}
	public function muc_chi() {
		$data['muc_chi'] = $this->db->get('tlbdich_vu_chi')->result_array();

		if ($this->input->post()) {
			 $this->load->helper(array('form', 'url'));

                $this->load->library('form_validation');
$this->form_validation->set_rules('description', 'Description', 'required');
$this->form_validation->set_rules('rate', 'Rate', 'required');
   if ($this->form_validation->run() == TRUE)
                {
                	$description = $_POST['description'];
			$long_description = $_POST['long_description'];
			$rate = $_POST['rate'];
			$unit = $_POST['unit'];

			$CI = &get_instance();
			$CI->db->insert('tlbdich_vu_chi', [
				'description_chi' => $description,
				'long_description_chi' => $long_description,
				'rate_chi' => $rate,
				'unit_chi' => $unit,

			]);
			set_alert('success', _l('added_successfully', _l('service')));
			redirect('/admin/expenses/muc_chi', 'refresh');
                }else{
                	set_alert('danger', _l('added_failed', _l('service')));

                }
			
			
		}
		$this->load->view('admin/expenses/muc_chi', $data);
	}
	public function edit_service(){
		if(isset($_GET['id'])){
			$id=$_GET['id'];
			$item = $this->db->where('id', $id)->get('tlbdich_vu')->first_row();
			echo json_encode($item);
		}

	}
	public function edit_service_chi(){
		if(isset($_GET['id'])){
			$id=$_GET['id'];
			$item = $this->db->where('id_chi', $id)->get('tlbdich_vu_chi')->first_row();
			echo json_encode($item);
		}

	}

	// function sua dich vu
	public function postedit(){
		if ($this->input->post()) {
			$description = $_POST['description'];
			$long_description = $_POST['long_description'];
			$rate = $_POST['rate'];
			$unit = $_POST['unit'];

			$CI = &get_instance();
			$CI->db->update('tlbdich_vu', [
				'description' => $description,
				'long_description' => $long_description,
				'rate' => $rate,
				'unit' => $unit,

			],'id='.$_POST['id_service']);

			redirect('/admin/expenses/dich_vu', 'refresh');
		}
		$this->load->view('admin/expenses/dich_vu', $data);
	}
	public function postedit_chi(){
		if ($this->input->post()) {
			$description = $_POST['description'];
			$long_description = $_POST['long_description'];
			$rate =str_replace(",", "",$_POST['rate']);
			$unit = $_POST['unit'];

			$CI = &get_instance();
			$CI->db->update('tlbdich_vu_chi', [
				'description_chi' => $description,
				'long_description_chi' => $long_description,
				'rate_chi' => (int)$rate,
				'unit_chi' => $unit,

			],'id_chi='.$_POST['id_service']);

			redirect('/admin/expenses/muc_chi', 'refresh');
		}
		$this->load->view('admin/expenses/muc_chi', $data);
	}
	public function get_item_dich_vu($id) {
		$item = $this->db->where('id', $id)->get('tlbdich_vu')->result_array();
		echo json_encode($item);

	}
	public function get_client($id) {
		$item = $this->db->where('userid', $id)->get('tblclients')->result_array();
		echo json_encode($item);

	}
	public function get_item_service($id) {
		$item = $this->db->where('id_chi', $id)->get('tlbdich_vu_chi')->result_array();
		echo json_encode($item);

	}
	public function hopdong($id='') {

		if($this->input->post()){


			$a =$_POST['gia_tri'];
			$string = str_replace(',', '', $a);
			$b = (float)$string;
			$a1 =$_POST['tien_coc'];
			$string1 = str_replace(',', '', $a1);
			$b1 = (float)$string1;
			$name_chu=$_POST['name_building'];
			$thue=$_POST['thue'];
			$address=$_POST['address'];
			$gia_tri=$b;
			$name=$_POST['name'];
			$dia_chi=$_POST['dia_chi'];
			$phone=$_POST['phone'];
			$group=$_POST['group'];
			$cmnd=$_POST['cmnd'];
			$start_date=$_POST['start_date'];
			$end_date=$_POST['end_date'];
			$tien_coc=$b1;
			$recurring=$_POST['recurring'];

			if($id){
				$CI = &get_instance();
				$CI->db->update('tbl_hop_dong', [
					'name_chu' => $name,
					'phone' => $phone,
					'cmnd' => $cmnd,
					'address' => $address,
					'dia_chi' => $dia_chi,
					'gia_tri' => $gia_tri+$tien,
					'name_building' => $name_chu,
					'thue' => $thue,
					'group_building' => $group,
					'start_date' => $start_date,
					'end_date' => $end_date,
					'tien_coc' => $tien_coc,
					'recurringg' => $recurring,

				],'idhd='.$id);
				redirect('admin/expenses/bao_cao_hd', 'refresh');

			}else{
				$CI = &get_instance();
				$CI->db->insert('tbl_hop_dong', [
					'name_chu' => $name,
					'phone' => $phone,
					'cmnd' => $cmnd,
					'address' => $address,
					'dia_chi' => $dia_chi,
					'gia_tri' => $gia_tri+$tien,
					'name_building' => $name_chu,
					'thue' => $thue,
					'group_building' => $group,
					'start_date' => $start_date,
					'end_date' => $end_date,
					'tien_coc' => $tien_coc,
					'recurringg' => $recurring,
					'stt' => $recurring,

				]);
				redirect('admin/expenses/bao_cao_hd', 'refresh');
			}


		}
		$this->load->model('Invoice_items_model');
		$this->load->model('Taxes_model');
		$data['taxd'] = $this->Taxes_model->get();
		$data['data'] = $this->Invoice_items_model->get_data($id);
		$data['group'] = $this->Invoice_items_model->get_groups();
		$this->load->view('admin/expenses/hopdong', $data);
	}
	public function delete_dichvu($nam) {
		$data['dich_vu'] = $this->db->get('tlbdich_vu')->result_array();
		$CI = &get_instance();
		$CI->db->delete('tlbdich_vu', array('id' => $nam));
		// $this->load->view('admin/expenses/dich_vu', $data);
		redirect('admin/expenses/dich_vu', 'refresh');
	}
	public function delete_chiphi($nam) {

		$CI = &get_instance();
		$CI->db->delete('tblexpen', array('id' => $nam));
		// $this->load->view('admin/expenses/list_chiphi', $data);
		redirect('admin/expenses/list_chiphi', 'refresh');
	}
	public function list_chiphi() {
		if ($this->input->post()) {

			$id = $_POST['id'];
			$expense_name = $_POST['expense_name'];
			$note = $_POST['note'];
			$category = $_POST['category'];
			$build = $_POST['build'];
			$reference_no = $_POST['reference_no'];
			$paymentmode = $_POST['paymentmode'];
			$tax = $_POST['tax'];
			$amount = $_POST['amount'];

			$this->db->where('id', $id);
			$this->db->update('tblexpen', [

				'name' => $expense_name,
				'note' => $note,
				'reference_no' => $reference_no,
				'total_money' => $amount,
			]);
		}
		$this->load->view('admin/expenses/list_chiphi', $data);
	}
	public function chiphi() {

		if ($this->input->post()) {

			$expense_name = $_POST['expense_name'];
			$note = $_POST['note'];
			$category = $_POST['category'];
			$build = $_POST['build'];
			$date = $_POST['date'];
			$amount = $_POST['amount'];
			$paymentmode = $_POST['paymentmode'];
			$reference_no = $_POST['reference_no'];
			$repeat_every = $_POST['repeat_every'];
			$repeat_every_custom = $_POST['repeat_every_custom'];
			$repeat_type_custom = $_POST['repeat_type_custom'];
			$clientid = $_POST['clientid'];
			$tax = $_POST['tax'];
			if ($tax) {
				$total1 = $tax * $amount / 100;
				$total = $amount + $total1;
			} else {
				$total = $amount;
			}

			$CI = &get_instance();
			$CI->db->insert('tblexpen', [
				'file' => 1,
				'name' => $expense_name,
				'note' => $note,
				'category_id' => $category,
				'date' => $date,
				'total_money' => $total,
				'clientid' => $clientid,
				'paymentmode' => $paymentmode,
				'reference_no' => $reference_no,
				'repeat_every' => $repeat_every,
				'group_id' => $build,
			]);

			redirect('/admin/expenses/list_chiphi', 'refresh');
		}
		$this->load->view('admin/expenses/chiphi', $data);
	}
	public function list_expenses($id = '') {
		close_setup_menu();

		if (!has_permission('expenses', '', 'view') && !has_permission('expenses', '', 'view_own')) {
			access_denied('expenses');
		}

		$data['expenseid'] = $id;
		$data['categories'] = $this->expenses_model->get_category();
		$data['years'] = $this->expenses_model->get_expenses_years();
		$data['title'] = _l('expenses');

		$this->load->view('admin/expenses/manage', $data);
	}

	public function table($clientid = '') {
		if (!has_permission('expenses', '', 'view') && !has_permission('expenses', '', 'view_own')) {
			ajax_access_denied();
		}

		$this->app->get_table_data('expenses', [
			'clientid' => $clientid,
		]);
	}

	public function expense($id = '') {
		if ($this->input->post()) {
			if ($id == '') {
				if (!has_permission('expenses', '', 'create')) {
					set_alert('danger', _l('access_denied'));
					echo json_encode([
						'url' => admin_url('expenses/expense'),
					]);
					die;
				}
				$id = $this->expenses_model->add($this->input->post());
				if ($id) {
					set_alert('success', _l('added_successfully', _l('expense')));
					echo json_encode([
						'url' => admin_url('expenses/list_expenses/' . $id),
						'expenseid' => $id,
					]);
					die;
				}
				echo json_encode([
					'url' => admin_url('expenses/expense'),
				]);
				die;
			}
			if (!has_permission('expenses', '', 'edit')) {
				set_alert('danger', _l('access_denied'));
				echo json_encode([
					'url' => admin_url('expenses/expense/' . $id),
				]);
				die;
			}
			$success = $this->expenses_model->update($this->input->post(), $id);
			if ($success) {
				set_alert('success', _l('updated_successfully', _l('expense')));
			}
			echo json_encode([
				'url' => admin_url('expenses/list_expenses/' . $id),
				'expenseid' => $id,
			]);
			die;
		}
		if ($id == '') {
			$title = _l('add_new', _l('expense_lowercase'));
		} else {
			$data['expense'] = $this->expenses_model->get($id);

			if (!$data['expense'] || (!has_permission('expenses', '', 'view') && $data['expense']->addedfrom != get_staff_user_id())) {
				blank_page(_l('expense_not_found'));
			}

			$title = _l('edit', _l('expense_lowercase'));
		}

		if ($this->input->get('customer_id')) {
			$data['customer_id'] = $this->input->get('customer_id');
		}

		$this->load->model('taxes_model');
		$this->load->model('payment_modes_model');
		$this->load->model('currencies_model');

		$data['taxes'] = $this->taxes_model->get();
		$data['categories'] = $this->expenses_model->get_category();
		$data['payment_modes'] = $this->payment_modes_model->get('', [
			'invoices_only !=' => 1,
		]);
		$data['bodyclass'] = 'expense';
		$data['currencies'] = $this->currencies_model->get();
		$data['title'] = $title;
		$this->load->view('admin/expenses/expense', $data);
	}

	public function get_expenses_total() {
		if ($this->input->post()) {
			$data['totals'] = $this->expenses_model->get_expenses_total($this->input->post());

			if ($data['totals']['currency_switcher'] == true) {
				$this->load->model('currencies_model');
				$data['currencies'] = $this->currencies_model->get();
			}

			$data['expenses_years'] = $this->expenses_model->get_expenses_years();

			if (count($data['expenses_years']) >= 1 && $data['expenses_years'][0]['year'] != date('Y')) {
				array_unshift($data['expenses_years'], ['year' => date('Y')]);
			}

			$data['_currency'] = $data['totals']['currencyid'];
			$this->load->view('admin/expenses/expenses_total_template', $data);
		}
	}

	public function delete($id) {
		if (!has_permission('expenses', '', 'delete')) {
			access_denied('expenses');
		}
		if (!$id) {
			redirect(admin_url('expenses/list_expenses'));
		}
		$response = $this->expenses_model->delete($id);
		if ($response === true) {
			set_alert('success', _l('deleted', _l('expense')));
		} else {
			if (is_array($response) && $response['invoiced'] == true) {
				set_alert('warning', _l('expense_invoice_delete_not_allowed'));
			} else {
				set_alert('warning', _l('problem_deleting', _l('expense_lowercase')));
			}
		}

		if (strpos($_SERVER['HTTP_REFERER'], 'expenses/') !== false) {
			redirect(admin_url('expenses/list_expenses'));
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	public function copy($id) {
		if (!has_permission('expenses', '', 'create')) {
			access_denied('expenses');
		}
		$new_expense_id = $this->expenses_model->copy($id);
		if ($new_expense_id) {
			set_alert('success', _l('expense_copy_success'));
			redirect(admin_url('expenses/expense/' . $new_expense_id));
		} else {
			set_alert('warning', _l('expense_copy_fail'));
		}
		redirect(admin_url('expenses/list_expenses/' . $id));
	}

	public function convert_to_invoice($id) {
		if (!has_permission('invoices', '', 'create')) {
			access_denied('Convert Expense to Invoice');
		}
		if (!$id) {
			redirect(admin_url('expenses/list_expenses'));
		}
		$draft_invoice = false;
		if ($this->input->get('save_as_draft')) {
			$draft_invoice = true;
		}

		$params = [];
		if ($this->input->get('include_note') == 'true') {
			$params['include_note'] = true;
		}

		if ($this->input->get('include_name') == 'true') {
			$params['include_name'] = true;
		}

		$invoiceid = $this->expenses_model->convert_to_invoice($id, $draft_invoice, $params);
		if ($invoiceid) {
			set_alert('success', _l('expense_converted_to_invoice'));
			redirect(admin_url('invoices/invoice/' . $invoiceid));
		} else {
			set_alert('warning', _l('expense_converted_to_invoice_fail'));
		}
		redirect(admin_url('expenses/list_expenses/' . $id));
	}

	public function get_expense_data_ajax($id) {
		if (!has_permission('expenses', '', 'view') && !has_permission('expenses', '', 'view_own')) {
			echo _l('access_denied');
			die;
		}
		$expense = $this->expenses_model->get($id);

		if (!$expense || (!has_permission('expenses', '', 'view') && $expense->addedfrom != get_staff_user_id())) {
			echo _l('expense_not_found');
			die;
		}

		$data['expense'] = $expense;
		if ($expense->billable == 1) {
			if ($expense->invoiceid !== null) {
				$this->load->model('invoices_model');
				$data['invoice'] = $this->invoices_model->get($expense->invoiceid);
			}
		}

		$data['child_expenses'] = $this->expenses_model->get_child_expenses($id);
		$data['members'] = $this->staff_model->get('', ['active' => 1]);
		$this->load->view('admin/expenses/expense_preview_template', $data);
	}

	public function get_customer_change_data($customer_id = '') {
		echo json_encode([
			'customer_has_projects' => customer_has_projects($customer_id),
			'client_currency' => $this->clients_model->get_customer_default_currency($customer_id),
		]);
	}

	public function categories() {
		if (!is_admin()) {
			access_denied('expenses');
		}
		if ($this->input->is_ajax_request()) {
			$this->app->get_table_data('expenses_categories');
		}
		$data['title'] = _l('expense_categories');
		$this->load->view('admin/expenses/manage_categories', $data);
	}

	public function category() {
		if (!is_admin() && get_option('staff_members_create_inline_expense_categories') == '0') {
			access_denied('expenses');
		}
		if ($this->input->post()) {
			if (!$this->input->post('id')) {
				$id = $this->expenses_model->add_category($this->input->post());
				echo json_encode([
					'success' => $id ? true : false,
					'message' => $id ? _l('added_successfully', _l('expense_category')) : '',
					'id' => $id,
					'name' => $this->input->post('name'),
				]);
			} else {
				$data = $this->input->post();
				$id = $data['id'];
				unset($data['id']);
				$success = $this->expenses_model->update_category($data, $id);
				$message = _l('updated_successfully', _l('expense_category'));
				echo json_encode(['success' => $success, 'message' => $message]);
			}
		}
	}

	public function delete_category($id) {
		if (!is_admin()) {
			access_denied('expenses');
		}
		if (!$id) {
			redirect(admin_url('expenses/categories'));
		}
		$response = $this->expenses_model->delete_category($id);
		if (is_array($response) && isset($response['referenced'])) {
			set_alert('warning', _l('is_referenced', _l('expense_category_lowercase')));
		} elseif ($response == true) {
			set_alert('success', _l('deleted', _l('expense_category')));
		} else {
			set_alert('warning', _l('problem_deleting', _l('expense_category_lowercase')));
		}
		redirect(admin_url('expenses/categories'));
	}

	public function add_expense_attachment($id) {
		handle_expense_attachments($id);
		echo json_encode([
			'url' => admin_url('expenses/list_expenses/' . $id),
		]);
	}

	public function delete_expense_attachment($id, $preview = '') {
		$this->db->where('rel_id', $id);
		$this->db->where('rel_type', 'expense');
		$file = $this->db->get('tblfiles')->row();

		if ($file->staffid == get_staff_user_id() || is_admin()) {
			$success = $this->expenses_model->delete_expense_attachment($id);
			if ($success) {
				set_alert('success', _l('deleted', _l('expense_receipt')));
			} else {
				set_alert('warning', _l('problem_deleting', _l('expense_receipt_lowercase')));
			}
			if ($preview == '') {
				redirect(admin_url('expenses/expense/' . $id));
			} else {
				redirect(admin_url('expenses/list_expenses/' . $id));
			}
		} else {
			access_denied('expenses');
		}
	}
//function find all pile contract expired by day
	public function find_expired_pile_contract(){
		// $contracts= $this->db->get('btl_coc')->where()->result_array();
		$CI = &get_instance();
		$today = date('Y-m-d');
		$yesterday = date('Y-m-d', strtotime("-1 days"));
		// $total=total_rows('btl_coc','han_coc BETWEEN "'.$today.'" AND "'.$plus_7_days.'" AND han_coc is NOT NULL');
		$contracts = $CI->db->query('SELECT * FROM btl_coc WHERE trang_thai != 4 AND han_coc ="'.$yesterday.'"')->result_array();
		if(sizeof($contracts)>0){
		$error=0;
		foreach ($contracts as $key => $value) {
			//update expired contract 
			$this->db->where('id_coc', $value['id_coc']);
			$save=$this->db->update('btl_coc', [
						'trang_thai' => 4
						]);
			if(!$save){
			$error+=1;
			}
		}
	
		if($error==0){
			echo json_encode(["status"=>'OK']);
		}else{
			echo json_encode(["status"=>'ERROR!']);

		}
	}else{
		echo json_encode(["status"=>'404','message'=>'Not found expired contract! ']);
	}
	}
	//end function
	//function list pile contract
	public function list_pile_contract(){
		$CI = &get_instance();
		$data['contracts']= $CI->db->query("SELECT * FROM btl_coc AS pile LEFT JOIN tblclients AS client ON client.userid=pile.khach_hang ")->result_array();
		$this->load->view('admin/expenses/list_pile_contract', $data);

	}
	//end function list pile contract


}

