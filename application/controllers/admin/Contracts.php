<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Contracts extends Admin_controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('invoices_model');
		$this->load->model('contracts_model');
	}

	/* List all contracts */
	public function index() {
		close_setup_menu();

		if (!has_permission('contracts', '', 'view') && !has_permission('contracts', '', 'view_own')) {
			access_denied('contracts');
		}

		$data['chart_types'] = json_encode($this->contracts_model->get_contracts_types_chart_data());
		$data['chart_types_values'] = json_encode($this->contracts_model->get_contracts_types_values_chart_data());
		$data['contract_types'] = $this->contracts_model->get_contract_types();
		$data['years'] = $this->contracts_model->get_contracts_years();
		$this->load->model('currencies_model');
		$data['base_currency'] = $this->currencies_model->get_base_currency();
		$data['title'] = _l('contracts');
		$this->load->view('admin/contracts/manage', $data);
	}

	public function table($clientid = '') {
		if (!has_permission('contracts', '', 'view') && !has_permission('contracts', '', 'view_own')) {
			ajax_access_denied();
		}

		$this->app->get_table_data('contracts', [
			'clientid' => $clientid,
		]);
	}
	private function vn_to_str($str)
    {

        $unicode = array(

            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

            'd' => 'đ',

            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

            'i' => 'í|ì|ỉ|ĩ|ị',

            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

            'y' => 'ý|ỳ|ỷ|ỹ|ỵ',

            'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'D' => 'Đ',

            'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'I' => 'Í|Ì|Ỉ|Ĩ|Ị',

            'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );

        foreach ($unicode as $nonUnicode => $uni) {

            $str = preg_replace("/($uni)/i", $nonUnicode, $str);

        }
//        $str = str_replace(' ', ' ', $str);

        return $str;

    }
	/* Edit contract or add new contract */
	public function contract($id = '') {
		
		$data['staff'] = $this->staff_model->get('', ['active' => 1]);
		if($id!=''){
			$client=$this->db->query('SELECT client FROM tblcontracts WHERE id='.$id)->row_array();
		$data['coc']  =$this->db->query('SELECT * FROM btl_coc WHERE khach_hang='.$client['client'])->result_array();

		}else{
		$data['coc'] = "";
		}
		$startDate = strtotime($_POST['datestart']);;
		$endDate = strtotime($_POST['dateend']);;
		if ($startDate > $endDate){
		$data['error']=_l('error_date');
		}else{

		$this->load->model('Invoices_model');
		if ($this->input->post()) {
		$date_coc = $_POST['dateend'];
		$coc_id = $_POST['deposit'];
		$this->db->where('id_coc',$coc_id);
		$this->db->update('btl_coc', [
				'han_coc' => $date_coc,
					]);

			if ($id == '') {
				$a =$_POST['deposit'];
			$string = str_replace(',', '', $a);
			$b = (float)$string;
		$_POST['deposit']=$a;	
		$ngay_db = $_POST['date_bd'];
		$thang = date('m',strtotime($ngay_db));
		$nam = date('Y',strtotime($ngay_db));
		$buil = $_POST['group'];
		$dien = $_POST['so_dien'];
		$nuoc = $_POST['so_nuoc'];
		if($thang==1){
			$nam_1 = $nam-1;
			$thang_1 = 12;
		}else{
			$nam_1 = $nam;
			$thang_1 = $thang-1;
		}
		$bien = $this->db->where('id_name_building',$buil)->where('nam',$nam_1)->where('thang',$thang_1)->get('tblmoney_sv')->result_array();
	
		if($bien){
			$this->db->where('id_name_building',$buil);
		$this->db->where('nam',$nam_1);
		$this->db->where('thang',$thang_1);
			$this->db->update('tblmoney_sv', [
				'dien' => $dien,
				'nuoc' => $nuoc,
				
			]);
		}else{
			$CI = &get_instance();
					$CI->db->insert('tblmoney_sv', [
						'id_name_building' => $buil,
						'nam' => $nam_1,
						'thang' => $thang_1,
						'dien' => $dien,
						'nuoc' => $nuoc,

					]);
		}
		
		
			$ngay_start = $_POST['datestart'];
			$ngay_end = $_POST['dateend'];
			$ngay = new DateTime($_POST['datestart']);
			$ngayn = Date("Y-m-d", strtotime($ngay_end . "+ 7 day"));
			$ngay1 = new DateTime($ngayn);
			$interval = date_diff($ngay, $ngay1);
			$date_y = $interval->format('%R%Y');
			$date_m = $interval->format('%R%m');
			$month = ($date_y * 12) + $date_m;
			$curr = $_POST['recurring'];
			$lan = ceil($month / $curr);
			$arr1[] = $_POST;
			$add = get_staff_user_id();
			$invoice_data = [
				'clientid' => $_POST['client'],
				'datecreated' => $_POST['datestart'],
				'date' => $_POST['datestart'],
				'currency' => '1',
				'id_buil' => $_POST['name_building'],
				'recurring' => $_POST['recurring'],
				'name_building' => $_POST['floor'],
				'subtotal' => 0,
				'total' => 0,
				'total_tax' => 0,
			];

			//start
			$cycles = $lan;
			for ($i=0; $i <$lan ; $i++) { 
				$cycles--;
				$ko = Date("Y-m-d", strtotime($ngay_start . "+" . $_POST['recurring']*$i . " Month"));
				$koo = Date("Y-m-d", strtotime($ngay_start . "+" . $_POST['recurring']*($i+1) . " Month"));
				$ko1 = Date("Y-m-d", strtotime($ngay_end . "+ 3 day"));
				$nko= new DateTime($ko);
				$nko1= new DateTime($ko1);
				$month_new = date('m',strtotime($ngay_start))+$_POST['recurring']*$i;
				$month_new_koo = date('m',strtotime($ngay_start))+$_POST['recurring']*($i+1);
				
				if($month_new>12&&$month_new<24){
					$month_new = $month_new -12;
				}
				else if($month_new>24&&$month_new<35){
					$month_new = $month_new -24;
				}
				else if($month_new>36&&$month_new<48){
					$month_new = $month_new -36;
				}else if($month_new==24||$month_new==36||$month_new==48){
					$month_new = 12;
				}
				
				if($month_new_koo>12&&$month_new_koo<24){
					$month_new_koo = $month_new_koo -12;
				}
				else if($month_new_koo>24&&$month_new_koo<35){
					$month_new_koo = $month_new_koo -24;
				}
				else if($month_new_koo>36&&$month_new_koo<48){
					$month_new_koo = $month_new_koo -36;
				}else if($month_new_koo==24||$month_new_koo==36||$month_new_koo==48){
					$month_new_koo = 12;
				}
				$month_old = date('m',strtotime($ko));
				$day_old = date('d',strtotime($ko));
				$year_old = date('Y',strtotime($ko));
				$day_new = date('d',strtotime($ngay_start));
				$year_new = date('Y',strtotime($ko));
				$month_old_koo = date('m',strtotime($koo));
				$year_old_koo = date('Y',strtotime($koo));
				$day_old_koo = date('d',strtotime($koo));
				
				if(checkdate($month_new, $day_new, $year_old)){
					
					if(checkdate($month_new_koo, $day_new, $year_old_koo)){
						$date_old = $koo;
					}else{
						$date_old = '01-'.$month_old_koo.'-'.$year_old_koo;
						$date_old = Date("Y-m-d", strtotime($date_old . "- 1 day"));
					}
					$date_new = $ko;
					
				}else{
				$date_new = '01-'.$month_old.'-'.$year_old;
				
				$date_new = Date("Y-m-d", strtotime($date_new . "- 1 day"));
				if(checkdate($month_new_koo, $day_new, $year_old_koo)){
						$date_old = $koo;
					}else{
						$date_old = '01-'.$month_old_koo.'-'.$year_old_koo;
						$date_old = Date("Y-m-d", strtotime($date_old . "- 1 day"));
					}
				
				}
			$so_sanh = date_diff($nko, $nko1);
			$mm = $so_sanh->format('%R%m');
			$yy = $so_sanh->format('%R%Y');
			$con_dem = ($yy * 12) + $mm;
			if($con_dem<$_POST['recurring']){
				$date_old = $_POST['dateend'];
			}
			$re=0;
			$ndv = $this->db->where('id',$_POST['group'])->get('tblitems')->result_array();
			$nd = $this->vn_to_str($ndv[0][description])."_TN";
			$item = count($_POST['name_dv']);
				$next_invoice_number = get_option('next_invoice_number');
				$time = Date("Y-m-d", strtotime($ngay . "+" . $re . " Month"));
				$dtime = Date("Y-m-d", strtotime($time . "+ 10 day"));
				$invoice_data['datecreated'] = date('Y-m-d');
				$invoice_data['date'] = $date_new;
				$invoice_data['title_payment'] = $nd;
				$invoice_data['duedate'] = $date_old;
				$invoice_data['invoi_tye'] = 1;
				$invoice_data['sale_agent'] = $_POST['sale_agent'];
				$invoice_data['cycles'] = $cycles;
				$invoice_data['number'] = $next_invoice_number;
				$invoice_data['allowed_payment_modes'] = ["1", "2", "stripe"];
				$idt = $this->invoices_model->add($invoice_data);
				
				$thue_in = 0;
				$tien_in = 0;
				for ($t = 0; $t < $item; $t++) {
					$description = $_POST['name_dv'][$t];
					$long_description = $_POST['long_dv'][$t];
					
					if( $_POST['id_item'][$t]<30){
						$qty = 0;
					}else{
						if($con_dem<$_POST['recurring']){
								$qty = $con_dem;
						}else{
							$qty = $_POST['recurring'];
						}
					}
					$a =$_POST['rate_dv'][$t];
					$string = str_replace(',', '', $a);
					$b = (float)$string;
					$rate = $b;
					$group_id = $_POST['group'];
					$id_item = $_POST['id_item'][$t];
					$tien_in = $tien_in + ($b*$qty);
					if($_POST['address_vp']){
					if($_POST['id_item'][$t]<1000&&$_POST['id_item'][$t]>50){
						$thue_in = $thue_in + ($rate*$qty)*0.1;
						
					}	
					}
					if($_POST['id_item'][$t]<1000&&$_POST['id_item'][$t]>50){
						$CI = &get_instance();
						$CI->db->insert('tblitems_in', [
						'rel_id' => $idt,
						'rel_type' => "invoice",
						'description' => $description,
						'long_description' => $long_description,
						'qty' => $qty,
						'rate' => $rate,
						'group_id' => $group_id,
						'item' => $id_item,

					]);
					}
					
				
				};
				$bp = $this->db->where('rel_id',$idt)->get('tblitems_in')->result_array();
				if($_POST['address_vp']){
					foreach ($bp as $key) {
					if($key['item']>50&&$key['item']<1000){
						$k = $key['id'];
						$CI = &get_instance();
					$CI->db->insert('tblitemstax', [
						'rel_id' => $idt,
						'rel_type' => "invoice",
						'itemid' => $k,
						'taxrate' => 10,
						'taxname' => "VAT",

					]);
					}
				}
				}
				
				$tong = $tien_in+$thue_in;
				$this->db->where('id',$idt);
					$this->db->update('tblinvoices', [
						'subtotal' => $tien_in,
						'total_tax' => $thue_in,
						'total' => $tong,
						'status' => 1,
					]);

				$re = $re + $_POST['recurring'];
				$_POST['stt'] = $_POST['recurring'];
			}
			//end
			if ($_POST['address_vp']) {
				$thue = ($_POST['address_vp'] * $_POST['contract_value']) / 100;
			}
			$tien = 0;
			$item = count($_POST['name_dv']);
			for ($p=0; $p <$item ; $p++) { 
				$a =$_POST['rate_dv'][$p];
					$string = str_replace(',', '', $a);
					$b = (float)$string;
					if($_POST['id_item'][$p]>=30){
					$tien = $tien + $b;
				}
			}
			
			$re = 0;
			
			$_POST['name_building'] = $_POST['group'];
			$group_t = $_POST['group'];
			unset($_POST['service']);
			unset($_POST['name_dv']);
			unset($_POST['long_dv']);
			unset($_POST['rate_dv']);
			unset($_POST['id_item']);
			unset($_POST['room_id']);
			unset($_POST['group']);
			unset($_POST['name_phi']);
			unset($_POST['id_item_phi']);
			unset($_POST['iditem_phi']);
			unset($_POST['rate_phi']);
			unset($_POST['id_buil']);
			$_POST['contract_value'] = $tien;
			
			
			$_POST['tota'] = $lan;
				if (!has_permission('contracts', '', 'create')) {
					access_denied('contracts');
				}
				$this->db->query("UPDATE `tblitems` SET `recurring` = '2' WHERE `tblitems`.`id` = $group_t");
				$id = $this->contracts_model->add($this->input->post());
			
				for ($tt = 0; $tt < $item; $tt++) {
					$description = $arr1[0]['name_dv'][$tt];
					$long_description = $arr1[0]['long_dv'][$tt];
					$qty = '1';
					$a =$arr1[0]['rate_dv'][$tt];
					$string = str_replace(',', '', $a);
					$b = (float)$string;
					$rate = $b;
					$group_id = $arr1[0]['group'];
					$id_item = $arr1[0]['id_item'][$tt];
					$CI = &get_instance();
					$CI->db->insert('tbl_contracts_item', [
						'rel_id' => $id,
						'description' => $description,
						'long_description' => $long_description,
						'qty' => $qty,
						'rate' => $rate,
						'group_id' => $group_id,
						'item' => $id_item,
					]);
				};
				if ($id) {
					set_alert('success', _l('added_successfully', _l('contract')));
					redirect(admin_url('contracts/contract/' . $id));
				}
			} else {
				if (!has_permission('contracts', '', 'edit')) {
					access_denied('contracts');
				}
				$a =$_POST['deposit'];
			$string = str_replace(',', '', $a);
			$b = (float)$string;
		$_POST['deposit']=$a;	
		$ngay_db = $_POST['date_bd'];
		$thang = date('m',strtotime($ngay_db));
		$nam = date('Y',strtotime($ngay_db));
		$buil = $_POST['group'];
		$dien = $_POST['so_dien'];
		$nuoc = $_POST['so_nuoc'];
		if($thang==1){
			$nam_1 = $nam-1;
			$thang_1 = 12;
		}else{
			$nam_1 = $nam;
			$thang_1 = $thang-1;
		}
		$bien = $this->db->where('id_name_building',$buil)->where('nam',$nam_1)->where('thang',$thang_1)->get('tblmoney_sv')->result_array();
	
		if($bien){
			$this->db->where('id_name_building',$buil);
		$this->db->where('nam',$nam_1);
		$this->db->where('thang',$thang_1);
			$this->db->update('tblmoney_sv', [
				'dien' => $dien,
				'nuoc' => $nuoc,
				
			]);
		}else{
			$CI = &get_instance();
					$CI->db->insert('tblmoney_sv', [
						'id_name_building' => $buil,
						'nam' => $nam_1,
						'thang' => $thang_1,
						'dien' => $dien,
						'nuoc' => $nuoc,

					]);
		}
			if ($_POST['address_vp']) {
				$thue = ($_POST['address_vp'] * $_POST['contract_value']) / 100;
			}
			$tien = 0;
			$item = count($_POST['name_dv']);
			for ($p=0; $p <$item ; $p++) { 
				$iditem = $_POST['iditem'][$p];
				$a =$_POST['rate_dv'][$p];
					$string = str_replace(',', '', $a);
					$b = (float)$string;
					$rate = $b;
				
				if($_POST['id_item'][$p]>=30){
					$tien = $tien + $rate;
				}
				$this->db->where('id',$iditem);
				$this->db->update('tbl_contracts_item', [
				'rate' => $rate,				
			]);
			}
			if($_POST['contract_type']==3||$_POST['contract_type']==4){
				$num = get_option('next_invoice_number');
			$invoice_data = [
				'clientid' => $_POST['client'],
				'datecreated' => date('Y-m-d'),
				'date' => $_POST['datestart'],
				'duedate' => $_POST['dateend'],
				'currency' => '1',
				'recurring' => 1,
				'cycles' => 1,
				'subtotal' => 0,
				'total' => 0,
				'total_tax' => 0,
				'title_payment' => "phí phát sinh",
				'invoi_tye' => 2,
				'number' => $num,
			];
			$idt_phi = $this->invoices_model->add($invoice_data);
			$tong_phi = 0;
			for ($i=0; $i <count($_POST['name_phi']) ; $i++) { 
				$description = $_POST['name_phi'][$i];
				$long_description = "";
				$a =$_POST['rate_phi'][$i];
				$string = str_replace(',', '', $a);
					$b = (float)$string;
					$rate = $b;
				$tong_phi = $tong_phi+$rate;
				$id_item_phi = $_POST['id_item_phi'][$i];
				if($_POST['name_phi'][$i]!=""){
					$CI = &get_instance();
						$CI->db->insert('tblitems_in', [
						'rel_id' => $idt_phi,
						'rel_type' => "invoice",
						'description' => $description,
						'long_description' => $long_description,
						'qty' => 1,
						'rate' => $rate,
						'group_id' => 1,
						'item' => $id_item_phi,

					]);
				}
				
			}
			$this->db->where('id',$idt_phi);
					$this->db->update('tblinvoices', [
						'subtotal' => $tong_phi,
						'total_tax' => 0,
						'total' => $tong_phi,
						'status' => 1,
					]);
			}
			if($_POST['contract_type']==2){
				$invoi_clientid = $_POST['client'];
				$id_buil = $_POST['id_buil'];

				$invoi_date = $this->db->where('clientid',$invoi_clientid)->where('id_buil',$id_buil)->where('status',2)->order_by('id','desc')->limit(1)->get('tblinvoices')->result_array();
				
				if($invoi_date){
					$invoi_date = $this->db->where('clientid',$invoi_clientid)->where('id_buil',$id_buil)->where('status',2)->order_by('id','desc')->limit(1)->get('tblinvoices')->result_array();
				}else{
					$invoi_date = $this->db->where('clientid',$invoi_clientid)->where('id_buil',$id_buil)->where('status',1)->order_by('id','asc')->limit(1)->get('tblinvoices')->result_array();
					$invoi_date[0]['duedate']=$invoi_date[0]['date'];
				}
				$delete_invoice = $this->db->query("DELETE FROM `tblinvoices` WHERE `tblinvoices`.`clientid` = $invoi_clientid and `tblinvoices`.`id_buil` = $id_buil and `tblinvoices`.`status` = 1 ");
			$ngay_start = $invoi_date[0]['duedate'];
			$ngay_end = $_POST['dateend'];
			$ngay = new DateTime($invoi_date[0]['duedate']);
			$ngayn = Date("Y-m-d", strtotime($ngay_end . "+ 7 day"));
			$ngay1 = new DateTime($ngayn);
			$interval = date_diff($ngay, $ngay1);
			$date_y = $interval->format('%R%Y');
			$date_m = $interval->format('%R%m');
			$month = ($date_y * 12) + $date_m;
			$curr = $invoi_date[0]['recurring'];
			$lan = ceil($month / $curr);
			$add = get_staff_user_id();
				$invoice_data = [
				'clientid' => $invoi_date[0]['clientid'],
				'date' => date('Y-m-d'),
				'currency' => '1',
				'id_buil' => $invoi_date[0]['id_buil'],
				'recurring' => $invoi_date[0]['recurring'],
				'name_building' => $invoi_date[0]['name_building'],
				'subtotal' => 0,
				'total' => 0,
				'total_tax' => 0,
					];
				//start
			$cycles = $lan;
			for ($i=0; $i <$lan ; $i++) {
				$cycles--;
				$ko = Date("Y-m-d", strtotime($ngay_start . "+" . $invoi_date[0]['recurring']*$i . " Month"));
				$koo = Date("Y-m-d", strtotime($ngay_start . "+" . $invoi_date[0]['recurring']*($i+1) . " Month"));
				$ko1 = Date("Y-m-d", strtotime($ngay_end . "+ 3 day"));
				$nko= new DateTime($ko);
				$nko1= new DateTime($ko1);
				$month_new = date('m',strtotime($ngay_start))+$invoi_date[0]['recurring']*$i;
				$month_new_koo = date('m',strtotime($ngay_start))+$invoi_date[0]['recurring']*($i+1);
				
				if($month_new>12&&$month_new<24){
					$month_new = $month_new -12;
				}
				else if($month_new>24&&$month_new<35){
					$month_new = $month_new -24;
				}
				else if($month_new>36&&$month_new<48){
					$month_new = $month_new -36;
				}else if($month_new==24||$month_new==36||$month_new==48){
					$month_new = 12;
				}
				
				if($month_new_koo>12&&$month_new_koo<24){
					$month_new_koo = $month_new_koo -12;
				}
				else if($month_new_koo>24&&$month_new_koo<35){
					$month_new_koo = $month_new_koo -24;
				}
				else if($month_new_koo>36&&$month_new_koo<48){
					$month_new_koo = $month_new_koo -36;
				}else if($month_new_koo==24||$month_new_koo==36||$month_new_koo==48){
					$month_new_koo = 12;
				}
				$month_old = date('m',strtotime($ko));
				$day_old = date('d',strtotime($ko));
				$year_old = date('Y',strtotime($ko));
				$day_new = date('d',strtotime($ngay_start));
				$year_new = date('Y',strtotime($ko));
				$month_old_koo = date('m',strtotime($koo));
				$year_old_koo = date('Y',strtotime($koo));
				$day_old_koo = date('d',strtotime($koo));
				
				if(checkdate($month_new, $day_new, $year_old)){
					
					if(checkdate($month_new_koo, $day_new, $year_old_koo)){
						$date_old = $koo;
					}else{
						$date_old = '01-'.$month_old_koo.'-'.$year_old_koo;
						$date_old = Date("Y-m-d", strtotime($date_old . "- 1 day"));
					}
					$date_new = $ko;
					
				}else{
				$date_new = '01-'.$month_old.'-'.$year_old;
				
				$date_new = Date("Y-m-d", strtotime($date_new . "- 1 day"));
				if(checkdate($month_new_koo, $day_new, $year_old_koo)){
						$date_old = $koo;
					}else{
						$date_old = '01-'.$month_old_koo.'-'.$year_old_koo;
						$date_old = Date("Y-m-d", strtotime($date_old . "- 1 day"));
					}
				
				}
			$so_sanh = date_diff($nko, $nko1);
			$mm = $so_sanh->format('%R%m');
			$yy = $so_sanh->format('%R%Y');
			$con_dem = ($yy * 12) + $mm;
			if($con_dem<$invoi_date[0]['recurring']){
				$date_old = $_POST['dateend'];
			}
			$re=0;
			$ndv = $this->db->where('id',$_POST['group'])->get('tblitems')->result_array();
			$nd = $this->vn_to_str($ndv[0][description])."_TN";
			$item = count($_POST['name_dv']);
				$next_invoice_number = get_option('next_invoice_number');
				$time = Date("Y-m-d", strtotime($ngay . "+" . $re . " Month"));
				$dtime = Date("Y-m-d", strtotime($time . "+ 10 day"));
				$invoice_data['datecreated'] = date('Y-m-d');
				$invoice_data['date'] = $date_new;
				$invoice_data['title_payment'] = $nd;
				$invoice_data['duedate'] = $date_old;
				$invoice_data['invoi_tye'] = 1;
				$invoice_data['cycles'] = $cycles;
				$invoice_data['number'] = $next_invoice_number;
				$invoice_data['allowed_payment_modes'] = ["1", "2", "stripe"];
				$idt = $this->invoices_model->add($invoice_data);
				
				$thue_in = 0;
				$tien_in = 0;
				for ($t = 0; $t < $item; $t++) {
					$description = $_POST['name_dv'][$t];
					$long_description = $_POST['long_dv'][$t];
					
					if( $_POST['id_item'][$t]<30){
						$qty = 0;
					}else{
						if($con_dem<$invoi_date[0]['recurring']){
								$qty = $con_dem;
						}else{
							$qty = $invoi_date[0]['recurring'];
						}
					}
					$a =$_POST['rate_dv'][$t];
					$string = str_replace(',', '', $a);
					$b = (float)$string;
					$rate = $b;
					$group_id = $_POST['group'];
					$id_item = $_POST['id_item'][$t];
					$tien_in = $tien_in + ($b*$qty);
					if($_POST['address_vp']){
					if($_POST['id_item'][$t]<1000&&$_POST['id_item'][$t]>50){
						$thue_in = $thue_in + ($rate*$qty)*0.1;
						
					}	
					}
					if($_POST['id_item'][$t]<1000&&$_POST['id_item'][$t]>50){
						$CI = &get_instance();
						$CI->db->insert('tblitems_in', [
						'rel_id' => $idt,
						'rel_type' => "invoice",
						'description' => $description,
						'long_description' => $long_description,
						'qty' => $qty,
						'rate' => $rate,
						'group_id' => $group_id,
						'item' => $id_item,

					]);
					}
					
				
				};
				$bp = $this->db->where('rel_id',$idt)->get('tblitems_in')->result_array();
				if($_POST['address_vp']){
					foreach ($bp as $key) {
					if($key['item']>50&&$key['item']<1000){
						$k = $key['id'];
						$CI = &get_instance();
					$CI->db->insert('tblitemstax', [
						'rel_id' => $idt,
						'rel_type' => "invoice",
						'itemid' => $k,
						'taxrate' => 10,
						'taxname' => "VAT",

					]);
					}
				}
				}
				
				$tong = $tien_in+$thue_in;
				$this->db->where('id',$idt);
					$this->db->update('tblinvoices', [
						'subtotal' => $tien_in,
						'total_tax' => $thue_in,
						'total' => $tong,
						'status' => 1,
					]);

				$re = $re + $invoi_date[0]['recurring'];
				$_POST['stt'] = $invoi_date[0]['recurring'];
			}
			//end
				
			}
			$re = 0;
			$_POST['name_building'] = $_POST['id_buil'];
			$group_t = $_POST['id_buil'];
			unset($_POST['service']);
			unset($_POST['name_dv']);
			unset($_POST['long_dv']);
			unset($_POST['rate_dv']);
			unset($_POST['id_item']);
			unset($_POST['room_id']);
			unset($_POST['group']);
			unset($_POST['iditem']);
			unset($_POST['name_phi']);
			unset($_POST['id_item_phi']);
			unset($_POST['iditem_phi']);
			unset($_POST['rate_phi']);
			unset($_POST['id_buil']);
			$_POST['contract_value'] = $tien;
			
			
			$_POST['tota'] = $lan;
				$success = $this->contracts_model->update($this->input->post(), $id);
				$item = count($_POST['name_dv']);
			for ($p=0; $p <$item ; $p++) { 
				$iditem = $_POST['iditem'][$p];
				$a =$_POST['rate_dv'][$p];
					$string = str_replace(',', '', $a);
					$b = (float)$string;
					$rate = abs($b);
				
				if($_POST['id_item'][$p]>=3){
					$tien = $tien + $rate;
				}
				$this->db->where('id',$iditem);
				$this->db->update('tbl_contracts_item', [
				'rate' => $rate,				
			]);
			}
				$next_invoice_number = get_option('next_invoice_number');
				if($_POST['contract_type']==3){
					$this->db->query("UPDATE `tblitems` SET `recurring` = '1' WHERE `tblitems`.`id` = $group_t");
				$floor=$_POST['floor'];
				$this->db->where('status',1)->where('name_building',$floor)->delete('tblinvoices');
					$ids = $_POST['deposit'];
					$date_update = $_POST['dateend'];
					$dd = $this->db->where('id_coc',$ids)->get('btl_coc')->result_array();
					 $ttt = (int)-$dd[0]['tien_coc'];
					
						$CI = &get_instance();
						$CI->db->insert('tblinvoices', [
							'clientid' => $_POST['client'],
							'number' => $next_invoice_number,
							'datecreated' =>date('Y-m-d'),
							'date' => date('Y-m-d'),
							'duedate' => date('Y-m-d'),
							'currency' => 1,
							'subtotal' => $ttt,
							'total' => $ttt,
							'status' =>2,
							'invoi_tye' =>3,
						]);
				$this->db->where('id_coc',$ids);
				$this->db->update('btl_coc', [
				'trang_thai' => 2,
				'tien_tra' => 1,
				'date_update' => $date_update,

			]);
				}
				if($_POST['contract_type']==4){
					$this->db->query("UPDATE `tblitems` SET `recurring` = '1' WHERE `tblitems`.`id` = $group_t");
					$floor=$_POST['floor'];
				$this->db->where('status',1)->where('name_building',$floor)->delete('tblinvoices');
					$ids = $_POST['deposit'];
					$date_update = $_POST['dateend'];
				$this->db->where('id_coc',$ids);
				$this->db->update('btl_coc', [
				'trang_thai' => 3,
				'tien_tra' => 0,
				'date_update' => $date_update,

			]);
				}
				if ($success) {
					set_alert('success', _l('updated_successfully', _l('contract')));
				}
				if($idt_phi){
					redirect(admin_url('invoices/list_invoices#' . $idt_phi));
				}else{
				redirect(admin_url('contracts/contract/' . $id));
				}
				
			}
		}
		if ($id == '') {
			$title = _l('add_new', _l('contract_lowercase'));
		} else {
		    $data['item_contract'] = $this->contracts_model->get_item_contract($id);
			$data['contract'] = $this->contracts_model->get($id, [], true);
			$data['contract_renewal_history'] = $this->contracts_model->get_contract_renewal_history($id);
			$data['totalNotes'] = total_rows('tblnotes', ['rel_id' => $id, 'rel_type' => 'contract']);
			if (!$data['contract'] || (!has_permission('contracts', '', 'view') && $data['contract']->addedfrom != get_staff_user_id())) {
				blank_page(_l('contract_not_found'));
			}
			$contract_merge_fields = get_available_merge_fields();
			$_contract_merge_fields = [];
			foreach ($contract_merge_fields as $key => $val) {
				foreach ($val as $type => $f) {
					if ($type == 'contract') {
						foreach ($f as $available) {
							foreach ($available['available'] as $av) {
								if ($av == 'contract') {
									array_push($_contract_merge_fields, $f);

									break;
								}
							}

							break;
						}
					} elseif ($type == 'other') {
						array_push($_contract_merge_fields, $f);
					} elseif ($type == 'clients') {
						array_push($_contract_merge_fields, $f);
					}
				}
			}
			$data['contract_merge_fields'] = $_contract_merge_fields;
			$title = $data['contract']->subject;

			$contact = $this->clients_model->get_contact(get_primary_contact_user_id($data['contract']->client));
			$email = '';
			if ($contact) {
				$email = $contact->email;
			}

			$template_name = 'send-contract';
			$data['template'] = get_email_template_for_sending($template_name, $email);
			$data['template_name'] = $template_name;

			$this->db->where('slug', $template_name);
			$this->db->where('language', 'english');
			$template_result = $this->db->get('tblemailtemplates')->row();

			$data['template_system_name'] = $template_result->name;
			$data['template_id'] = $template_result->emailtemplateid;

			$data['template_disabled'] = false;
			if (total_rows('tblemailtemplates', ['slug' => $data['template_name'], 'active' => 0]) > 0) {
				$data['template_disabled'] = true;
			}
		}

		if ($this->input->get('customer_id')) {
			$data['customer_id'] = $this->input->get('customer_id');
		}
		}
		if($id!=''){
			$data['id']=$id;
		}
		$this->load->model('currencies_model');
		$this->load->model('Invoice_items_model');
		$this->load->model('Taxes_model');
		$data['invoi'] = $this->Invoice_items_model->get_item_vp();
		$data['dich_vu'] = $this->Invoice_items_model->get_dich_vu();
		$data['taxd'] = $this->Taxes_model->get();
		$data['group'] = $this->Invoice_items_model->get_groups();
		$data['base_currency'] = $this->currencies_model->get_base_currency();
		$data['types'] = $this->contracts_model->get_contract_types();
		$data['title'] = $title;
		$data['bodyclass'] = 'contract';
		$this->load->view('admin/contracts/contract', $data);
	}

	public function get_template() {
		$name = $this->input->get('name');
		echo $this->load->view('admin/contracts/templates/' . $name, [], true);
	}
public function group_in($group){
		$this->load->model('Invoice_items_model');
		$invoi = $this->Invoice_items_model->get_item_in_group($group);
		
		echo json_encode([
				'invoi' => $this->Invoice_items_model->get_item_in_group($group),
			]);

	}
	public function name_group_in($id){
		$this->load->model('Invoice_items_model');
		$invoi = $this->db->where('id = ',$id)->get('tblitems_groups')->result_array();
		echo json_encode([
				'name_group_in' => $invoi,
			]);

	}
	public function name_buil_in($id){
			$this->load->model('Invoice_items_model');
			$invoi = $this->db->where('id = ',$id)->get('tblitems')->result_array();
			echo json_encode([
					'name_buil_in' => $invoi,
				]);

		}
	public function name_client_in($id){
			$invoi = $this->db->where('userid = ',$id)->get('tblclients')->result_array();
			echo json_encode([
					'name_client_in' => $invoi,
				]);

		}
	public function pdf($id) {
		if (!has_permission('contracts', '', 'view') && !has_permission('contracts', '', 'view_own')) {
			access_denied('contracts');
		}

		if (!$id) {
			redirect(admin_url('contracts'));
		}

		$contract = $this->contracts_model->get($id);

		try {
			$pdf = contract_pdf($contract);
		} catch (Exception $e) {
			echo $e->getMessage();
			die;
		}

		$type = 'D';

		if ($this->input->get('output_type')) {
			$type = $this->input->get('output_type');
		}

		if ($this->input->get('print')) {
			$type = 'I';
		}

		$pdf->Output(slug_it($contract->subject) . '.pdf', $type);
	}

	public function send_to_email($id) {
		if (!has_permission('contracts', '', 'view') && !has_permission('contracts', '', 'view_own')) {
			access_denied('contracts');
		}
		$success = $this->contracts_model->send_contract_to_client($id, $this->input->post('attach_pdf'), $this->input->post('cc'));
		if ($success) {
			set_alert('success', _l('contract_sent_to_client_success'));
		} else {
			set_alert('danger', _l('contract_sent_to_client_fail'));
		}
		redirect(admin_url('contracts/contract/' . $id));
	}

	public function add_note($rel_id) {
		if ($this->input->post() && (has_permission('contracts', '', 'view') || has_permission('contracts', '', 'view_own'))) {
			$this->misc_model->add_note($this->input->post(), 'contract', $rel_id);
			echo $rel_id;
		}
	}

	public function get_notes($id) {
		if ((has_permission('contracts', '', 'view') || has_permission('contracts', '', 'view_own'))) {
			$data['notes'] = $this->misc_model->get_notes($id, 'contract');
			$this->load->view('admin/includes/sales_notes_template', $data);
		}
	}

	public function clear_signature($id) {
		if (has_permission('contracts', '', 'delete')) {
			$this->contracts_model->clear_signature($id);
		}

		redirect(admin_url('contracts/contract/' . $id));
	}

	public function save_contract_data() {
		if (!has_permission('contracts', '', 'edit') && !has_permission('contracts', '', 'create')) {
			header('HTTP/1.0 400 Bad error');
			echo json_encode([
				'success' => false,
				'message' => _l('access_denied'),
			]);
			die;
		}

		$success = false;
		$message = '';

		$this->db->where('id', $this->input->post('contract_id'));
		$this->db->update('tblcontracts', [
			'content' => $this->input->post('content', false),
		]);

		$success = $this->db->affected_rows() > 0;
		$message = _l('updated_successfully', _l('contract'));

		echo json_encode([
			'success' => $success,
			'message' => $message,
		]);
	}

	public function add_comment() {
		if ($this->input->post()) {
			echo json_encode([
				'success' => $this->contracts_model->add_comment($this->input->post()),
			]);
		}
	}

	public function edit_comment($id) {
		if ($this->input->post()) {
			echo json_encode([
				'success' => $this->contracts_model->edit_comment($this->input->post(), $id),
				'message' => _l('comment_updated_successfully'),
			]);
		}
	}

	public function get_comments($id) {
		$data['comments'] = $this->contracts_model->get_comments($id);
		$this->load->view('admin/contracts/comments_template', $data);
	}

	public function remove_comment($id) {
		$this->db->where('id', $id);
		$comment = $this->db->get('tblcontractcomments')->row();
		if ($comment) {
			if ($comment->staffid != get_staff_user_id() && !is_admin()) {
				echo json_encode([
					'success' => false,
				]);
				die;
			}
			echo json_encode([
				'success' => $this->contracts_model->remove_comment($id),
			]);
		} else {
			echo json_encode([
				'success' => false,
			]);
		}
	}

	public function renew() {
		if (!has_permission('contracts', '', 'create') && !has_permission('contracts', '', 'edit')) {
			access_denied('contracts');
		}
		if ($this->input->post()) {
			$data = $this->input->post();
			$success = $this->contracts_model->renew($data);
			if ($success) {
				set_alert('success', _l('contract_renewed_successfully'));
			} else {
				set_alert('warning', _l('contract_renewed_fail'));
			}
			redirect(admin_url('contracts/contract/' . $data['contractid'] . '?tab=renewals'));
		}
	}

	public function delete_renewal($renewal_id, $contractid) {
		$success = $this->contracts_model->delete_renewal($renewal_id, $contractid);
		if ($success) {
			set_alert('success', _l('contract_renewal_deleted'));
		} else {
			set_alert('warning', _l('contract_renewal_delete_fail'));
		}
		redirect(admin_url('contracts/contract/' . $contractid . '?tab=renewals'));
	}

	public function copy($id) {
		if (!has_permission('contracts', '', 'create')) {
			access_denied('contracts');
		}
		if (!$id) {
			redirect(admin_url('contracts'));
		}
		$newId = $this->contracts_model->copy($id);
		if ($newId) {
			set_alert('success', _l('contract_copied_successfully'));
		} else {
			set_alert('warning', _l('contract_copied_fail'));
		}
		redirect(admin_url('contracts/contract/' . $newId));
	}

	/* Delete contract from database */
	public function delete($id) {
		if (!has_permission('contracts', '', 'delete')) {
			access_denied('contracts');
		}
		if (!$id) {
			redirect(admin_url('contracts'));
		}
		$response = $this->contracts_model->delete($id);
		if ($response == true) {
			set_alert('success', _l('deleted', _l('contract')));
		} else {
			set_alert('warning', _l('problem_deleting', _l('contract_lowercase')));
		}
		if (strpos($_SERVER['HTTP_REFERER'], 'clients/') !== false) {
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect(admin_url('contracts'));
		}
	}

	/* Manage contract types Since Version 1.0.3 */
	public function type($id = '') {
		if (!is_admin() && get_option('staff_members_create_inline_contract_types') == '0') {
			access_denied('contracts');
		}
		if ($this->input->post()) {
			if (!$this->input->post('id')) {
				$id = $this->contracts_model->add_contract_type($this->input->post());
				if ($id) {
					$success = true;
					$message = _l('added_successfully', _l('contract_type'));
				}
				echo json_encode([
					'success' => $success,
					'message' => $message,
					'id' => $id,
					'name' => $this->input->post('name'),
				]);
			} else {
				$data = $this->input->post();
				$id = $data['id'];
				unset($data['id']);
				$success = $this->contracts_model->update_contract_type($data, $id);
				$message = '';
				if ($success) {
					$message = _l('updated_successfully', _l('contract_type'));
				}
				echo json_encode([
					'success' => $success,
					'message' => $message,
				]);
			}
		}
	}

	public function types() {
		if (!is_admin()) {
			access_denied('contracts');
		}
		if ($this->input->is_ajax_request()) {
			$this->app->get_table_data('contract_types');
		}
		$data['title'] = _l('contract_types');
		$this->load->view('admin/contracts/manage_types', $data);
	}

	/* Delete announcement from database */
	public function delete_contract_type($id) {
		if (!$id) {
			redirect(admin_url('contracts/types'));
		}
		if (!is_admin()) {
			access_denied('contracts');
		}
		$response = $this->contracts_model->delete_contract_type($id);
		if (is_array($response) && isset($response['referenced'])) {
			set_alert('warning', _l('is_referenced', _l('contract_type_lowercase')));
		} elseif ($response == true) {
			set_alert('success', _l('deleted', _l('contract_type')));
		} else {
			set_alert('warning', _l('problem_deleting', _l('contract_type_lowercase')));
		}
		redirect(admin_url('contracts/types'));
	}

	public function add_contract_attachment($id) {
		handle_contract_attachment($id);
	}

	public function add_external_attachment() {
		if ($this->input->post()) {
			$this->misc_model->add_attachment_to_database(
				$this->input->post('contract_id'),
				'contract',
				$this->input->post('files'),
				$this->input->post('external'));
		}
	}

	public function delete_contract_attachment($attachment_id) {
		$file = $this->misc_model->get_file($attachment_id);
		if ($file->staffid == get_staff_user_id() || is_admin()) {
			echo json_encode([
				'success' => $this->contracts_model->delete_contract_attachment($attachment_id),
			]);
		}
	}
	public function pile_contract($id){
		$CI = &get_instance();
		$invoi = $CI->db->query('SELECT * FROM btl_coc WHERE trang_thai=0 AND khach_hang='.$id)->result_array();
		echo json_encode([
				'invoi' => $invoi,
			]);
	}
}
