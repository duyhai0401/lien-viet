<?php
use yidas\queue\worker\Controller as Admin_controller;

class My_worker extends Admin_controller {
	// Initializer
	// Setting for that a listener could fork up to 10 workers
	public $workerMaxNum = 10;

	// Enable text log writen into specified file for listener and worker
	public $logPath = 'tmp/my-worker.log';

	protected function init() {
		// Optional autoload (Load your own libraries or models)
		$this->load->library('myjobs');
	}

	// Worker
	protected function handleWork() {
		// Your own method to get a job from your queue in the application
		$job = $this->myjobs->popJob();

		// return `false` for job not found, which would close the worker itself.
		if (!$job) {
			return false;
		}

		// Your own method to process a job
		$this->myjobs->processJob($job);

		// return `true` for job existing, which would keep handling.
		return true;
	}

	// Listener
	protected function handleListen() {
		// Your own method to detect job existence
		// return `true` for job existing, which leads to dispatch worker(s).
		// return `false` for job not found, which would keep detecting new job
		return $this->myjobs->exists();
	}
}