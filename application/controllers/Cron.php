<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cron extends CRM_Controller
{
    public function __construct()
    {
        parent::__construct();
        update_option('cron_has_run_from_cli', 1);
        $this->load->model('Bytesoft_notify_model');
        $this->load->model('Cron_model');
        $this->load->model('invoices_model');
    }

    public function index($key = '')
    {
        if (defined('APP_CRON_KEY') && (APP_CRON_KEY != $key)) {
            header('HTTP/1.0 401 Unauthorized');
            die('Passed cron job key is not correct. The cron job key should be the same like the one defined in APP_CRON_KEY constant.');
        }

        $last_cron_run = get_option('last_cron_run');
        if ($last_cron_run == '' || (time() > ($last_cron_run + do_action('cron_functions_execute_seconds', 300)))) {
            do_action('before_cron_run');

            $this->load->model('cron_model');
            $this->cron_model->run();

            do_action('after_cron_run');
        }
    }
    public function check_sms() {
      print_r("expression");
        $this->Cron_model->run();
    }
    public function test_hd() {

        $this->load->view('admin/expenses/test');
    }
    public function test() {
       print_r("expression");
    $date = date('Y-m-d');
    $ko = Date("Y-m-01", strtotime($date . "-3 day"));
    print_r($ko);
    $data = $this->db->where('dateend >=',$date)->where('datestart <',$ko)->get('tblcontracts')->result_array();
// $data = $this->db->where('dateend >=',$date)->get('tblcontracts')->result_array();
foreach ($data as $data1) {
  
   $description = $data1['description'];
      $client = $data1['client'];
      $contract_value = $data1['contract_value'];
      $name_building = $data1['name_building'];
      $deposit = $data1['deposit'];
      $thue = $data1['contract_value'];
      $recurring = $data1['recurring'];
      $datestart = $data1['datestart'];
      $dateend = $data1['dateend'];
      $address_vp = $data1['address_vp'];
      $sale_agent = $data1['sale_agent'];
      $floor = $data1['floor'];
      
      $cycle = $data1['tota'];
      $id = $data1['id'];
      $hom = date('Y-m-d');
      $ngay = new DateTime($data1['datestart']);
      $ngay1 = new DateTime($hom);
      $interval = date_diff($ngay, $ngay1);
      $date_y = $interval->format('%R%Y');
      $date_m = $interval->format('%R%m');
      $month = ($date_y * 12) + $date_m;
     
      $invoi_tye = 2;
      $cycle = 1;
      $data_item = $this->db->where('rel_id',$id)->get('tbl_contracts_item')->result_array();
      
      $number = get_option('next_invoice_number');
      foreach ($data_item as $data1_item) {
        $tien_invoi = 0;
        $ndv = $this->db->where('id',$data1_item['group_id'])->get('tblitems')->result_array();
        $nd = $this->vn_to_str($ndv[0][description])."_DV";
        
        if ($data1_item['item']>50&&$data1_item['item']<1000) {
          $number++;
          $id_it = $data1_item['item'];
          $invoice_data = [
        'clientid' => $client,
        'datecreated' => $date,
        'date' => $date,
        'currency' => '1',
        'recurring' => $recurring,
        'cycles' => 0,
        'subtotal' => 0,
        'total' => 0,
        'name_building' => $floor,
        'total_tax' => 0,
        'duedate' => $date,
        'number' => $number,
        'invoi_tye' => 2,
        'status' => 1,
        'title_payment' => $nd,
        'allowed_payment_modes' => ["1", "2", "stripe"],
      ];
         $idt = $this->invoices_model->add($invoice_data);
        $this->db->where('id',$idt);
    $this->db->update('tblinvoices', [
      'status' => 1,
    ]);
          $data_id =$this->db->order_by('id','desc')->limit(1)->get('tblinvoices')->result_array();
          $data1_id = $data_id[0]['id'];
          $data_invoice = $this->db->where('rel_id',$id)->get('tbl_contracts_item')->result_array();
          foreach ($data_invoice as $data1_invoice) {
            
           if($data1_invoice['item']<50||$data1_invoice['item']>1000){
        // print_r($data1_invoice['item']);
        $rel_id = $data1_id;
        $rel_type = "invoice";
        $description = $data1_invoice['description'];
        $long_description = $data1_invoice['long_description'];
        $date = date('Y-m-d');
        $rate = $data1_invoice['rate'];
        $group_id = $id_it;
        $item = $data1_invoice['item'];
        if($data1_invoice['item']<3){
           $qty = 0;
        }else{
           $qty = 1;
        }
        $tien_invoi = $tien_invoi + $data1_invoice['rate']*$qty;
        $CI = &get_instance();
    $CI->db->insert('tblitems_in', [
        'rel_id' => $rel_id,
        'rel_type' => $rel_type,
        'description' =>$description,
        'long_description' => $long_description,
        'qty' => $qty,
        'rate' => $rate,
        'group_id' => $group_id,
        'item' => $item,
        'date_in' => $date,
      ]);
       
       }
          }
        

       $ii = $data1_id;
     
        $this->db->where('id', $ii);
      $this->db->update('tblinvoices', [
        'subtotal' => $tien_invoi,
        'total' => $tien_invoi,
        'status' => 1,
      ]);
    
    $ta = $this->db->where('rel_id',$ii)->get('tblitems_in')->result_array();
    if($ta){
    }else{
    $this->db->query("DELETE FROM `tblinvoices` WHERE `id` = $ii");
    }
        }
       
        
        
      }
}

$date_time = date('Y-m-d');
$time = getDate();
$thang = $time['mon']-1;
$nam = $time['year'];
if($thang==2){
  $thang_truoc = 12;
  $nam_truoc = $time['year']-1;
}elseif($thang==1){
 $thang_truoc = 11;
  $nam_truoc = $time['year']-1;
}
else{
  $thang_truoc = $thang-1;
  $nam_truoc = $time['year'];
}

$value= $this->db->where('nam',$nam)->where('thang',$thang)->get('tblmoney_sv')->result_array();

foreach ( $value as $value1) {
    $id_name_building=$value1['id_name_building'];
 

    $value_truoc= $this->db->where('nam',$nam_truoc)->where('thang',$thang_truoc)->where('id_name_building',$id_name_building)->get('tblmoney_sv')->result_array();
  foreach ( $value_truoc as $data_truoc) {
    $dien = $value1['dien']-$data_truoc['dien'];
    $nuoc = $value1['nuoc']-$data_truoc['nuoc'];
    $update_dien = $this->db->where('date_in',$date_time)->where('group_id',$id_name_building)->where('item',1)->update('tblitems_in', [
        'qty' => $dien,
      ]);
    $update_nuoc = $this->db->where('date_in',$date_time)->where('group_id',$id_name_building)->where('item',2)->update('tblitems_in', [
        'qty' => $nuoc,
      ]);
    $data_invoi= $this->db->where('group_id',$id_name_building)->where('date_in',$date_time)->limit(1)->get('tblitems_in')->result_array();
   echo '<pre>';
    $rel = $data_invoi[0]['rel_id'];
    print_r($rel);
    $data_data = $this->db->where('id',$rel)->get('tblinvoices')->result_array();
    
 $select_invoi_1= $this->db->where('date_in',$date_time)->where('group_id',$id_name_building)->where('rel_id',$rel)->get('tblitems_in')->result_array();
 $tien_thue =0;
 $tien_tong=0;
 foreach ($select_invoi_1 as $data_data_1) {
   $gro = $data_data_1['group_id'];
    $itm = $data_data_1['item'];
   
      if($gro==$itm){
        $tien_thue=$data_data_1['qty']*$data_data_1['rate']*0.1;
      }
    
    $tien_tong = $tien_tong+($data_data_1['qty']*$data_data_1['rate']);
 }
 $mo = $tien_tong+$tien_thue;
$up_voi= $this->db->where('id',$rel)->update('tblinvoices', [
        'subtotal' => $tien_tong,
        'total' => $mo,
        'status' => 1,
      ]);

  }
    
}
  }
    private function vn_to_str($str)
    {

        $unicode = array(

            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

            'd' => 'đ',

            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

            'i' => 'í|ì|ỉ|ĩ|ị',

            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

            'y' => 'ý|ỳ|ỷ|ỹ|ỵ',

            'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'D' => 'Đ',

            'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'I' => 'Í|Ì|Ỉ|Ĩ|Ị',

            'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );

        foreach ($unicode as $nonUnicode => $uni) {

            $str = preg_replace("/($uni)/i", $nonUnicode, $str);

        }
//        $str = str_replace(' ', ' ', $str);

        return $str;

    }
    function get_expire_pile_contract()
    {
        $CI = &get_instance();
        $where_own = array();
        $today = date('Y-m-d'); 
// $today = "2019-03-05"; 
        $plus_7_days = date('Y-m-d', strtotime("+3 days"));
// $plus_7_days = "2019-03-16";
        $total=total_rows('btl_coc','han_coc BETWEEN "'.$today.'" AND "'.$plus_7_days.'" AND han_coc is NOT NULL');
        $contract = $CI->db->query('SELECT * FROM btl_coc WHERE han_coc BETWEEN "'.$today.'" AND "'.$plus_7_days.'" AND han_coc is NOT NULL')->result_array();
        if(sizeof($contract)>0){
            foreach ($contract as $key => $value) {
                $add= add_notification([
                    'description' => 'pile_contract_expired',
                    'fromcompany' => 1,
                    'fromuserid' => null,
                    'touserid'=>$value['nguoi_ban'],
                    'link' => 'expenses/tien_coc/' . $value['id_coc'],
                    'additional_data' => serialize([
                        '<b>' .$value['hopdong']. '</b>',
                    ]),
                ]);

            }
        }
        //kiem tra hop dong coc het han ngay hom truoc va doi trang thai
        
        $ex_contracts=$CI->db->query('SELECT * FROM btl_coc WHERE han_coc= "'.date('Y-m-d', strtotime("-1 days")).'" AND han_coc is NOT NULL AND trang_thai =1')->result_array();
        if(sizeof($ex_contracts)>0){
            foreach ($ex_contracts as $key => $value) {
                    $this->db->where('id_coc',$value['id_coc']);
                    $this->db->update('btl_coc', [
                        'trang_thai' => 4,
                        'date_update' => date('Y-m-d'),
                    ]);

            }
        }
        return $total;
    }
    function get_expire_contract()
    {
        $CI = &get_instance();
        $where_own = array();
        $today = date('Y-m-d'); 
// $today = "2019-03-05"; 
        $plus_7_days = date('Y-m-d', strtotime("+7 days"));
// $plus_7_days = "2019-03-16";
        $total=total_rows('tblcontracts','dateend BETWEEN "'.$today.'" AND "'.$plus_7_days.'" AND trash=0 AND dateend is NOT NULL');
        $contract = $CI->db->query('SELECT * FROM tblcontracts WHERE dateend BETWEEN "'.$today.'" AND "'.$plus_7_days.'" AND trash=0 AND dateend is NOT NULL')->result_array();
        if(sizeof($contract)>0){
            foreach ($contract as $key => $value) {
                $add= add_notification([
                    'description' => 'contract_expired',
                    'fromcompany' => 1,
                    'fromuserid' => null,
                    'touserid'=>$value['addedfrom'],
                    'link' => 'contracts/contract/' . $value['id'],
                    'additional_data' => serialize([
                        '<b>' .$value['subject']. '</b>',
                    ]),
                ]);
                if($value['sale_agent']!==0 && $value['sale_agent']!==$value['addedfrom']){
                    $add= add_notification([
                        'description' => 'contract_expired',
                        'fromcompany' => 1,
                        'fromuserid' => null,
                        'touserid'=>$value['sale_agent'],
                        'link' => 'contracts/contract/' . $value['id'],
                        'additional_data' => serialize([
                            '<b>' .$value['subject']. '</b>',
                        ]),
                    ]);

                }
            }
        }


        return $total;
    }
}
