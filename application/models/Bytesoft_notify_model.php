<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Bytesoft_notify_model extends CRM_Model
{

    public $ROOT_URL = 'https://service.bytesoft.net/api/';

    public function __construct()
    {
        parent::__construct();
    }


    public function get($id = '', $where = [])
    {
        $this->db->select('*, tblcurrencies.id as currencyid, tblinvoices.id as id, tblcurrencies.name as currency_name');
        $this->db->from('tblinvoices');
        $this->db->join('tblcurrencies', 'tblcurrencies.id = tblinvoices.currency', 'left');
        $this->db->where($where);
        if (is_numeric($id)) {
            $this->db->where('tblinvoices' . '.id', $id);
            $invoice = $this->db->get()->row();
            // var_dump($invoice);
            if ($invoice) {
                $invoice->total_left_to_pay = get_invoice_total_left_to_pay($invoice->id, $invoice->total);

                $invoice->items = get_items_by_type('invoice', $id);

                if ($invoice->project_id != 0) {
                    $this->load->model('projects_model');
                    $invoice->project_data = $this->projects_model->get($invoice->project_id);
                }

                $client = $this->clients_model->get($invoice->clientid);
                $invoice->client = $client;
                if (!$invoice->client) {
                    $invoice->client = new stdClass();
                    $invoice->client->company = $invoice->deleted_customer_name;
                }

                $this->load->model('payments_model');
                $invoice->payments = $this->payments_model->get_invoice_payments($id);
            }
            return $invoice;
        }

        $this->db->order_by('number,YEAR(date)', 'desc');

        return $this->db->get()->result_array();
    }


    /**
     *
     * Send Notice to Customer
     *
     * @param $id
     * @return bool
     */

    public function send($id)
    {

        $invoice = $this->get($id);
        $this->db->where('id', $id);
        $this->db->update('tblinvoices', [
            'last_overdue_reminder' => date('Y-m-d'),
        ]);

        // var_dump('RUN');

        // Check Phone Number of customer
        if ($invoice->client->phonenumber != null && $invoice->client->phonenumber != "") {
            if ($invoice->invoi_tye == 1) {
                // var_dump("RUN TYPE 1");
                $this->sms($invoice->client->phonenumber, 'sms_invoice_due', $invoice);
                $this->zalo($invoice->client->phonenumber, 'zalo_invoice_due', $invoice);
            } elseif ($invoice->invoi_tye == 2) {
                // var_dump("RUN TYPE 1");
                $this->sms($invoice->client->phonenumber, 'sms_invoice_service', $invoice);
                $this->zalo($invoice->client->phonenumber, 'zalo_invoice_service', $invoice);
                // print_r($this->zalo($invoice->client->phonenumber, 'zalo_invoice_service', $invoice));
            }
        }
        return "Done";
    }


    /**
     *
     * Send SMS
     *
     * Type of Notify
     *
     * --- sms_invoice_due : When due invoice payment
     * --- sms_content_utilities_invoice : Payment Ultility every month
     *
     */

    private function sms($to, $type, $arr)
    {

        $content = "";

        switch ($type) {
            case 'sms_invoice_due':

                $content = $this->sms_content_due_invoice(date('d-m-Y', strtotime($arr->date)), date('d-m-Y', strtotime($arr->duedate)), $this->vn_to_str($arr->items[0][description]), date('d-m-Y', strtotime($arr->date)), date('d-m-Y', strtotime($arr->date . ' + 3 days')), number_format($arr->total_left_to_pay), $arr->title_payment);
                break;
            case 'sms_invoice_service':
                $service_list = [];
                foreach ($arr->items as $service) {
                    $service_list[] = $this->vn_to_str($service["description"]) . ' : ' . $service["qty"] . ' x ' . number_format($service["rate"]) . ' = ' . number_format($service["qty"] * $service["rate"]) . " VND.\n";
                }
                $se = "";
                foreach ($service_list as $value) {
                    $se = $se."\n".$value;
                }
                $time = date('n')-1;
                $content = $this->sms_content_utilities_invoice(date('d-m-Y', strtotime($arr->date)), date('d-m-Y', strtotime($arr->duedate)), $arr->items[0][description], date('d-m-Y', strtotime($arr->date)), date('d-m-Y', strtotime($arr->date . ' + 3 days')), number_format($arr->total_left_to_pay), $arr->title_payment, $se,$time);
                break;
        }

        $json = json_encode(array('to' => $to, 'smsContent' => $content));

        $headers = array('Content-type: application/json');

        $url = $this->ROOT_URL . 'sms';
        $http = curl_init($url);
        curl_setopt($http, CURLOPT_HEADER, false);
        curl_setopt($http, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($http, CURLOPT_POSTFIELDS, $json);
        curl_setopt($http, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($http, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($http, CURLOPT_VERBOSE, 0);
        curl_setopt($http, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($http, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($http, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($http);

        if (curl_errno($http)) {
            return "null";
        } else {
            curl_close($http);
            return json_decode($result, true);
        }
    }


    private function zalo($to, $type, $arr)
    {
        switch ($type) {
            case 'zalo_invoice_due':
                $content = $this->zalo_content_due_invoice(date('d-m-Y', strtotime($arr->date)), date('d-m-Y', strtotime($arr->duedate . '+ 0 months')), $arr->items[0][description], date('d-m-Y', strtotime($arr->date)), date('d-m-Y', strtotime($arr->date . ' + 3 days')), number_format($arr->total_left_to_pay), $arr->title_payment);
                break;
            case 'zalo_invoice_service':
                 $service_list = [];
                foreach ($arr->items as $service) {
                    $service_list[] = $this->vn_to_str($service["description"]) . ' : ' . $service["qty"] . ' x ' . number_format($service["rate"]) . ' = ' . number_format($service["qty"] * $service["rate"]) . " VND.\n";
                }
                $se = "";
                foreach ($service_list as $value) {
                    $se = $se."\n".$value;
                }
                $time = date('n')-1;
             
           $ndv = $this->db->where('id',$arr->items[0]['group_id'])->get('tblitems')->result_array();
          

                $content = $this->zalo_content_utilities_invoice(date('d-m-Y', strtotime($arr->date)), date('d-m-Y', strtotime($arr->duedate . '+ 0 months')), $arr->items[0][description], date('d-m-Y', strtotime($arr->date)), date('d-m-Y', strtotime($arr->date . ' + 3 days')), number_format($arr->total_left_to_pay), $arr->title_payment, $se, $time);
                break;

        }

        $json = json_encode(array('phone' => $to, 'param' => array('message' => $content), 'type' => 'text'));


        $headers = array('Content-type: application/json');

        $url = $this->ROOT_URL . 'zalo';
        $http = curl_init($url);
        curl_setopt($http, CURLOPT_HEADER, false);
        curl_setopt($http, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($http, CURLOPT_POSTFIELDS, $json);
        curl_setopt($http, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($http, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($http, CURLOPT_VERBOSE, 0);
        curl_setopt($http, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($http, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($http, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($http);
        if (curl_errno($http)) {
            return "null";
        } else {
            curl_close($http);
            return json_decode($result, true);
        }
    }

    private function sms_content_due_invoice($start, $end, $address, $pay_start, $pay_expressed, $total, $content)
    {
        return "BDS Lien Viet thong bao tien thue nha tu $start toi $end - $address.\nHan thanh toan: Tu $pay_start Toi $pay_expressed.\nTong thanh toan: $total VND.\nNoi dung chuyen khoan: $content.\nNhan vien ho tro: Ms . Hang - 0973502096\n---\nSTK: 12510000633317\nTran Van Nam\nBIDV chi nhanh Dong Do";
    }

    private function sms_content_utilities_invoice($start, $end, $address, $pay_start, $pay_expressed, $total, $content, $service,$time)
    {
        return "BĐS Lien Viet thong bao tien dich vu thang $time \nHan thanh toan: Tu $pay_start Toi $pay_expressed.\n$service Tong thanh toan: $total  VND.\nNội dung chuyển khoản: $content.\nNhan vien ho tro: Ms . Hang - 0973502096\n---\nSTK: 12510000633317\nTran Van Nam\nBIDV chi nhanh Dong Do";
    }

    private function zalo_content_due_invoice($start, $end, $address, $pay_start, $pay_expressed, $total, $content)
    {
        return "BĐS LIÊN VIỆT THÔNG BÁO.\nTiền thuê nhà từ $start tới $end - $address.\nHạn thanh toán: Từ ngày $pay_start tới ngày $pay_expressed.\nTổng thanh toán: $total VND.\nNội dung chuyển khoản: $content.\nNhân viên hỗ trợ: Ms . Hằng - 0973502096\n---\nSố tài khoản: 12510000633317\nChủ tài khoản: Trần Văn Nam\nNgân hàng Đầu tư và Phát triển Việt Nam BIDV chi nhánh Đông Đô";

    }

    private function zalo_content_utilities_invoice($start, $end, $address, $pay_start, $pay_expressed, $total, $content, $service, $time)
    {
        return "BĐS LIÊN VIỆT THÔNG BÁO\nTiền dịch vụ tháng $time \nHạn thanh toán: Từ $pay_start đến $pay_expressed.\n$service Tổng thanh toán: $total VND.\nNội dung chuyển khoản: $content.\nNhân viên hỗ trợ: Ms . Hằng - 0973502096\n---\nSố tài khoản: 12510000633317\nChủ tài khoản: Trần Văn Nam\nNgân hàng Đầu tư và Phát triển Việt Nam BIDV chi nhánh Đông Đô";
    }


    private function vn_to_str($str)
    {

        $unicode = array(

            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

            'd' => 'đ',

            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

            'i' => 'í|ì|ỉ|ĩ|ị',

            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

            'y' => 'ý|ỳ|ỷ|ỹ|ỵ',

            'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'D' => 'Đ',

            'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'I' => 'Í|Ì|Ỉ|Ĩ|Ị',

            'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );

        foreach ($unicode as $nonUnicode => $uni) {

            $str = preg_replace("/($uni)/i", $nonUnicode, $str);

        }
//        $str = str_replace(' ', ' ', $str);

        return $str;

    }


}