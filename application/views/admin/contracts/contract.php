<?php init_head();?>
<style type="text/css">
   .red{
      border-color: red!important;
   }
</style>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-5 left-column">
            <div class="panel_s">
               <div class="panel-body">
                  <div class="form_error">

                     <?php 
                     if(isset($error)&& $error!==""){
                       ?>
                       <div class="alert alert-danger"><?php echo $error;?></div>
                       <?php
                    }
                    ?>

                 </div>
                 <?php echo form_open($this->uri->uri_string(), array('id' => 'contract-form')); ?>
           <!--        <div class="form-group">
                     <div class="checkbox checkbox-primary no-mtop checkbox-inline">
                        <input type="checkbox" id="trash" name="trash" data-toggle="tooltip" title="<?php echo _l('contract_trash_tooltip'); ?>" <?php if (isset($contract)) {if ($contract->trash == 1) {echo 'checked';}}
;?>>
                        <label for="trash"><?php echo _l('contract_trash'); ?></label>
                     </div>
                     <div class="checkbox checkbox-primary checkbox-inline">
                        <input type="checkbox" name="not_visible_to_client" id="not_visible_to_client" <?php if (isset($contract)) {if ($contract->not_visible_to_client == 1) {echo 'checked';}}
;?>>
                        <label for="not_visible_to_client"><?php echo _l('contract_not_visible_to_client'); ?></label>
                     </div>
                  </div> -->
                <!--   <div class="checkbox checkbox-primary checkbox-inline">
                     <input <?php if($contract->de_nghi=="on"){ echo "checked";} ?> type="checkbox" name="de_nghi" id="not_visible_to_client">
                     <label for="not_visible_to_client">Tạo đề nghị thanh toán</label>
                  </div></br> -->

                  <label>Tòa nhà</label>
                  <select onchange="group_in()" name="floor" id="group" class="selectpicker _select_input_group" data-width="100%" data-none-selected-text="Không có mục nào được chọn" data-live-search="true" tabindex="-98">
                     <option value=""></option>
                     <?php foreach ($group as $value) {
                        if($contract->floor==$value['id']){
                           echo '<option selected value="'.$value['id'].'">'.$value['name'].'</option>';
                        }else{
                           echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                        }
                     }
                     
                     ?>
                  </select>
                  
                  <div class="form-group" app-field-wrapper="subject">
                   <label for="subject" class="control-label">Văn phòng</label>
                   <input type="text" name="id_buil" class="hidden" value="<?php echo $contract->name_building; ?>">
                   <select id="name_building" name="name_building" <?php if($id==null){
                     echo "required ";
                   } ?>class="form-control" onchange="myFunction_chage2()">
                     <option value=""></option>


                  </select>
                  <label for="subject" class="control-label">Dịch vụ</label>
                  <input type="text" name="group" id="name_gr" value="<?php echo $contract->name_building;?>" class="hidden">
                  <select id="service" name="service" onchange="myFunction_chage()" class="selectpicker _select_input_group" data-width="100%" data-none-selected-text="Không có mục nào được chọn" data-live-search="true" tabindex="-98">
                     <option value=""></option>
                     <?php
                     foreach ($dich_vu as $key => $value) {
                        echo '<option value="' . $value['id'] . '">' . $value['description'] . '</option>';
                     }

                     ?>

                  </select>

                  <div>
                     <table id="p" class="table">
                        <tr>
                           <th>Tên</th>
                           
                           <th>Giá</th>
                        </tr>
                        <?php

                        if($item_contract){
                           foreach ($item_contract as $key => $value) {
                              $rate=number_format($value['rate']);
                              $rate=$rate;
                             echo "<tr>
                             <td><input type='text' readonly name='name_dv[]' value='".$value['description']."' class='form-control'/><input type='text' class='hidden' name='id_item[]' value='".$value['item']."' /><input type='text' class='hidden' name='iditem[]' value='".$value['id']."' /></td>

                             <td><input type='text' name='rate_dv[]' class='rate' value='".$rate."'  class='form-control'/></td>
                             <td></td></tr>"; 
                          }

                       }
                       ?>
                    </table>
                 </div>
              </div>

              <div class="form-group">
               <label>Thuế</label>
               <select id="address_vp" name="address_vp" class="selectpicker _select_input_group" data-width="100%" data-none-selected-text="Không có mục nào được chọn" data-live-search="true" tabindex="-98">
                  <option value=""></option>
                  <?php
                  foreach ($taxd as $key => $value) {
                     if ($value['taxrate'] == $contract->address_vp) {
                        echo '<option selected value="' . $value['taxrate'] . '">' . $value['name'] . '</option>';
                     } else {
                        echo '<option  value="' . $value['taxrate'] . '">' . $value['name'] . '</option>';
                     }
                  }

                  ?>

               </select>

            </div>
            <div class="form-group select-placeholder" id="client-form">
               <label for="clientid" class="control-label"><span class="text-danger">* </span><?php echo _l('contract_client_string'); ?></label><div class="input-group-addon" style="width: 10%;opacity: 1;float: right;"><a href="#" data-toggle="modal" data-target="#sales_item_modal"><i class="fa fa-plus"></i></a></div>
               <select id="clientid" name="client" data-live-search="true" data-width="100%" class="ajax-search" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                  <?php $selected = (isset($contract) ? $contract->client : '');
                  if ($selected == '') {
                     $selected = (isset($customer_id) ? $customer_id : '');
                  }
                  if ($selected != '') {
                     $rel_data = get_relation_data('customer', $selected);
                     $rel_val = get_relation_values($rel_data, 'customer');
                     echo '<option value="' . $rel_val['id'] . '" selected>' . $rel_val['name'] . '</option>';
                  }?>
               </select>

            </div>
            <?php $value = (isset($contract) ? $contract->subject : '');?>
            <i class="fa fa-question-circle pull-left" data-toggle="tooltip" title="<?php echo _l('contract_subject_tooltip'); ?>"></i>
            <?php echo render_input('subject', 'contract_subject', $value); ?>
            <div class="form-group hidden">
               <label for="contract_value"><?php echo _l('contract_value'); ?></label>
               <div class="input-group" data-toggle="tooltip" title="<?php echo _l('contract_value_tooltip'); ?>">
                  <input type="number" readonly class="form-control " name="contract_value" id="contract_value" value="<?php if (isset($contract)) {echo $contract->contract_value;}?>">
                  <div class="input-group-addon">
                     <?php echo $base_currency->symbol; ?>
                  </div>
               </div>
            </div>
            <?php
            $i = 0;
            $selected = 'selected';
            foreach ($staff as $member) {
               if (isset($contract)) {
                  if ($contract->sale_agent == $member['staffid']) {
                     $selected = $member['staffid'];
                  }
               }
               $i++;
            }
            echo render_select('sale_agent', $staff, array('staffid', array('firstname', 'lastname')), 'sale_agent_string', $selected);
            ?>
            <div class="form-group">
               <label>Tiền cọc</label>

               <select id="deposit" name="deposit"  class="form-control">
                  <option value=""></option>
                  <?php
                  if(sizeof($coc)>0){
                     echo '<option disabled selected>Chọn hợp đồng cọc</option>';
                     foreach ($coc as $key) {
                       if ($contract->deposit == $key['id_coc']) {
                        echo '<option selected value="'.$key['id_coc'].'">'.$key['hopdong'].'</option>';
                     }else{
                        echo '<option value="'.$key['id_coc'].'">'.$key['hopdong'].'</option>';
                     }
                  }
               }else{
                  echo '<option disabled selected>'._l('none_pile_contract').'</option>';
               }
               ?>
            </select>
         </div>

         <?php if ($contract->shipping_street) {
            echo '<div class="form-group" app-field-wrapper="subject"><label for="subject" class="control-label"> <small class="req text-danger">* </small>Địa Chỉ</label><input type="text" readonly class="form-control" value="';
            echo $contract->shipping_street;
            echo "-";
            echo $contract->shipping_city;
            echo '" aria-invalid="false"></div>';
         }?>
         <?php if ($contract->phonenumber) {
            echo '<div class="form-group" app-field-wrapper="subject"><label for="subject" class="control-label"> <small class="req text-danger">* </small>Số Điện Thoại</label><input type="text" readonly class="form-control" value="';
            echo $contract->phonenumber;
            echo '" aria-invalid="false"></div>';
         }?>
         <?php if ($contract->vat) {
            echo '<div class="form-group" app-field-wrapper="subject"><label for="subject" class="control-label"> <small class="req text-danger">* </small>Mã Số Thuế/CMND</label><input type="text" readonly class="form-control" value="';
            echo $contract->vat;
            echo '" aria-invalid="false"></div>';
         }?>
         <div class="form-group">
            <label>Kỳ thanh toán</label>
            <select class="selectpicker" <?php if($contract->recurring){echo "disabled";}?> data-width="100%" name="recurring" data-none-selected-text="Không có mục nào được chọn" tabindex="-98">
               <option <?php if ($contract->recurring == 1) {
                  echo "selected";
               }
               ?> value="1">Mỗi 1 tháng</option>
               <option <?php if ($contract->recurring == 2) {
                  echo "selected";
               }
               ?> value="2">Mỗi 2 tháng</option>
               <option <?php if ($contract->recurring == 3) {
                  echo "selected";
               }
               ?> value="3">Mỗi 3 tháng</option>
               <option <?php if ($contract->recurring == 4) {
                  echo "selected";
               }
               ?> value="4">Mỗi 4 tháng</option>
               <option <?php if ($contract->recurring == 5) {
                  echo "selected";
               }
               ?> value="5">Mỗi 5 tháng</option>
               <option <?php if ($contract->recurring == 6) {
                  echo "selected";
               }
               ?> value="6">Mỗi 6 tháng</option>
               <option <?php if ($contract->recurring == 7) {
                  echo "selected";
               }
               ?> value="7">Mỗi 7 tháng</option>
               <option <?php if ($contract->recurring == 8) {
                  echo "selected";
               }
               ?> value="8">Mỗi 8 tháng</option>
               <option <?php if ($contract->recurring == 9) {
                  echo "selected";
               }
               ?> value="9">Mỗi 9 tháng</option>
               <option <?php if ($contract->recurring == 10) {
                  echo "selected";
               }
               ?> value="10">Mỗi 10 tháng</option>
               <option <?php if ($contract->recurring == 11) {
                  echo "selected";
               }
               ?> value="11">Mỗi 11 tháng</option>
               <option <?php if ($contract->recurring == 12) {
                  echo "selected";
               }
               ?> value="12">Mỗi 12 tháng</option>
            </select>
         </div>
         <?php
         $selected = (isset($contract) ? $contract->contract_type : '');
         if (is_admin() || get_option('staff_members_create_inline_contract_types') == '1') {
            echo render_select_with_input_group('contract_type', $types, array('id', 'name'), 'contract_type', $selected, '<a href="#" onclick="new_type();return false;"><i class="fa fa-plus"></i></a>');
         } else {
            echo render_select('contract_type', $types, array('id', 'name'), 'contract_type', $selected);
         }
         ?>
         <div class="row hidden" id="phi_phat_sinh">
   <div class="col-md-12">
      <label>Phí phát sinh</label>
      <table id="p" class="table">
                           <tr>
                           <th>Tên</th>
                           
                           <th>Giá</th>
                           </tr>
                           <?php
                           if($contract->contract_type==3||$contract->contract_type==4){
                              $show="none";
                           }else{
                              $show="";
                           }
                           if($item_contract){
                        foreach ($item_contract as $key => $value) {
                         echo "<tr>
                           <td><input type='text' readonly name='name_phi[]' value='".$value['description']."' class='form-control'/><input type='text' class='hidden' name='id_item_phi[]' value='".$value['item']."' /><input type='text' class='hidden' name='iditem_phi[]' value='".$value['id']."' /></td>
                           
                           <td><input type='text' name='rate_phi[]' class='rate' value='".number_format($value['rate'])."'  class='form-control'/></td>
                           <td></td></tr>"; 
                        }
                           echo "<tr>
                           <td><input type='text' name='name_phi[]' value='' class='form-control'/><input type='text' class='hidden' name='id_item_phi[]' value='' /><input type='text' class='hidden' name='iditem_phi[]' value='' /></td>
                           
                           <td><input type='text' name='rate_phi[]' class='rate' value=''  class='form-control'/></td>
                           <td></td></tr>";
                           }
                          
                           ?>
                        </table>
   </div>
</div>
         <div class="row">
            <div class="col-md-12">
               <?php $value = (isset($contract) ? _d($contract->date_bd) : _d(date('Y-m-d')));?>
               <?php echo render_date_input('date_bd', 'date_bd', $value); ?>
               <div style="color: red;"><?php echo form_error('date_bd');?></div>
            </div>
         </div>
         <div class="row">

            <div class="col-md-6">
               <label>Số công tơ điện</label>
               <input type="number" class="form-control" min="0" name="so_dien" value="<?php echo $contract->so_dien; ?>" oninput="this.value=(this.value   < Number(this.min) )  ? '' : this.value;">
            </div>
            <div class="col-md-6">
               <label>Số công tơ nước</label>
               <input type="number" class="form-control" min="0" name="so_nuoc" value="<?php echo $contract->so_nuoc; ?>" oninput="this.value=(this.value   < Number(this.min) )  ? '' : this.value;">
            </div>
         </div>
         <div style="margin-top: 10px;" class="row">
            <div class="col-md-6">
               <?php $value = (isset($contract) ? _d($contract->datestart) : _d(date('Y-m-d')));?>
               <?php echo render_date_input('datestart', 'contract_start_date', $value); ?>
               <div style="color: red;"><?php echo form_error('datestart');?></div>
            </div>
            <div class="col-md-6">
               <?php $value = (isset($contract) ? _d($contract->dateend) : '');?>
               <?php echo render_date_input('dateend', 'contract_end_date', $value); ?>
               <div style="color: red;"><?php echo form_error('dateend');?></div>
            </div>
         </div>

         <?php $value = (isset($contract) ? $contract->description : '');?>
         <?php echo render_textarea('description', 'contract_description', $value, array('rows' => 10)); ?>
         <?php $rel_id = (isset($contract) ? $contract->id : false);?>
         <?php echo render_custom_fields('contracts', $rel_id); ?>
         <div class="btn-bottom-toolbar text-right">
            <button style="display: <?php echo $show; ?>" onclick="return submit_hd()" type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>

         </div>
         <?php echo form_close(); ?>
      </div>
   </div>
</div>
<?php if (isset($contract)) {
   ?>
   <div class="col-md-7 right-column">
      <div class="panel_s">
         <div class="panel-body">
            <h4 class="no-margin"><?php echo $contract->subject; ?></h4>
            <a href="<?php echo site_url('contract/' . $contract->id . '/' . $contract->hash); ?>" target="_blank">
               <?php echo _l('view_contract'); ?>
               </a> <?php if($contract->id){ echo '<a target="_blank" href="/admin/expenses/word/'.$contract->id.'" class="btn-info btn"  style="width:10%;float:right;">IN</a>';} ?> 
               <hr class="hr-panel-heading" />
               <?php if ($contract->trash > 0) {
                  echo '<div class="ribbon default"><span>' . _l('contract_trash') . '</span></div>';
               }?>
               <div class="horizontal-scrollable-tabs preview-tabs-top">
                  <div class="scroller arrow-left"><i class="fa fa-angle-left"></i></div>
                  <div class="scroller arrow-right"><i class="fa fa-angle-right"></i></div>
                  <div class="horizontal-tabs">
                     <ul class="nav nav-tabs tabs-in-body-no-margin contract-tab nav-tabs-horizontal mbot15" role="tablist">
                        <li role="presentation" class="<?php if (!$this->input->get('tab') || $this->input->get('tab') == 'tab_content') {echo 'active';}?>">
                           <a href="#tab_content" aria-controls="tab_content" role="tab" data-toggle="tab">
                              <?php echo _l('contract_content'); ?>
                           </a>
                        </li>
                        <li role="presentation" class="<?php if ($this->input->get('tab') == 'attachments') {echo 'active';}?>">
                           <a href="#attachments" aria-controls="attachments" role="tab" data-toggle="tab">
                              <?php echo _l('contract_attachments'); ?>
                              <?php if ($totalAttachments = count($contract->attachments)) {?>
                                <span class="badge attachments-indicator"><?php echo $totalAttachments; ?></span>
                             <?php }?>
                          </a>
                       </li>
                       <li role="presentation">
                        <a href="#tab_comments" aria-controls="tab_comments" role="tab" data-toggle="tab" onclick="get_contract_comments(); return false;">
                           <?php echo _l('contract_comments'); ?>
                           <?php
                           $totalComments = total_rows('tblcontractcomments', 'contract_id=' . $contract->id)
                           ?>
                           <span class="badge comments-indicator<?php echo $totalComments == 0 ? ' hide' : ''; ?>"><?php echo $totalComments; ?></span>
                        </a>
                     </li>
                     <li role="presentation" class="<?php if ($this->input->get('tab') == 'renewals') {echo 'active';}?>">
                        <a href="#renewals" aria-controls="renewals" role="tab" data-toggle="tab">
                           <?php echo _l('no_contract_renewals_history_heading'); ?>
                           <?php if ($totalRenewals = count($contract_renewal_history)) {?>
                              <span class="badge"><?php echo $totalRenewals; ?></span>
                           <?php }?>
                        </a>
                     </li>
                     <li role="presentation" class="tab-separator">
                        <a href="#tab_tasks" aria-controls="tab_tasks" role="tab" data-toggle="tab" onclick="init_rel_tasks_table(<?php echo $contract->id; ?>,'contract'); return false;">
                           <?php echo _l('tasks'); ?>
                        </a>
                     </li>
                     <li role="presentation" class="tab-separator">
                        <a href="#tab_notes" onclick="get_sales_notes(<?php echo $contract->id; ?>,'contracts'); return false" aria-controls="tab_notes" role="tab" data-toggle="tab">
                           <?php echo _l('contract_notes'); ?>
                           <span class="notes-total">
                              <?php if ($totalNotes > 0) {?>
                                 <span class="badge"><?php echo $totalNotes; ?></span>
                              <?php }?>
                           </span>
                        </a>
                     </li>
                     <li role="presentation" data-toggle="tooltip" title="<?php echo _l('emails_tracking'); ?>" class="tab-separator">
                        <a href="#tab_emails_tracking" aria-controls="tab_emails_tracking" role="tab" data-toggle="tab">
                           <?php if (!is_mobile()) {?>
                              <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
                           <?php } else {?>
                              <?php echo _l('emails_tracking'); ?>
                           <?php }?>
                        </a>
                     </li>
                     <li role="presentation" class="tab-separator toggle_view">
                        <a href="#" onclick="contract_full_view(); return false;" data-toggle="tooltip" data-title="<?php echo _l('toggle_full_view'); ?>">
                           <i class="fa fa-expand"></i></a>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="tab-content">
                  <div role="tabpanel" class="tab-pane<?php if (!$this->input->get('tab') || $this->input->get('tab') == 'tab_content') {echo ' active';}?>" id="tab_content">
                     <div class="row">
                        <?php if ($contract->signed == 1) {
                           ?>
                           <div class="col-md-12">
                              <div class="alert alert-success">
                                 <?php echo _l('document_signed_info', array(
                                    '<b>' . $contract->acceptance_firstname . ' ' . $contract->acceptance_lastname . '</b> (<a href="mailto:' . $contract->acceptance_email . '">' . $contract->acceptance_email . '</a>)',
                                    '<b>' . _dt($contract->acceptance_date) . '</b>',
                                    '<b>' . $contract->acceptance_ip . '</b>')
                                 ); ?>
                              </div>
                           </div>
                        <?php }?>
                        <div class="col-md-12 text-right _buttons">
                           <div class="btn-group" style="display: none;">
                              <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-pdf-o"></i><?php if (is_mobile()) {echo ' PDF';}?> <span class="caret"></span></a>
                              <ul class="dropdown-menu dropdown-menu-right">
                                 <li class="hidden-xs"><a href="<?php echo admin_url('contracts/pdf/' . $contract->id . '?output_type=I'); ?>"><?php echo _l('view_pdf'); ?></a></li>
                                 <li class="hidden-xs"><a href="<?php echo admin_url('contracts/pdf/' . $contract->id . '?output_type=I'); ?>" target="_blank"><?php echo _l('view_pdf_in_new_window'); ?></a></li>
                                 <li><a href="<?php echo admin_url('contracts/pdf/' . $contract->id); ?>"><?php echo _l('download'); ?></a></li>
                                 <li>
                                    <!-- <a href="<?php echo admin_url('contracts/pdf/' . $contract->id . '?print=true'); ?>" target="_blank">
                                       <?php echo _l('print'); ?>
                                    </a> -->
                                 </li>
                              </ul>
                           </div>
                          <!--  <a href="#" class="btn btn-default" data-target="#contract_send_to_client_modal" data-toggle="modal"><span class="btn-with-tooltip" data-toggle="tooltip" data-title="<?php echo _l('contract_send_to_email'); ?>" data-placement="bottom">
                              <i class="fa fa-envelope"></i></span>
                           </a> -->
                           <div class="btn-group">
                              <button type="button" class="btn btn-default pull-left dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <?php echo _l('more'); ?> <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right">
                                 <li>
                                    <a href="<?php echo site_url('admin/clients/client/'.$contract->client.'?group=invoices'); ?>" target="_blank">
                                       <?php echo _l('view_invoice'); ?>
                                    </a>
                                 </li>
                                 <li>
                                    <a href="<?php echo site_url('contract/' . $contract->id . '/' . $contract->hash); ?>" target="_blank">
                                       <?php echo _l('view_contract'); ?>
                                    </a>
                                 </li>
                                 <?php if (has_permission('contracts', '', 'create')) {?>
                                    <li>
                                        <a href="<?php echo admin_url('expenses/coppy_hd/' . $contract->id); ?>" onclick="return confirm('Bạn có thực sự muốn chuyển hợp đồng cọc?');">
                                       <?php echo _l('contract_copy'); ?>
                                       </a>
                                    </li>
                                 <?php }?>
                                 <?php if ($contract->signed == 1 && has_permission('contracts', '', 'delete')) {?>
                                    <li>
                                       <a href="<?php echo admin_url('contracts/clear_signature/' . $contract->id); ?>" class="_delete">
                                          <?php echo _l('clear_signature'); ?>
                                       </a>
                                    </li>
                                 <?php }?>
                                 <?php if (has_permission('contracts', '', 'delete')) {?>
                                    <li>
                                       <a href="<?php echo admin_url('contracts/delete/' . $contract->id); ?>" class="_delete">
                                          <?php echo _l('delete'); ?></a>
                                       </li>
                                    <?php }?>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <?php if (isset($contract_merge_fields)) {
                                 ?>
                                 <hr class="hr-panel-heading" />
                                 <p class="bold mtop10 text-right"><a href="#" onclick="slideToggle('.avilable_merge_fields'); return false;"><?php echo _l('available_merge_fields'); ?></a></p>
                                 <div class=" avilable_merge_fields mtop15 hide">
                                    <ul class="list-group">
                                       <?php
                                       foreach ($contract_merge_fields as $field) {
                                          foreach ($field as $f) {
                                             if (strpos($f['key'], 'statement_') === FALSE && strpos($f['key'], 'password') === FALSE && strpos($f['key'], 'email_signature') === FALSE) {
                                                echo '<li class="list-group-item"><b>' . $f['name'] . '</b>  <a href="#" class="pull-right" onclick="insert_merge_field(this); return false">' . $f['key'] . '</a></li>';
                                             }
                                          }
                                       }?>
                                    </ul>
                                 </div>
                              <?php }?>
                           </div>
                        </div>
                        <hr class="hr-panel-heading" />
                        <div class="editable tc-content" style="border:1px solid #d2d2d2;min-height:70px; border-radius:4px;">
                           <?php
                           if (empty($contract->content)) {
                              echo do_action('new_contract_default_content', '<span class="text-danger text-uppercase mtop15 editor-add-content-notice"> ' . _l('click_to_add_content') . '</span>');
                           } else {
                              echo $contract->content;
                           }
                           ?>
                        </div>
                        <?php if (!empty($contract->signature)) {?>
                           <div class="row mtop25">
                              <div class="col-md-6 col-md-offset-6 text-right">
                                 <p class="bold"><?php echo _l('document_customer_signature_text'); ?>
                                 <?php if ($contract->signed == 1 && has_permission('contracts', '', 'delete')) {?>
                                    <a href="<?php echo admin_url('contracts/clear_signature/' . $contract->id); ?>" data-toggle="tooltip" title="<?php echo _l('clear_signature'); ?>" class="_delete text-danger">
                                       <i class="fa fa-remove"></i>
                                    </a>
                                 <?php }?>
                              </p>
                              <div class="pull-right">
                                 <img src="<?php echo site_url('download/preview_image?path=' . protected_file_url_by_path(get_upload_path_by_type('contract') . $contract->id . '/' . $contract->signature)); ?>" class="img-responsive" alt="">
                              </div>
                           </div>
                        </div>
                     <?php }?>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="tab_notes">
                     <?php echo form_open(admin_url('contracts/add_note/' . $contract->id), array('id' => 'sales-notes', 'class' => 'contract-notes-form')); ?>
                     <?php echo render_textarea('description'); ?>
                     <div class="text-right">
                        <button type="submit" class="btn btn-info mtop15 mbot15"><?php echo _l('contract_add_note'); ?></button>
                     </div>
                     <?php echo form_close(); ?>
                     <hr />
                     <div class="panel_s mtop20 no-shadow" id="sales_notes_area">
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="tab_comments">
                     <div class="row contract-comments mtop15">
                        <div class="col-md-12">
                           <div id="contract-comments"></div>
                           <div class="clearfix"></div>
                           <textarea name="content" id="comment" rows="4" class="form-control mtop15 contract-comment"></textarea>
                           <button type="button" class="btn btn-info mtop10 pull-right" onclick="add_contract_comment();"><?php echo _l('proposal_add_comment'); ?></button>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane<?php if ($this->input->get('tab') == 'attachments') {echo ' active';}?>" id="attachments">
                     <?php echo form_open(admin_url('contracts/add_contract_attachment/' . $contract->id), array('id' => 'contract-attachments-form', 'class' => 'dropzone')); ?>
                     <?php echo form_close(); ?>
                     <div class="text-right mtop15">
                        <button class="gpicker" data-on-pick="contractGoogleDriveSave">
                           <i class="fa fa-google" aria-hidden="true"></i>
                           <?php echo _l('choose_from_google_drive'); ?>
                        </button>
                        <div id="dropbox-chooser"></div>
                        <div class="clearfix"></div>
                     </div>
                     <!-- <img src="https://drive.google.com/uc?id=14mZI6xBjf-KjZzVuQe8-rjtv_wXEbDTw" /> -->

                     <div id="contract_attachments" class="mtop30">
                        <?php
                        $data = '<div class="row">';
                        foreach ($contract->attachments as $attachment) {
                           $href_url = site_url('download/file/contract/' . $attachment['attachment_key']);
                           if (!empty($attachment['external'])) {
                              $href_url = $attachment['external_link'];
                           }
                           $data .= '<div class="display-block contract-attachment-wrapper">';
                           $data .= '<div class="col-md-10">';
                           $data .= '<div class="pull-left"><i class="' . get_mime_class($attachment['filetype']) . '"></i></div>';
                           $data .= '<a href="' . $href_url . '"' . (!empty($attachment['external']) ? ' target="_blank"' : '') . '>' . $attachment['file_name'] . '</a>';
                           $data .= '<p class="text-muted">' . $attachment["filetype"] . '</p>';
                           $data .= '</div>';
                           $data .= '<div class="col-md-2 text-right">';
                           if ($attachment['staffid'] == get_staff_user_id() || is_admin()) {
                              $data .= '<a href="#" class="text-danger" onclick="delete_contract_attachment(this,' . $attachment['id'] . '); return false;"><i class="fa fa fa-times"></i></a>';
                           }
                           $data .= '</div>';
                           $data .= '<div class="clearfix"></div><hr/>';
                           $data .= '</div>';
                        }
                        $data .= '</div>';
                        echo $data;
                        ?>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane<?php if ($this->input->get('tab') == 'renewals') {echo ' active';}?>" id="renewals">
                     <?php if (has_permission('contracts', '', 'create') || has_permission('contracts', '', 'edit')) {?>
                        <div class="_buttons">
                           <a href="#" class="btn btn-default" data-toggle="modal" data-target="#renew_contract_modal">
                              <i class="fa fa-refresh"></i> <?php echo _l('contract_renew_heading'); ?>
                           </a>
                        </div>
                        <hr />
                     <?php }?>
                     <div class="clearfix"></div>
                     <?php
                     if (count($contract_renewal_history) == 0) {
                        echo _l('no_contract_renewals_found');
                     }
                     foreach ($contract_renewal_history as $renewal) {
                        ?>
                        <div class="display-block">
                           <div class="media-body">
                              <div class="display-block">
                                 <b>
                                    <?php
                                    echo _l('contract_renewed_by', $renewal['renewed_by']);
                                    ?>
                                 </b>
                                 <?php if ($renewal['renewed_by_staff_id'] == get_staff_user_id() || is_admin()) {?>
                                    <a href="<?php echo admin_url('contracts/delete_renewal/' . $renewal['id'] . '/' . $renewal['contractid']); ?>" class="pull-right _delete text-danger"><i class="fa fa-remove"></i></a>
                                    <br />
                                 <?php }?>
                                 <small class="text-muted"><?php echo _dt($renewal['date_renewed']); ?></small>
                                 <hr class="hr-10" />
                                 <span class="text-success bold" data-toggle="tooltip" title="<?php echo _l('contract_renewal_old_start_date', _d($renewal['old_start_date'])); ?>">
                                    <?php echo _l('contract_renewal_new_start_date', _d($renewal['new_start_date'])); ?>
                                 </span>
                                 <br />
                                 <?php if (is_date($renewal['new_end_date'])) {
                                    $tooltip = '';
                                    if (is_date($renewal['old_end_date'])) {
                                       $tooltip = _l('contract_renewal_old_end_date', _d($renewal['old_end_date']));
                                    }
                                    ?>
                                    <span class="text-success bold" data-toggle="tooltip" title="<?php echo $tooltip; ?>">
                                       <?php echo _l('contract_renewal_new_end_date', _d($renewal['new_end_date'])); ?>
                                    </span>
                                    <br/>
                                 <?php }?>
                                 <?php if ($renewal['new_value'] > 0) {
                                    $contract_renewal_value_tooltip = '';
                                    if ($renewal['old_value'] > 0) {
                                       $contract_renewal_value_tooltip = ' data-toggle="tooltip" data-title="' . _l('contract_renewal_old_value', _format_number($renewal['old_value'])) . '"';
                                    }?>
                                    <span class="text-success bold"<?php echo $contract_renewal_value_tooltip; ?>>
                                       <?php echo _l('contract_renewal_new_value', _format_number($renewal['new_value'])); ?>
                                    </span>
                                    <br />
                                 <?php }?>
                              </div>
                           </div>
                           <hr />
                        </div>
                     <?php }?>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="tab_emails_tracking">
                     <?php
                     $this->load->view('admin/includes/emails_tracking', array(
                        'tracked_emails' =>
                        get_tracked_emails($contract->id, 'contract'))
                  );
                  ?>
               </div>
               <div role="tabpanel" class="tab-pane" id="tab_tasks">
                  <?php init_relation_tasks_table(array('data-new-rel-id' => $contract->id, 'data-new-rel-type' => 'contract'));?>
               </div>
            </div>
         </div>
      </div>
   </div>
<?php }?>
</div>
</div>
</div>
<div class="modal fade" id="sales_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
          <span class="edit-title"><?php echo _l('invoice_item_edit_heading'); ?></span>
          <span class="add-title"><?php echo _l('invoice_item_add_heading'); ?></span>
       </h4>
    </div>

    <div class="modal-body">
     <div class="form-group">
       <label>Mã khách hàng</label>
       <input type="text" id="ms" name="ms" value="" class="form-control">
    </div>
    <label>Tên khách hàng</label>
    <input id="khach" type="text" name="client" class="form-control">
    <div class="form-group" app-field-wrapper="vat"><label for="vat" class="control-label">Mã số thuế/ Số CMND</label><input type="text" id="vat" name="vat" class="form-control" value=""></div>

    <div class="form-group" app-field-wrapper="address"><label for="address" class="control-label">Địa chỉ đăng ký kinh doanh</label><textarea id="address" name="address" class="form-control" rows="4"></textarea></div>
    <div class="form-group" app-field-wrapper="billing_street"><label for="billing_street" class="control-label">Địa chỉ nhận thư tín</label><textarea id="billing_street" name="billing_street" class="form-control" rows="4"></textarea></div>
    <div class="form-group" app-field-wrapper="phonenumber"><label for="phonenumber" class="control-label">Điện thoại</label><input type="text" id="phonenumber" name="phonenumber" class="form-control" value=""></div>
    <div class="form-group" app-field-wrapper="website"><label for="website" class="control-label">Website</label><input type="text" id="website" name="website" class="form-control" value=""></div>

 </div>
 <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
    <button type="submit" onclick="submit()" id="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
    <?php echo form_close(); ?>
 </div>
</div>
</div>
</div>
<?php init_tail();?>
<?php if (isset($contract)) {?>
   <!-- init table tasks -->
   <script>
      var contract_id = '<?php echo $contract->id; ?>';
   </script>
   <?php $this->load->view('admin/contracts/send_to_client');?>
   <?php $this->load->view('admin/contracts/renew_contract');?>
<?php }?>
<?php $this->load->view('admin/contracts/contract_type');?>

<script src="../../assets/js/lib.js"></script>
<script src="../../assets/js/cleave-phone.i18n.js"></script>
<script src="../../assets/js/cleave.min.js"></script>
<script src="../../assets/js/num_main.js"></script>
<script src="../../assets/js/prism.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script>
   st = "";
   sb = "";
  money = 0;
  function group_in(){
   var group = $('select[id="group"]').val();
   requestGetJSON("contracts/name_group_in/"+group).done(function (tre) {
    st = "";
     st = st + tre['name_group_in'][0]['name'];
    })
   requestGetJSON("contracts/group_in/"+group).done(function (re) {
    var ten = re['invoi'];
    var dem = ten.length;
    option=[];
    option =['"<option value="">Chọn văn phòng</option>"'];
    for (var i = 0; i < dem; i++) {
     op = ten[i]['description'];
     option += "<option value="+ten[i]['id']+">"+ten[i]['description']+"</option>";
     $('#name_building').html(option);

  }



});


}
$("#clientid").on("change", function(){
   var client_id = $("#clientid").val();
   // alert(coc);
   requestGetJSON("contracts/name_client_in/"+client_id).done(function (tde) {
     sst = tde['name_client_in'][0]['company']+" "+st+" "+sb;
     convert_string(sst);
    })
   requestGetJSON("contracts/pile_contract/"+client_id).done(function (re) {
      var ten = re['invoi'];
      var dem = ten.length;
      console.log(re);
      option=[];
      option =['"<option value="">Chọn hợp đồng cọc</option>"'];
      for (var i = 0; i < dem; i++) {

        option += "<option value="+ten[i]['id_coc']+">"+ten[i]['hopdong']+"</option>";
        $('#deposit').html(option);
     }
     $("#deposit").html(option);
  });
});
$("#contract_type").on("change", function(){
   var hd = $("#contract_type").val();
   if(hd==3||hd==4){
   $("#phi_phat_sinh").removeClass('hidden');
   }else{
   $("#phi_phat_sinh").addClass('hidden');
   }
});
function convert_string(str){
                var title, slug;
 
                //Lấy text từ thẻ input title 
                title = str;
 
                //Đổi chữ hoa thành chữ thường
                slug = title.toLowerCase();
 
                //Đổi ký tự có dấu thành không dấu
                slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
                slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
                slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
                slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
                slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
                slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
                slug = slug.replace(/đ/gi, 'd');
                //Xóa các ký tự đặt biệt
                slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
                //Đổi khoảng trắng thành ký tự gạch ngang
                slug = slug.replace(/ /gi, "-");
                //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
                //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
                slug = slug.replace(/\-\-\-\-\-/gi, '-');
                slug = slug.replace(/\-\-\-\-/gi, '-');
                slug = slug.replace(/\-\-\-/gi, '-');
                slug = slug.replace(/\-\-/gi, '-');
                //Xóa các ký tự gạch ngang ở đầu và cuối
                slug = '@' + slug + '@';
                slug = slug.replace(/\@\-|\-\@|\@/gi, '');
                //In slug ra textbox có id “slug”
//                 document.getElementById('slug').value = slug;
            $("#subject").val(slug);
            };
function myFunction_chage(e){
   //alert(2);
   var id = $('select[name="service"]').val();

   requestGetJSON("expenses/get_item_dich_vu/" + id).done(function (re) {
     var number = numeral(re[0].rate);
     var string = number.format('0,0');

     if(id>=3){
       money = money+ parseInt(re[0].rate);
       $("#contract_value").val(money); 
    }

    $("#p").append("<tr><td><input type='text' readonly name='name_dv[]' value='"+re[0].description+"' class='form-control'/><input type='text' name='id_item[]' value='"+re[0].id+"' class='hidden'/></td><td><input type='text' name='rate_dv[]'  class='rate' value='"+string+"'  class='form-control'/></td><td><span onclick='delete_item(this,undefined); return false;' class='btn btn-danger pull-left'><i class='fa fa-trash'></i></span></td></tr>");
 });
};
function myFunction_chage2(e){
   //alert(2);
   var id = $('select[name="name_building"]').val();
   requestGetJSON("contracts/name_buil_in/"+id).done(function (de) {
      sb = "";
     sb = sb +" "+ de['name_buil_in'][0]['description'];
      
    })
   requestGetJSON("invoice_items/get_item_by_id/" + id).done(function (re) {
      // console.log(re);
      var number = numeral(re.rate);
      var string = number.format('0,0');
      money = money+parseInt(re.rate);
      $("#contract_value").val(money);
      $("#name_gr").val(re.itemid);
      $("#p").append("<tr><td><input type='text' readonly name='name_dv[]' value='"+re.description+"' class='form-control'/><input type='text' name='id_item[]' value='"+re.itemid+"' class='hidden'/></td><td><input type='text' name='rate_dv[]'  class='rate' value='"+string+"'  class='form-control'/></td><td><span onclick='delete_item(this,undefined); return false;' class='btn btn-danger pull-left'><i class='fa fa-trash'></i></span></td></tr>");
   });
};

function delete_item(e, t) {
  $(e).parents("tr").addClass("animated fadeOut", function () {
    setTimeout(function () {
      $(e).parents("tr").remove(), calculate_total()
   }, 50)
 }), $('input[name="isedit"]').length > 0 && $("#removed-items").append(hidden_input("removed_items[]", t))
}

function submit(){
var company = $("#khach").val();
var vat = $("#vat").val();
var ms = $("#ms").val();
var address = $("#address").val();
var phonenumber = $("#phonenumber").val();
var website = $("#website").val();
var billing_street = $("#billing_street").val();
if(company!=""&&phonenumber!=""){
   $("#khach").removeClass('red');
   $("#phonenumber").removeClass('red');
 $.ajax({
type: "POST",
url: admin_url+"/clients/create_khach",
data: {
company:company,
vat:vat,
ms:ms,
address:address,
phonenumber:phonenumber,
website:website,
billing_street:billing_street,
},
success: function(data){
// alert(data);
 var response=JSON.parse(data);
  if(response.success == true){
            alert_float('success',response.message);
            if($('body').hasClass('contract') && typeof(response.id) != 'undefined') {
              // console.log(response.id);
                var ctype = $('#clientid');
                ctype.append('<option value="'+response.id+'">'+response.name+'</option>');
                ctype.selectpicker('val',response.id);
                ctype.selectpicker('refresh');
            }
$("#sales_item_modal").modal('hide');

        }

},
error:function(data){
console.log(data);
}
});  
}else{
   $("#khach").addClass('red');
   $("#phonenumber").addClass('red');
}

}
function CallPrint(strid) {
 var prtContent = document.getElementById(strid);
 var WinPrint = window.open('', '', 'left=0,top=0,width=1300px,height=800px,toolbar=0,scrollbars=1,status=0');
 WinPrint.document.write(prtContent.innerHTML);
 WinPrint.document.close();
 $(".an").css('display','block');
 WinPrint.focus();
 WinPrint.print();
 WinPrint.close();
 $(".an").css('display','none');
            //  prtContent.innerHTML="test";
         }
         function test(){


            CallPrint('print');
            

         }


         (function(i, s, o, g, r, a, m) {
          i["GoogleAnalyticsObject"] = r;
          (i[r] =
           i[r] ||
           function() {
            (i[r].q = i[r].q || []).push(arguments);
         }),
          (i[r].l = 1 * new Date());
          (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
          a.async = 1;
          a.src = g;
          m.parentNode.insertBefore(a, m);
       })(
       window,
       document,
       "script",
       "https://www.google-analytics.com/analytics.js",
       "ga"
       );

       ga("create", "UA-79828599-2", "auto");
       ga("send", "pageview");
    </script>
<!-- <script type="text/javascript">

   var cleave = new Cleave('#contract_value', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand'
});
   var cleave1 = new Cleave('#deposit', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand'
});

</script> -->
<script>
   Dropzone.autoDiscover = false;
   $(function () {

     if ($('#contract-attachments-form').length > 0) {
        new Dropzone("#contract-attachments-form", $.extend({}, _dropzone_defaults(), {
           success: function (file) {
              if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                 var location = window.location.href;
                 window.location.href = location.split('?')[0] + '?tab=attachments';
              }
           }
        }));
     }

    // In case user expect the submit btn to save the contract content
    $('#contract-form').on('submit', function () {
     $('#inline-editor-save-btn').click();
     return true;
  });

    if (typeof (Dropbox) != 'undefined' && $('#dropbox-chooser').length > 0) {
     document.getElementById("dropbox-chooser").appendChild(Dropbox.createChooseButton({
        success: function (files) {
           $.post(admin_url + 'contracts/add_external_attachment', {
              files: files,
              contract_id: contract_id,
              external: 'dropbox'
           }).done(function () {
              var location = window.location.href;
              window.location.href = location.split('?')[0] + '?tab=attachments';
           });
        },
        linkType: "preview",
        extensions: app_allowed_files.split(','),
     }));
  }

  _validate_form($('#contract-form'), {
     client: 'required',
     datestart: 'required',
     subject: 'required',
     contract_type: 'required',
     // name_building: 'required',
     dateend: 'required'

  });
  _validate_form($('#renew-contract-form'), {
     new_start_date: 'required'
  });

  var _templates = [];
  $.each(contractsTemplates, function (i, template) {
     _templates.push({
        url: admin_url + 'contracts/get_template?name=' + template,
        title: template
     });
  });

  var editor_settings = {
     selector: 'div.editable',
     inline: true,
     theme: 'inlite',
     relative_urls: false,
     remove_script_host: false,
     inline_styles: true,
     verify_html: false,
     cleanup: false,
     apply_source_formatting: false,
     valid_elements: '+*[*]',
     valid_children: "+body[style], +style[type]",
     file_browser_callback: elFinderBrowser,
     table_default_styles: {
        width: '100%'
     },
     fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
     pagebreak_separator: '<p pagebreak="true"></p>',
     plugins: [
     'advlist pagebreak autolink autoresize lists link image charmap hr',
     'searchreplace visualblocks visualchars code',
     'media nonbreaking table contextmenu',
     'paste textcolor colorpicker'
     ],
     autoresize_bottom_margin: 50,
     insert_toolbar: 'image media quicktable | bullist numlist | h2 h3 | hr',
     selection_toolbar: 'save_button bold italic underline superscript | forecolor backcolor link | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect h2 h3',
     contextmenu: "image media inserttable | cell row column deletetable | paste pastetext searchreplace | visualblocks pagebreak charmap | code",
     setup: function (editor) {

        editor.addCommand('mceSave', function () {
           save_contract_content(true);
        });

        editor.addShortcut('Meta+S', '', 'mceSave');

        editor.on('MouseLeave blur', function () {
           if (tinymce.activeEditor.isDirty()) {
              save_contract_content();
           }
        });

        editor.on('MouseDown ContextMenu', function () {
           if (!is_mobile() && !$('.left-column').hasClass('hide')) {
              contract_full_view();
           }
        });

        editor.on('blur', function () {
           $.Shortcuts.start();
        });


        editor.on('focus', function () {
           $.Shortcuts.stop();
        });

     }
  }

  if (_templates.length > 0) {
     editor_settings.templates = _templates;
     editor_settings.plugins[3] = 'template ' + editor_settings.plugins[3];
     editor_settings.contextmenu = editor_settings.contextmenu.replace('inserttable', 'inserttable template');
  }

  if(is_mobile()) {

     editor_settings.theme = 'modern';
     editor_settings.mobile    = {};
     editor_settings.mobile.theme = 'mobile';
     editor_settings.mobile.toolbar = _tinymce_mobile_toolbar();

     editor_settings.inline = false;
     window.addEventListener("beforeunload", function (event) {
      if (tinymce.activeEditor.isDirty()) {
         save_contract_content();
      }
   });
  }

  tinymce.init(editor_settings);

});
function submit_hd(){
      a = $("#dateend").val();
      b = $("#datestart").val();
      if(a==b){
         $("#dateend").css('border','1px solid red');
         return false;
      }else if(a<b){
         $("#dateend").css('border','1px solid red');
         return false;
      }else{
         $("#dateend").css('border','1px solid #bfcbd9');
         return true;
      }
   }
function save_contract_content(manual) {
  var editor = tinyMCE.activeEditor;
  var data = {};
  data.contract_id = contract_id;
  data.content = editor.getContent();
  $.post(admin_url + 'contracts/save_contract_data', data).done(function (response) {
     response = JSON.parse(response);
     if (typeof (manual) != 'undefined') {
          // Show some message to the user if saved via CTRL + S
          alert_float('success', response.message);
       }
       // Invokes to set dirty to false
       editor.save();
    }).fail(function (error) {
     var response = JSON.parse(error.responseText);
     alert_float('danger', response.message);
  });
 }

 function delete_contract_attachment(wrapper, id) {
  if (confirm_delete()) {
     $.get(admin_url + 'contracts/delete_contract_attachment/' + id, function (response) {
        if (response.success == true) {
           $(wrapper).parents('.contract-attachment-wrapper').remove();

           var totalAttachmentsIndicator = $('.attachments-indicator');
           var totalAttachments = totalAttachmentsIndicator.text().trim();
           if(totalAttachments == 1) {
            totalAttachmentsIndicator.remove();
         } else {
            totalAttachmentsIndicator.text(totalAttachments-1);
         }
      } else {
        alert_float('danger', response.message);
     }
  }, 'json');
  }
  return false;
}

function insert_merge_field(field) {
  var key = $(field).text();
  tinymce.activeEditor.execCommand('mceInsertContent', false, key);
}

function contract_full_view() {
  $('.left-column').toggleClass('hide');
  $('.right-column').toggleClass('col-md-7');
  $('.right-column').toggleClass('col-md-12');
  $(window).trigger('resize');
}

function add_contract_comment() {
  var comment = $('#comment').val();
  if (comment == '') {
     return;
  }
  var data = {};
  data.content = comment;
  data.contract_id = contract_id;
  $('body').append('<div class="dt-loader"></div>');
  $.post(admin_url + 'contracts/add_comment', data).done(function (response) {
     response = JSON.parse(response);
     $('body').find('.dt-loader').remove();
     if (response.success == true) {
        $('#comment').val('');
        get_contract_comments();
     }
  });
}

function get_contract_comments() {
  if (typeof (contract_id) == 'undefined') {
     return;
  }
  requestGet('contracts/get_comments/' + contract_id).done(function (response) {
     $('#contract-comments').html(response);
     var totalComments = $('[data-commentid]').length;
     var commentsIndicator = $('.comments-indicator');
     if(totalComments == 0) {
      commentsIndicator.addClass('hide');
   } else {
      commentsIndicator.removeClass('hide');
      commentsIndicator.text(totalComments);
   }
});
}

function remove_contract_comment(commentid) {
  if (confirm_delete()) {
     requestGetJSON('contracts/remove_comment/' + commentid).done(function (response) {
        if (response.success == true) {

         var totalComments = $('[data-commentid]').length;

         $('[data-commentid="' + commentid + '"]').remove();

         var commentsIndicator = $('.comments-indicator');
         if(totalComments-1 == 0) {
            commentsIndicator.addClass('hide');
         } else {
            commentsIndicator.removeClass('hide');
            commentsIndicator.text(totalComments-1);
         }
      }
   });
  }
}

function edit_contract_comment(id) {
  var content = $('body').find('[data-contract-comment-edit-textarea="' + id + '"] textarea').val();
  if (content != '') {
     $.post(admin_url + 'contracts/edit_comment/' + id, {
        content: content
     }).done(function (response) {
        response = JSON.parse(response);
        if (response.success == true) {
           alert_float('success', response.message);
           $('body').find('[data-contract-comment="' + id + '"]').html(nl2br(content));
        }
     });
     toggle_contract_comment_edit(id);
  }
}

function toggle_contract_comment_edit(id) {
  $('body').find('[data-contract-comment="' + id + '"]').toggleClass('hide');
  $('body').find('[data-contract-comment-edit-textarea="' + id + '"]').toggleClass('hide');
}

function contractGoogleDriveSave(pickData) {
   var data = {};
   data.contract_id = contract_id;
   data.external = 'gdrive';
   data.files = pickData;
   $.post(admin_url + 'contracts/add_external_attachment', data).done(function () {
    var location = window.location.href;
    window.location.href = location.split('?')[0] + '?tab=attachments';
 });
}

</script>
</body>
</html>
