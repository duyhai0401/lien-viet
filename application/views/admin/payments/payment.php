<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-5">
				<div class="panel_s">
					<div class="col-md-12 no-padding">
						<div class="panel_s">
							<?php echo form_open($this->uri->uri_string()); ?>
							<div class="panel-body">
								<h4 class="no-margin"><?php echo _l('payment_edit_for_invoice'); ?> <a href="<?php echo admin_url('invoices/list_invoices/'.$payment->invoiceid); ?>"><?php echo format_invoice_number($invoice->id); ?>-- <?php echo $invoice->client->company; ?></a></h4>
								<hr class="hr-panel-heading" />
								<?php echo render_input('amount','payment_edit_amount_received',number_format($payment->amount),'text'); ?>
								<?php echo render_date_input('date','payment_edit_date',_d($payment->date)); ?>
								<?php echo render_select('paymentmode',$payment_modes,array('id','name'),'payment_mode',$payment->paymentmode); ?>
								<i class="fa fa-question-circle" data-toggle="tooltip" data-title="<?php echo _l('payment_method_info'); ?>"></i>
								  <div class="form-gruoup">
                            <label for="note" class="control-label">Tài Khoản gửi</label>
                            <input name="bank_send" class="form-control" value="<?php echo $payment->bank_send;?>" >
                            <input name="user_id" class="form-control" type="hidden" value="<?php echo $current_user->staffid;?>" >

                        </div>
								   <div class="form-group" ><label for="bank_id" class="control-label">Tài Khoản hưởng</label>
								                       <?php
$conn = mysqli_connect("localhost", "root","RedbeeX0605@","lienviet");
mysqli_set_charset($conn,"utf8");
 $sql = "SELECT * FROM `btlbank` where 1
";

$data = mysqli_query($conn,$sql);

                    ?>
                                <div  class="dropdown bootstrap-select" style="width: 100%;">
                                    <select id="bank_id" name="bank_id" class="selectpicker" data-width="100%" data-none-selected-text="Không có mục nào được chọn" data-live-search="true" tabindex="-98"><option value=""></option>
                                    <?php while ($data1 = mysqli_fetch_assoc($data)) { ?>
                                    <option value="<?php echo $data1['id']?>" ><?php echo $data1['name_bank']?></option>
                                   <?php }?>
                                    </select>
                                </div>
                                </div>

								<?php echo render_input('paymentmethod','payment_method',$payment->paymentmethod); ?>
								<?php echo render_input('transactionid','payment_transaction_id',$payment->transactionid); ?>
								<?php echo render_textarea('note','note',$payment->note,array('rows'=>7)); ?>
								<div class="btn-bottom-toolbar text-right">
									<!-- <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button> -->
								</div>
							</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="panel_s">
					<div class="panel-body">
						<h4 class="pull-left "><?php echo _l('payment_view_heading'); ?></h4>
						<div class="pull-right">
							<div class="btn-group">
								<a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-pdf-o"></i><?php if(is_mobile()){echo ' PDF';} ?> <span class="caret"></span></a>
								<ul class="dropdown-menu dropdown-menu-right">
									<li class="hidden-xs"><a href="<?php echo admin_url('payments/pdf/'.$payment->paymentid.'?output_type=I'); ?>"><?php echo _l('view_pdf'); ?></a></li>
									<li class="hidden-xs"><a href="<?php echo admin_url('payments/pdf/'.$payment->paymentid.'?output_type=I'); ?>" target="_blank"><?php echo _l('view_pdf_in_new_window'); ?></a></li>
									<li><a href="<?php echo admin_url('payments/pdf/'.$payment->paymentid); ?>"><?php echo _l('download'); ?></a></li>
									<li>
										<a href="<?php echo admin_url('payments/pdf/'.$payment->paymentid.'?print=true'); ?>" target="_blank">
											<?php echo _l('print'); ?>
										</a>
									</li>
								</ul>
							</div>
							<!-- <?php if(has_permission('payments','','delete')){ ?>
								<a href="<?php echo admin_url('payments/delete/'.$payment->paymentid); ?>" class="btn btn-danger _delete">
									<i class="fa fa-remove"></i>
								</a>
							<?php } ?> -->
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<address>
									<?php  $sql_in = "SELECT * FROM `tbl_infor` where 1 order by id desc ";
									$data_in = mysqli_query($conn,$sql_in); 
									while ($data1_in = mysqli_fetch_assoc($data_in)) {?>
									<p>
										<b>
											<h5><?php echo $data1_in['company']?></h5>
										</br>
										<?php echo $data1_in['address']?></br>
										Mã số thuế: <?php echo $data1_in['tax_code']?></br>
										hotline: <?php echo $data1_in['phone_number']?></br>
										Email:<?php echo $data1_in['email']?></br>
									</b></p>

										<?php }?>
								</address>
							</div>
							<div class="col-sm-6 text-right">
								<address>
									<span class="bold">
										<?php echo format_customer_info($invoice, 'payment', 'billing', true); ?>
									</address>
								</div>
							</div>
							<div class="col-md-12 text-center">
								<h3 class="text-uppercase"><?php echo _l('payment_receipt'); ?></h3>
							</div>
							<div class="col-md-12 mtop30">
								<div class="row">
									<div class="col-md-6">
										<p><?php echo _l('payment_date'); ?> <span class="pull-right bold"><?php echo _d($payment->date); ?></span></p>
										<hr />
										<p><?php echo _l('payment_view_mode'); ?>
											<span class="pull-right bold">
												<?php echo $payment->name; ?>
												<?php if(!empty($payment->paymentmethod)){
													echo ' - ' . $payment->paymentmethod;
												}
												?>
											</span></p>
											<?php if(!empty($payment->transactionid)) { ?>
											<hr />
											<p><?php echo _l('payment_transaction_id'); ?>: <span class="pull-right bold"><?php echo $payment->transactionid; ?></span></p>
											<?php } ?>

										</div>
										<div class="clearfix"></div>
										<div class="col-md-6">
											<div class="payment-preview-wrapper">
												<?php echo _l('payment_total_amount'); ?><br />
												<?php echo format_money($payment->amount,$invoice->symbol); ?>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12 mtop30">
									<h4><?php echo _l('payment_for_string'); ?></h4>
									<div class="table-responsive">
										<table class="table table-borderd table-hover">
											<thead>
												<tr>
													<th><?php echo _l('payment_table_invoice_number'); ?></th>
													<th><?php echo _l('payment_table_invoice_date'); ?></th>
													<th><?php echo _l('payment_table_invoice_amount_total'); ?></th>
													<th><?php echo _l('payment_table_payment_amount_total'); ?></th>
													<th>Tài khoản gửi</th>
													<th>Người gửi</th>
													<th>Tài khoản nhận</th>
													<th>Người Tạo</th>
													<?php if($invoice->status != 2 && $invoice->status != 5) { ?>
													<th><span class="text-danger"><?php echo _l('invoice_amount_due'); ?></span></th>
													<?php } ?>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><?php echo format_invoice_number($invoice->id); ?></td>
													<td><?php echo _d($invoice->date); ?></td>
													<td><?php echo format_money($invoice->total,$invoice->symbol); ?></td>
													<td><?php echo format_money($payment->amount,$invoice->symbol); ?></td>
													<td><?php echo $payment->bank_send;?></td>
													<td><?php echo $invoice->client->company?></td>
													<td><?php  
													$sql1 = "SELECT * FROM `btlbank` where id = $payment->bank_id";
													$data2 = mysqli_query($conn,$sql1);
													while ($data11 = mysqli_fetch_assoc($data2)) {
													echo $data11['number_id']."-".$data11['name_bank']."-".$data11['branch'];
												}
													?>
														
													</td>
													<td><?php  
													$sql11 = "SELECT * FROM `tblstaff` where staffid = $payment->user_id";
													$data21 = mysqli_query($conn,$sql11);
													while ($data111 = mysqli_fetch_assoc($data21)) {
													echo $data111['firstname']." ".$data111['lastname'];
												}
													?>
														
													</td>
													<?php if($invoice->status != 2 && $invoice->status != 5) { ?>
													<td class="text-danger">
													<?php echo format_money(get_invoice_total_left_to_pay($invoice->id, $invoice->total), $invoice->symbol); ?>
													</td>
													<?php } ?>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				 <div class="btn-bottom-pusher"></div>
			</div>
		</div>
		<?php init_tail(); ?>
		<script>
			$(function(){
				_validate_form($('form'),{amount:'required',date:'required'});
			});
		</script>
	</body>
	</html>
