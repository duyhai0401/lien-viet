<?php init_head(); ?>
<script type="text/javascript">
//<![CDATA[
function Pager(tableName, itemsPerPage) {
    this.tableName = tableName;
    this.itemsPerPage = itemsPerPage;
    this.currentPage = 1;
    this.pages = 0;
    this.inited = false;
   
    this.showRecords = function(from, to) {      
        var rows = document.getElementById(tableName).rows;
        // i starts from 1 to skip table header row
        for (var i = 1; i < rows.length; i++) {
            if (i < from || i > to)
                rows[i].style.display = 'none';
            else
                rows[i].style.display = '';
        }
    }
   
    this.showPage = function(pageNumber) {
          if (! this.inited) {
                   alert("not inited");
                   return;
          }
        var oldPageAnchor = document.getElementById('pg'+this.currentPage);
        oldPageAnchor.className = 'pg-normal';
       
        this.currentPage = pageNumber;
        var newPageAnchor = document.getElementById('pg'+this.currentPage);
        newPageAnchor.className = 'pg-selected';
       
        var from = (pageNumber - 1) * itemsPerPage + 1;
        var to = from + itemsPerPage - 1;
        this.showRecords(from, to);
    }  
   
    this.prev = function() {
        if (this.currentPage > 1)
            this.showPage(this.currentPage - 1);
    }
   
    this.next = function() {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);
        }
    }                      
   
    this.init = function() {
        var rows = document.getElementById(tableName).rows;
        var records = (rows.length - 1);
        this.pages = Math.ceil(records / itemsPerPage);
        this.inited = true;
    }
    this.showPageNav = function(pagerName, positionId) {
          if (! this.inited) {
                   alert("not inited");
                   return;
          }
          var element = document.getElementById(positionId);
         
          var pagerHtml = '<li  id="DataTables_Table_0_previous" onclick="' + pagerName + '.prev();" class="pg-normal paginate_button previous "> <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">Lùi lại</a> </li>  ';
        for (var page = 1; page <= this.pages; page++)
            pagerHtml += '<li id="pg' + page + '" class="pg-normal paginate_button previous " onclick="' + pagerName + '.showPage(' + page + ');"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">' + page + '</a></li>  ';
        pagerHtml += '<li onclick="'+pagerName+'.next();" class="pg-normal paginate_button previous "> <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">Đi tới</a></li>';          
       
        element.innerHTML = pagerHtml;
    }
}
//]]>
</script>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if(has_permission('expenses','','create')){ ?>
                  <a href="#" data-toggle="modal" data-target="#sales_item_modal" class="btn btn-info">Thêm mới</a>
                  <?php } ?>
                  <!-- <?php $this->load->view('admin/expenses/filter_by_template'); ?> -->
                  <!-- <a href="#" onclick="slideToggle('#stats-top'); return false;" class="pull-right btn btn-default mleft5 btn-with-tooltip" data-toggle="tooltip" title="<?php echo _l('view_stats_tooltip'); ?>"><i class="fa fa-bar-chart"></i></a>
                  <a href="#" class="btn btn-default pull-right btn-with-tooltip toggle-small-view hidden-xs" onclick="toggle_small_view('.table-expenses','#expense'); return false;" data-toggle="tooltip" title="<?php echo _l('invoices_toggle_table_tooltip'); ?>"><i class="fa fa-angle-double-left"></i></a> -->
                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="expenses_total"></div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12" id="small-table">
                  <div class="panel_s">
                     <div class="panel-body">
                        <div class="clearfix"></div>
                        <!-- if expenseid found in url -->
                        <?php echo form_hidden('expenseid',$expenseid); ?>
                       <table data-last-order-identifier="expenses" data-default-order="" class="table table-expenses dataTable no-footer dtr-inline" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" >
                    <thead>
                      <tr role="row">
                      <th style="display: none;"></th>
                       <th class="sorting"  rowspan="1" colspan="1" aria-label="Phân loại kích hoạt để sắp xếp từ dưới lên">ID</th>
                       <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Số tiền kích hoạt để sắp xếp từ dưới lên">Tên Công Ty</th>
                       <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Tên kích hoạt để sắp xếp từ dưới lên" style="width: 200px">Địa Chỉ</th>
                       <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Tên kích hoạt để sắp xếp từ dưới lên">Số Thuế</th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Phương thức thanh toán kích hoạt để sắp xếp từ dưới lên">Email</th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Phương thức thanh toán kích hoạt để sắp xếp từ dưới lên">Số điện thoại</th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Phương thức thanh toán kích hoạt để sắp xếp từ dưới lên">Hành động</th>
                    </tr>
                    </thead>
                    <?php
$conn = mysqli_connect("localhost", "root","RedbeeX0605@","lienviet");
mysqli_set_charset($conn,"utf8");
 $sql = "SELECT * FROM `tbl_infor` where 1
";
$data = mysqli_query($conn,$sql);

                    ?>
                    <tbody>
                  
                    <?php
               while ($data1 = mysqli_fetch_assoc($data)) {
                    ?>
                    <tr role="row" class="odd">
                  <td style="display: none;"></td>
                    <td><?php echo $data1['id']?></td>
                    <td><?php echo $data1['company']?></td>
                    <td><?php echo $data1['address']?></td>
                    <td><?php echo $data1['tax_code']?></td>
                    <td><?php echo $data1['email']?></td>
                    <td><?php echo $data1['phone_number']?></td>
                    <td><a onclick="return confirm('Bạn có muốn xóa thông tin?');" href="/demo/admin/payments/delete_infor/<?php echo $data1['id']?>" class="btn-info btn ">Xóa</a>
                    <a href="#" data-toggle="modal" data-target="#sales_item_modal_<?php echo $data1['id']?>" class="btn-info btn ">Sửa</a></td>
                    </tr>
                    <div class="modal fade in" id="sales_item_modal_<?php echo $data1['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title hide">Chỉnh sửa</span>
                    <span class="add-title">Thêm mục mới</span>
                </h4>
            </div>
            <form action="" id="invoice_item_form" method="post" accept-charset="utf-8" novalidate="novalidate">
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning affect-warning hide">
                            Chỉnh sửa thông tin sản phẩm không ảnh hưởng đến hóa đơn/báo giá/đề xuất đã tạo.                        </div>

                         <div class="form-group" app-field-wrapper="description"><label for="description" class="control-label"> <small class="req text-danger">* </small>Tên Công Ty</label>
                         <input type="text" id="company" required name="company" class="form-control" value="<?php echo $data1['company']?>">
                         <input type="text" class="hidden" name="id" value="<?php echo $data1['id']?>">
                         </div>                        
                         <div class="form-group" app-field-wrapper="long_description"><label for="long_description" class="control-label">Địa Chỉ</label><textarea id="address" name="address" class="form-control" rows="4"><?php echo $data1['address']?></textarea></div>                       
                         <div class="form-group">
                        <label for="rate" class="control-label"> <small class="req text-danger">* </small>
                            Mã Số Thuế<small></small></label>
                            <input type="number" required id="tax_code" name="tax_code" class="form-control" value="<?php echo $data1['tax_code']?>">
                        </div><div class="form-group">
                        <label for="rate" class="control-label"> <small class="req text-danger">* </small>
                            Email<small></small></label>
                            <input type="email" required id="email" name="email" class="form-control" value="<?php echo $data1['email']?>">
                        </div><div class="form-group">
                        <label for="rate" class="control-label"> <small class="req text-danger">* </small>
                            Số Điện Thoại<small></small></label>
                            <input type="number" required id="phone_number" name="phone_number" class="form-control" value="<?php echo $data1['phone_number']?>">
                        </div>
                 
               
                      </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" name="update" value="submit" class="btn btn-info">Lưu lại</button>
            </div>
            </form>
</div>
</div>
</div>
                    <?php }?>
                    

                    </tbody>
                  </table><ul class="pagination" id="pageNavPosition">
            
                  </ul>
                     </div>
                 
                  </div>
               </div>
               <div class="col-md-7 small-table-right-col">
                  <div id="expense" class="hide">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade in" id="sales_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title hide">Chỉnh sửa</span>
                    <span class="add-title">Thêm mục mới</span>
                </h4>
            </div>
            <form action="" id="invoice_item_form" method="post" accept-charset="utf-8" novalidate="novalidate">
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning affect-warning hide">
                            Chỉnh sửa thông tin sản phẩm không ảnh hưởng đến hóa đơn/báo giá/đề xuất đã tạo.                        </div>
                        <div class="form-group" app-field-wrapper="description"><label for="description" class="control-label"> <small class="req text-danger">* </small>Tên Công Ty</label><input type="text" id="company" required name="company" class="form-control" value=""></div>                        <div class="form-group" app-field-wrapper="long_description"><label for="long_description" class="control-label">Địa Chỉ</label><textarea id="address" name="address" class="form-control" rows="4"></textarea></div>                       
                         <div class="form-group">
                        <label for="rate" class="control-label"> <small class="req text-danger">* </small>
                            Mã Số Thuế<small></small></label>
                            <input type="number" required id="tax_code" name="tax_code" class="form-control" value="">
                        </div><div class="form-group">
                        <label for="rate" class="control-label"> <small class="req text-danger">* </small>
                            Email<small></small></label>
                            <input type="email" required id="email" name="email" class="form-control" value="">
                        </div><div class="form-group">
                        <label for="rate" class="control-label"> <small class="req text-danger">* </small>
                            Số Điện Thoại<small></small></label>
                            <input type="number" required id="phone_number" name="phone_number" class="form-control" value="">
                        </div>
                 
               
                      </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" name="add" value="submit" class="btn btn-info">Lưu lại</button>
            </div></form>
</div>
</div>
</div>
<div class="modal fade" id="expense_convert_helper_modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
      <form action="" method="post" accept-charset="utf8">
        <div class="form-group" app-field-wrapper="description"><label for="description" class="control-label"> <small class="req text-danger">* </small>Mô tả</label><input type="text" id="description" name="description" class="form-control" value=""></div>
        <div class="form-group" app-field-wrapper="long_description"><label for="long_description" class="control-label">Mô tả dài</label><textarea id="long_description" name="long_description" class="form-control" rows="4"></textarea></div>
        <div class="form-group">
                        <label for="rate" class="control-label"> <small class="req text-danger">* </small>
                            Tỷ lệ - USD <small>(Base Currency)</small></label>
                            <input type="number" id="rate" name="rate" class="form-control" value="">
                        </div>
                        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-info">Lưu lại</button>
            </div>
                        </form>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<style type="text/css">
  .pg-normal {
       color: red;
       font-weight: normal;
       text-decoration: none;  
       cursor: pointer;  
}

.pg-selected {
       color: black;
       font-weight: bold;     
       text-decoration: underline;
       cursor: pointer;
}
</style>
<script type="text/javascript"><!--
       var pager = new Pager('DataTables_Table_0', 15);
       pager.init();
       pager.showPageNav('pager', 'pageNavPosition');
       pager.showPage(1);
//--></script>
<!-- <script>var hidden_columns = [4,5,6,7,8,9];</script> -->
<?php init_tail(); ?>
<!-- <script>
   Dropzone.autoDiscover = false;
   $(function(){
             // Expenses additional server params
             var Expenses_ServerParams = {};
             $.each($('._hidden_inputs._filters input'),function(){
               Expenses_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
             });
             initDataTable('.table-expenses', admin_url+'expenses/table', 'undefined', 'undefined', Expenses_ServerParams, <?php echo do_action('expenses_table_default_order',json_encode(array(5,'desc'))); ?>).column(0).visible(false, false).columns.adjust();

             init_expense();

             $('#expense_convert_helper_modal').on('show.bs.modal',function(){
                var emptyNote = $('#tab_expense').attr('data-empty-note');
                var emptyName = $('#tab_expense').attr('data-empty-name');
                if(emptyNote == '1' && emptyName == '1') {
                    $('#inc_field_wrapper').addClass('hide');
                } else {
                    $('#inc_field_wrapper').removeClass('hide');
                    emptyNote === '1' && $('.inc_note').addClass('hide') || $('.inc_note').removeClass('hide')
                    emptyName === '1' && $('.inc_name').addClass('hide') || $('.inc_name').removeClass('hide')
                }
             });

             $('body').on('click','#expense_confirm_convert',function(){
              var parameters = new Array();
              if($('input[name="expense_convert_invoice_type"]:checked').val() == 'save_as_draft_true'){
                parameters['save_as_draft'] = 'true';
              }
              parameters['include_name'] = $('#inc_name').prop('checked');
              parameters['include_note'] = $('#inc_note').prop('checked');
              window.location.href = buildUrl(admin_url+'expenses/convert_to_invoice/'+$('body').find('.expense_convert_btn').attr('data-id'), parameters);
            });
           });
</script> -->
</body>
</html>
