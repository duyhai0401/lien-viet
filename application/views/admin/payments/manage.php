<?php init_head(); ?>
<div id="wrapper">
	<div class="_filters _hidden_inputs hidden">
                        <?php
                       
                        echo form_hidden('expired');
                        echo form_hidden('expired_1');
                        echo form_hidden('from');
                        echo form_hidden('to');
                       
                    ?>
                </div>
	<div class="panel-body _buttons">
                    <div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-filter" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-left width300 height500">
                            
                            <li>
                                <a href="#" data-cview="expired" class="submit" onclick="dt_custom_view('expired','.table-payments','expired'); return false;">
                                    <?php echo "Thanh toán Ngân hàng"; ?>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-cview="expired_1" class="submit" onclick="dt_custom_view('expired_1','.table-payments','expired_1'); return false;">
                                    <?php echo "Thanh toán Tiền mặt"; ?>
                                </a>
                            </li>
                            <li>
                                <div style="margin-bottom: 15px;">
                           <div class="col-md-6">
                              <label for="report-from" class="control-label">Từ ngày</label>
                              <div class="input-group date">
                                 <input type="text" class="form-control datepicker" id="report-from" name="report-from">
                                 <div class="input-group-addon">
                                    <i class="fa fa-calendar calendar-icon"></i>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <label for="report-to" class="control-label">Đến ngày</label>
                              <div class="input-group date">
                                 <input type="text" class="form-control datepicker" id="report-to" name="report-to">
                                 <div class="input-group-addon">
                                    <i class="fa fa-calendar calendar-icon"></i>
                                 </div>
                              </div>
                           </div>
                        </div>
                            </li>
                        </ul>
                    </div>
                        </div>
	<div class="content">
		<div class="panel_s">
			 <?php echo form_hidden('custom_view'); ?>
			<div class="panel-body">
				<?php $this->load->view('admin/payments/table_html'); ?>
			</div>
		</div>
	</div>
</div>

<?php init_tail(); ?>
<script>
$("#report-from").on("change", function(){
var from = $("#report-from").val();
dt_custom_view(from,'.table-payments','from');
console.log(from);
});
$("#report-to").on("change", function(){
var to = $("#report-to").val();
dt_custom_view(to,'.table-payments','to');
console.log(to);
});
	$(function(){
		var ContractsServerParams = {};
        $.each($('._hidden_inputs._filters input'),function(){
            ContractsServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
        });
        console.log(ContractsServerParams);
		initDataTable('.table-payments', admin_url+'payments/table', undefined, undefined,ContractsServerParams,<?php echo do_action('payments_table_default_order',json_encode(array(6,'desc'))); ?>);
	});
</script>
</body>
</html>
