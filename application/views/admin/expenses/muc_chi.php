<?php init_head();?>

<div id="wrapper">
 <div class="content">
   <div class="_buttons">
    <a href="#" class="btn btn-info pull-left edit-service" data-toggle="modal" data-target="#sales_item_modal">Thêm mới</a>

  </div>
  <div class="row" style="margin-top: 30px;">
    <?php 
    if(validation_errors() != false){ 
      ?>
    <div class="alert alert-danger" style="margin-top: 10px;">
<?php echo validation_errors(); ?>
    </div>
      <?php
    }
    ?>

    <table style="background: white" class="table" id="DataTables_Table_1" role="grid" aria-describedby="DataTables_Table_1_info"><thead><tr role="row">
     <!--  <th class="sorting_disabled not-export" rowspan="1" colspan="1" aria-label=" - "><span class="hide"> - </span><div class="checkbox mass_select_all_wrap"><input type="checkbox" id="mass_select_all" data-to-table="invoice-items"><label></label></div></th> -->
     <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Mô tả kích hoạt để sắp xếp từ trên xuống">Dịch vụ</th>
     <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Mô tả dài kích hoạt để sắp xếp từ dưới lên">Mô tả </th>
     <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Giá kích hoạt để sắp xếp từ dưới lên">Giá</th>

     <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Đơn vị kích hoạt để sắp xếp từ dưới lên">Đơn vị tính</th>

   </tr></thead>
   <tbody>
    <?php

    foreach ($muc_chi as $key => $value) {

     echo '  <tr class="has-row-options odd" role="row">

     <td class="sorting_1"><a href="#" class="edit-service" value="' . $value['id_chi'] . '" data-toggle="modal" data-target="#sales_item_modal" data-id="' . $value['id_chi'] . '">' . $value['description_chi'] . '</a><div class="row-options"><a  class="edit-service" data-id="' . $value['id_chi'] . '" id="edit-service">Chỉnh sửa </a> | <a href="/admin/expenses/delete_dichvu/' . $value['id_chi'] . '" class="text-danger _delete">Xóa </a></div></td>
     <td>' . $value['long_description_chi'] . '</td>
     <td>' . $value['rate_chi'] . '</td>

     <td>' . $value['unit_chi'] . '</td>
     </tr>';
   }

   ?>

 </tbody></table>
</div>
<div class="btn-bottom-pusher"></div>
</div>
</div>
<div class="modal fade in" id="sales_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel">
          <span class="edit-title hide">Chỉnh sửa</span>
          <span class="add-title">Thêm mục mới</span>
        </h4>
      </div>
      <form action="" id="invoice_item_form" method="post" accept-charset="utf-8" novalidate="novalidate" class="dirty">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

        <input type="hidden" name="itemid" value="">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="alert alert-warning affect-warning hide">
                Chỉnh sửa thông tin sản phẩm không ảnh hưởng đến hóa đơn/báo giá/đề xuất đã tạo.
                <input type="hidden" id="id-service" name="id_service" class="form-control" value="">
              </div>
              <div class="form-group" app-field-wrapper="description"><label for="description" class="control-label"> <small class="req text-danger">* </small>Dịch vụ</label><input type="text" required id="description" name="description" class="form-control" value=""></div>                        <div class="form-group" app-field-wrapper="long_description"><label for="long_description" class="control-label">Mô tả </label><textarea id="long_description" name="long_description" class="form-control" rows="4"></textarea></div>                        <div class="form-group">
                <label for="rate" class="control-label"> <small class="req text-danger">* </small>
                  Tỷ lệ - VNĐ <small>(Base Currency)</small></label>
                  <input type="number" required id="rate" name="rate" class="form-control" value="" min="0" oninput="this.value=(this.value   < Number(this.min) )  ? '' : this.value;">
                </div>
                <div class="form-group" app-field-wrapper="unit"><label for="unit" class="control-label">Đơn vị</label><input type="text" id="unit" name="unit" class="form-control" value=""></div>                <div id="custom_fields_items">
                </div>


              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <button type="submit" class="btn btn-info">Lưu lại</button>
          </div></form>
        </div>
      </div>
    </div>

    <?php init_tail();?>
    <script type="text/javascript">
      function formatCurrency(number){
        var n = number.split('').reverse().join("");
        var n2 = n.replace(/\d\d\d(?!$)/g, "$&,");
        return  n2.split('').reverse().join('') ;
      }

      $(".edit-service").click(function(){
        var id=$(this).attr('data-id');

        $.ajax({
          type:"GET",
          url:admin_url+'expenses/edit_service_chi',
          data:{
            id:id
          },
          success:function(data){
           data=JSON.parse(data);
           if(data!==null){
            $("#id-service").val(id);
            $("#description").val(data['description_chi']);
            $("#long_description").val(data['long_description_chi']);
            $("#rate").val(formatCurrency(data['rate_chi']));
            $("#unit").val(data['unit_chi']);
            $("#invoice_item_form").attr('action',admin_url+'expenses/postedit_chi');
            $('#sales_item_modal').modal('show');
          }
        },
        error:function(data){
          console.log(data);
        }
      });
      });
    </script>
  </body>
  </html>
