<?php init_head(); ?>
<script type="text/javascript">
//<![CDATA[
function Pager(tableName, itemsPerPage) {
    this.tableName = tableName;
    this.itemsPerPage = itemsPerPage;
    this.currentPage = 1;
    this.pages = 0;
    this.inited = false;

    this.showRecords = function(from, to) {
        var rows = document.getElementById(tableName).rows;
        // i starts from 1 to skip table header row
        for (var i = 1; i < rows.length; i++) {
            if (i < from || i > to)
                rows[i].style.display = 'none';
            else
                rows[i].style.display = '';
        }
    }

    this.showPage = function(pageNumber) {
          if (! this.inited) {
                   alert("not inited");
                   return;
          }
        var oldPageAnchor = document.getElementById('pg'+this.currentPage);
        oldPageAnchor.className = 'pg-normal';

        this.currentPage = pageNumber;
        var newPageAnchor = document.getElementById('pg'+this.currentPage);
        newPageAnchor.className = 'pg-selected';

        var from = (pageNumber - 1) * itemsPerPage + 1;
        var to = from + itemsPerPage - 1;
        this.showRecords(from, to);
    }

    this.prev = function() {
        if (this.currentPage > 1)
            this.showPage(this.currentPage - 1);
    }

    this.next = function() {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);
        }
    }

    this.init = function() {
        var rows = document.getElementById(tableName).rows;
        var records = (rows.length - 1);
        this.pages = Math.ceil(records / itemsPerPage);
        this.inited = true;
    }
    this.showPageNav = function(pagerName, positionId) {
          if (! this.inited) {
                   alert("not inited");
                   return;
          }
          var element = document.getElementById(positionId);

          var pagerHtml = '<li  id="DataTables_Table_0_previous" onclick="' + pagerName + '.prev();" class="pg-normal paginate_button previous "> <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">Lùi lại</a> </li>  ';
        for (var page = 1; page <= this.pages; page++)
            pagerHtml += '<li id="pg' + page + '" class="pg-normal paginate_button previous " onclick="' + pagerName + '.showPage(' + page + ');"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">' + page + '</a></li>  ';
        pagerHtml += '<li onclick="'+pagerName+'.next();" class="pg-normal paginate_button previous "> <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">Đi tới</a></li>';

        element.innerHTML = pagerHtml;
    }
}
//]]>
</script>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if(has_permission('expenses','','create')){ ?>
                  <a href="<?php echo admin_url('expenses/tien_coc'); ?>" class="btn btn-info">Thêm mới Hợp đồng cọc</a>
                  <?php } ?>
                 <!--  <?php $this->load->view('admin/expenses/filter_by_template'); ?>
                  <a href="#" onclick="slideToggle('#stats-top'); return false;" class="pull-right btn btn-default mleft5 btn-with-tooltip" data-toggle="tooltip" title="<?php echo _l('view_stats_tooltip'); ?>"><i class="fa fa-bar-chart"></i></a>
                  <a href="#" class="btn btn-default pull-right btn-with-tooltip toggle-small-view hidden-xs" onclick="toggle_small_view('.table-expenses','#expense'); return false;" data-toggle="tooltip" title="<?php echo _l('invoices_toggle_table_tooltip'); ?>"><i class="fa fa-angle-double-left"></i></a> -->
                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="expenses_total"></div>
                  </div>
                  <form style="float: right;" action="" id="invoice-form" class="_transaction_form invoice-form" method="get" accept-charset="utf-8" novalidate="novalidate">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                   <div class="row">
                     <div class="col-md-6">

                        <?php echo render_date_input('start_date', 'contract_start_date', $_GET['start_date']); ?>
                     </div>
                     <div class="col-md-6">
                        <?php echo render_date_input('end_date', 'contract_end_date', $_GET['end_date']); ?>
                     </div>
                  </div>
                <div class="row">
                  <div class="col-md-6">
                    <label>Khách Hàng</label>
                    <select id="clientid" name="client" data-live-search="true" data-width="100%" class="ajax-search" data-none-selected-text="Không có mục nào được chọn" tabindex="-98" title="Chọn và bắt đầu gõ">
                      <option class="bs-title-option" value=""></option>
                    <?php
                    foreach ($client as $value) {
                      if($_GET['client']==$value['userid']){
                        echo '<option selected value="'.$value['userid'].'">'.$value['company'].'</option>';
                      }else{
                        echo '<option value="'.$value['userid'].'">'.$value['company'].'</option>';
                      }

                    }
                    ?>



                    </select>
                  </div>
                </div>
                    <button style="float: right;" class="btn-info btn">Xem</button>
                  </form>
               </div>
            </div>
            <div class="row">

               <div class="col-md-12" id="small-table">
                  <div class="panel_s">
                     <div class="panel-body">
                        <div class="clearfix"></div>
                        <!-- if expenseid found in url -->


                          <?php
$conn = mysqli_connect("localhost", "root","RedbeeX0605@","lienviet");
mysqli_set_charset($conn,"utf8");

if($_GET['client']){
  $kh = $_GET['client'];
  $where_k = "and `khach_hang`=$kh";
}
if($_GET['end_date']&&$_GET['start_date']){
  $where = " and `ngay_coc`>'".$_GET['start_date']."' and `han_coc`<'".$_GET['end_date']."'";

  $where1 = " and `ngay_coc`>'".$_GET['start_date']."' and `han_coc`<'".$_GET['end_date']."'";
}
$sql = "SELECT * FROM `btl_coc` LEFT JOIN `tblclients` ON `btl_coc`.`khach_hang`=`tblclients`.`userid` where 1 $where_k $where";
$sql_khong = "SELECT SUM(`tien_coc`) FROM `btl_coc` WHERE  `trang_thai`=3 $where1 $where_k";
$sql_co = "SELECT SUM(`tien_coc`) FROM `btl_coc` WHERE  `trang_thai`=2 $where1 $where_k";
$sql_new = "SELECT SUM(`tien_coc`) FROM `btl_coc` WHERE `trang_thai`=1 $where1 $where_k";
// $sql_lk = "SELECT SUM(`tien_coc`) FROM `btl_coc` WHERE `trang_thai`=3 $where1 $where_k";

$data = mysqli_query($conn,$sql);
$data_khong = mysqli_query($conn,$sql_khong);
$data_co = mysqli_query($conn,$sql_co);
$data_new = mysqli_query($conn,$sql_new);
$data_lk = mysqli_query($conn,$sql_lk);
$val = mysqli_fetch_assoc($data_khong);
$val2 = mysqli_fetch_assoc($data_co);
$val3 = mysqli_fetch_assoc($data_new);
// $val4 = mysqli_fetch_assoc($data_lk);
      ?>
<table class="table table-bordered table-hover expenses-report" id="expenses-report-table">
      <thead>
          <tr>
            <th class="bold">Tên hợp đồng</th>
            <th class="bold"> Khách Hàng</th>
            <th class="bold"> Tiền cọc</th>
            <th class="bold"> Trạng thái </th>
            <th class="bold"> Hạn hợp đồng</th>

          </tr>
            </thead>
            <tbody>
              <?php
            foreach ($data as $value) {
              if($value['han_coc']<date('Y-m-d')){
                $style = 'style="background: bisque;"';
              }else{
                $style = "";
              }
              ?>
              <!-- <?php if($value['trang_thai']=='2' || $value['trang_thai']=='3'){ echo 'style="pointer-events:none"'; } ?> -->
              <tr >
                <td <?php echo $style; ?>><a href="tien_coc/<?php echo $value['id_coc'] ?>"><?php echo $value['hopdong'] ?></a></td>
                <td <?php echo $style; ?>><?php echo $value['company'] ?></td>
                <td <?php echo $style; ?>><?php echo number_format($value['tien_coc'],0)  ?></td>
                <td <?php echo $style; ?>><?php
                if($value['trang_thai']==1){
                  echo "Đang hoạt động";
                }elseif($value['trang_thai']==2){
                   echo "Đã hủy và trả tiền";
                }elseif($value['trang_thai']==0){
                   echo "chưa hoạt động";
                }elseif($value['trang_thai']==3){
                   echo "Đã hủy";
                }elseif($value['trang_thai']==5){
                   echo "Thanh lý trả tiền cọc";
                }elseif($value['trang_thai']==6){
                   echo "Thanh lý giữ  tiền cọc";
                }else{
                  echo "hết hạn";
                }

                 ?></td>
                <td <?php echo $style; ?>><?php echo $value['han_coc'] ?></td>

              </tr>
             <?php }
              ?>
            </tbody>
        </table>
         <ul class="pagination" id="pageNavPosition">

                  </ul>
                   <div>
                    <p style="font-size: 18px;">Tổng tiền cọc thanh lý hợp đồng đã thanh toán: <?php echo number_format($val2['SUM(`tien_coc`)']); ?>đ</p></br>
                    <p style="font-size: 18px;">Tổng tiền cọc thanh lý hợp đồng đã được nhận: <?php echo number_format($val['SUM(`tien_coc`)']); ?>đ</p></br>
                    <p style="font-size: 18px;">Tổng tiền cọc đang hoạt động: <?php echo number_format($val3['SUM(`tien_coc`)']); ?>đ</p></br>
                  </div>
                  </div>
                  </div>

               </div>
               <div class="col-md-7 small-table-right-col">
                  <div id="expense" class="hide">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="expense_convert_helper_modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo _l('additional_action_required'); ?></h4>
         </div>
         <div class="modal-body">
            <div class="radio radio-primary">
               <input type="radio" checked id="expense_convert_invoice_type_1" value="save_as_draft_false" name="expense_convert_invoice_type">
               <label for="expense_convert_invoice_type_1"><?php echo _l('convert'); ?></label>
            </div>
            <div class="radio radio-primary">
               <input type="radio" id="expense_convert_invoice_type_2" value="save_as_draft_true" name="expense_convert_invoice_type">
               <label for="expense_convert_invoice_type_2"><?php echo _l('convert_and_save_as_draft'); ?></label>
            </div>
            <div id="inc_field_wrapper">
               <hr />
               <p><?php echo _l('expense_include_additional_data_on_convert'); ?></p>
               <p><b><?php echo _l('expense_add_edit_description'); ?> +</b></p>
               <div class="checkbox checkbox-primary inc_note">
                  <input type="checkbox" id="inc_note">
                  <label for="inc_note"><?php echo _l('expense'); ?> <?php echo _l('expense_add_edit_note'); ?></label>
               </div>
               <div class="checkbox checkbox-primary inc_name">
                  <input type="checkbox" id="inc_name">
                  <label for="inc_name"><?php echo _l('expense'); ?> <?php echo _l('expense_name'); ?></label>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-info" id="expense_confirm_convert"><?php echo _l('confirm'); ?></button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<style type="text/css">
  .pg-normal {
       color: red;
       font-weight: normal;
       text-decoration: none;
       cursor: pointer;
}

.pg-selected {
       color: black;
       font-weight: bold;
       text-decoration: underline;
       cursor: pointer;
}
</style>
<script type="text/javascript"><!--
       var pager = new Pager('expenses-report-table', 15);
       pager.init();
       pager.showPageNav('pager', 'pageNavPosition');
       pager.showPage(1);
//--></script>
<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail(); ?>
<script>
   Dropzone.autoDiscover = false;
   $(function(){
             // Expenses additional server params
             var Expenses_ServerParams = {};
             $.each($('._hidden_inputs._filters input'),function(){
               Expenses_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
             });
             initDataTable('.table-expenses', admin_url+'expenses/table', 'undefined', 'undefined', Expenses_ServerParams, <?php echo do_action('expenses_table_default_order',json_encode(array(5,'desc'))); ?>).column(0).visible(false, false).columns.adjust();

             init_expense();

             $('#expense_convert_helper_modal').on('show.bs.modal',function(){
                var emptyNote = $('#tab_expense').attr('data-empty-note');
                var emptyName = $('#tab_expense').attr('data-empty-name');
                if(emptyNote == '1' && emptyName == '1') {
                    $('#inc_field_wrapper').addClass('hide');
                } else {
                    $('#inc_field_wrapper').removeClass('hide');
                    emptyNote === '1' && $('.inc_note').addClass('hide') || $('.inc_note').removeClass('hide')
                    emptyName === '1' && $('.inc_name').addClass('hide') || $('.inc_name').removeClass('hide')
                }
             });

             $('body').on('click','#expense_confirm_convert',function(){
              var parameters = new Array();
              if($('input[name="expense_convert_invoice_type"]:checked').val() == 'save_as_draft_true'){
                parameters['save_as_draft'] = 'true';
              }
              parameters['include_name'] = $('#inc_name').prop('checked');
              parameters['include_note'] = $('#inc_note').prop('checked');
              window.location.href = buildUrl(admin_url+'expenses/convert_to_invoice/'+$('body').find('.expense_convert_btn').attr('data-id'), parameters);
            });
           });

</script>
</body>
</html>
