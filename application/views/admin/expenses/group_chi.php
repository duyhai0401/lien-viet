<?php init_head(); ?>
<script type="text/javascript">
//<![CDATA[
function Pager(tableName, itemsPerPage) {
    this.tableName = tableName;
    this.itemsPerPage = itemsPerPage;
    this.currentPage = 1;
    this.pages = 0;
    this.inited = false;
   
    this.showRecords = function(from, to) {      
        var rows = document.getElementById(tableName).rows;
        // i starts from 1 to skip table header row
        for (var i = 1; i < rows.length; i++) {
            if (i < from || i > to)
                rows[i].style.display = 'none';
            else
                rows[i].style.display = '';
        }
    }
   
    this.showPage = function(pageNumber) {
          if (! this.inited) {
                   alert("not inited");
                   return;
          }
        var oldPageAnchor = document.getElementById('pg'+this.currentPage);
        oldPageAnchor.className = 'pg-normal';
       
        this.currentPage = pageNumber;
        var newPageAnchor = document.getElementById('pg'+this.currentPage);
        newPageAnchor.className = 'pg-selected';
       
        var from = (pageNumber - 1) * itemsPerPage + 1;
        var to = from + itemsPerPage - 1;
        this.showRecords(from, to);
    }  
   
    this.prev = function() {
        if (this.currentPage > 1)
            this.showPage(this.currentPage - 1);
    }
   
    this.next = function() {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);
        }
    }                      
   
    this.init = function() {
        var rows = document.getElementById(tableName).rows;
        var records = (rows.length - 1);
        this.pages = Math.ceil(records / itemsPerPage);
        this.inited = true;
    }
    this.showPageNav = function(pagerName, positionId) {
          if (! this.inited) {
                   alert("not inited");
                   return;
          }
          var element = document.getElementById(positionId);
         
          var pagerHtml = '<li  id="DataTables_Table_0_previous" onclick="' + pagerName + '.prev();" class="pg-normal paginate_button previous "> <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">Lùi lại</a> </li>  ';
        for (var page = 1; page <= this.pages; page++)
            pagerHtml += '<li id="pg' + page + '" class="pg-normal paginate_button previous " onclick="' + pagerName + '.showPage(' + page + ');"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">' + page + '</a></li>  ';
        pagerHtml += '<li onclick="'+pagerName+'.next();" class="pg-normal paginate_button previous "> <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">Đi tới</a></li>';          
       
        element.innerHTML = pagerHtml;
    }
}
//]]>
</script>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <h2>Danh mục dịch vụ theo tòa nhà</h2>
                  
                  <style type="text/css">
                    #invoice-form{
                      padding-right: 130px;
                      position: relative;
                      margin-top: 30px;
                    }
                    #invoice-form .input-content{
                      margin: 0 -15px;
                    }
                    #invoice-form .input-content:before{
                      content: '';
                      display: table;
                    }
                    #invoice-form .input-content:after{
                      content: '';
                      display: table;
                      clear: both;
                    }
                    .item-input{
                      width: 33.33%;
                      float: left;
                      padding:  0 15px;
                      height: 61px;
                    }
                    .item-input .dropdown.bootstrap-select{
                      width: 100% !important;
                    }
                    #invoice-form .btn-info.btn{
                      width: 100px;
                      position: absolute;
                      top: 25px;
                      right: 0;
                      height: 34px;
                      text-align: center;
                    }
                    @media only screen and (max-width:767px){
                       #invoice-form{
                        max-width: 500px;
                        margin-left: auto;
                        margin-right: auto;
                        padding-right: 0;
                       }
                      .item-input{
                        width: 100%;
                        float: left;
                        padding:  0 15px;
                        height: 61px;
                      }
                      #invoice-form .btn-info.btn{
                        width: 100%;
                        position: relative;
                        top: 0px;
                        right: 0;
                        height: 34px;
                      }
                    }
                  </style>
                  
              
                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="expenses_total"></div>
                  </div>
                  <form action="" id="invoice-form" class="_transaction_form invoice-form" method="post" accept-charset="utf-8" novalidate="novalidate">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                  <div class="input-content">
                    <div class="item-input" style="margin-bottom: 20px;">
                     <label>Chọn Tòa Nhà</label>
                  <select id="name_building" name="name_building"  class="selectpicker _select_input_group" data-width="100%" data-none-selected-text="Không có mục nào được chọn" data-live-search="true" tabindex="-98">
                        <option value=""></option>
                         <?php
                    foreach ($items_groups as $key => $value) {
                      if ($value['id'] == $_GET['name_building']) {
                        echo '<option selected value="' . $value['id'] . '">' . $value['name'] . '</option>';
                      } else {
                        echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                      }

                    }

                    ?>
                      </select>
                    </div>
                    <div class="item-input" style="margin-bottom: 20px;">
                  <label>Chọn dịch vụ </label>
                      <select class="selectpicker" name="service" onchange="myFunction_chage()">
                        <option value="">Lựa chọn</option>
                       <?php 
                        foreach ($ims as $key => $value) {
                     
                        echo '<option value="' . $value['id_chi'] . '">' . $value['description_chi'] . '</option>';
                      

                    }
                     ?>
                      </select>

                    </div>
                  
                  </div>
                  <label>Hạng mục chi cho tòa nhà</label>
                    <div class="index">
                      <table id="p" class="table">
                        <tr>
                          <th>Tên dịch vụ</th>
                          <th>Đơn giá</th>
                          <th>đơn vị tính</th>
                        </tr>
                      </table>
                    </div>
                    <button style="position: initial; float: right;" type="submit" class="btn-info btn" name="save-btn">Lưu</button>
                  </form>
               </div>
            </div>
            <div class="row">

               <div class="col-md-12" id="small-table">
                  <div class="panel_s">
                     <div class="panel-body">
                        <div class="clearfix"></div>
                        <!-- if expenseid found in url -->
                        <?php echo form_hidden('expenseid',$expenseid); ?>
   
     

<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

<input type="text" class="hidden" name="nam" value="<?php echo $_GET['nam']?>">
<input type="text" class="hidden" name="thang" value="<?php echo $_GET['thang']?>">
<style type="text/css">
  .table-content{
    overflow: auto;
  }
  #expenses-report-table.table>thead>tr>th{
    white-space: nowrap;
  }

</style>
<div class="table-content">
  <form action="" method="GET">
   <div class="item-input" style="margin-bottom: 20px;">
                     <label>Chọn Tòa Nhà</label>
                  <select id="name_building2" name="name_building2"  class="selectpicker _select_input_group" data-width="100%" data-none-selected-text="Không có mục nào được chọn" data-live-search="true" tabindex="-98">
                        <option value=""></option>
                         <?php
                    foreach ($items_groups as $key => $value) {
                      if ($value['id'] == $_GET['name_building2']) {
                        echo '<option selected value="' . $value['id'] . '">' . $value['name'] . '</option>';
                      } else {
                        echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                      }

                    }

                    ?>
                      </select>
                    </div>
                    <button style="float: right;" class="btn-info btn" name="search-btn" type="submit">Xem</button>
        </form>   

  <table class="table table-bordered table-hover expenses-report" id="expenses-report-table">
      <thead>
          <tr>
            <th  class="bold">Tên Tòa nhà</th>
            <th  class="bold">Tên dịch vụ</th> 
            <th  class="bold">Đơn giá</th> 
          </tr>
            </thead>
            <tbody>
             <?php
             foreach ($data as $key => $value) {
             ?>                    
            <tr style="border: 1px solid black" >
              <td class="bold text-info" style="font-size: 20px;"><?php echo $value['name'] ?></td>
              <td class="bold"><?php echo $value['name_itemp'] ?></td>
              <td class="bold"><?php echo number_format($value['rate_itemp'],0) ?></td>
            </tr>
             
           <?php }?>

            </tbody>
        </table>
   </div>
      
        <!-- <button style="float: right;position: initial;" class="btn-info btn" >Lưu</button> -->
         <ul class="pagination" id="pageNavPosition">
            
                  </ul>
          </div>
                  </div>
               </div>
               <div class="col-md-7 small-table-right-col">
                  <div id="expense" class="hide">
                  </div>
               </div>
            </div>
         </div>
      </div>
</div>
</div>


<!-- /.modal -->
<style type="text/css">
  .pg-normal {
       color: red;
       font-weight: normal;
       text-decoration: none;  
       cursor: pointer;  
}

.pg-selected {
       color: black;
       font-weight: bold;     
       text-decoration: underline;
       cursor: pointer;
}
</style>
<script type="text/javascript"><!--
       var pager = new Pager('expenses-report-table', 15);
       pager.init();
       pager.showPageNav('pager', 'pageNavPosition');
       pager.showPage(1);
//--></script>
<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail(); ?>
<script>
  function myFunction_chage(){
    var id = $('select[name="service"]').val();

    requestGetJSON("expenses/get_item_service/" + id).done(function (re) {
      console.log(re);
       $("#p").append("<tr><td><input type='text' name='name_dv[]' value='"+re[0].description_chi+"' class='form-control'/><input type='text' name='id_item[]' value='"+re[0].id_chi+"' class='hidden'/></td><td><input type='text' name='rate_dv[]' class='rate' value='"+re[0].rate_chi+"'  class='form-control'/></td><td>"+re[0].unit_chi+"</td><td><span onclick='delete_item(this,undefined); return false;' class='btn btn-danger pull-left'><i class='fa fa-trash'></i></span></td></tr>");
    });
  }
  function delete_item(e, t) {
    $(e).parents("tr").addClass("animated fadeOut", function () {
        setTimeout(function () {
            $(e).parents("tr").remove(), calculate_total()
        }, 50)
    }), $('input[name="isedit"]').length > 0 && $("#removed-items").append(hidden_input("removed_items[]", t))
}
   Dropzone.autoDiscover = false;
   $(function(){
             // Expenses additional server params
             var Expenses_ServerParams = {};
             $.each($('._hidden_inputs._filters input'),function(){
               Expenses_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
             });
             initDataTable('.table-expenses', admin_url+'expenses/table', 'undefined', 'undefined', Expenses_ServerParams, <?php echo do_action('expenses_table_default_order',json_encode(array(5,'desc'))); ?>).column(0).visible(false, false).columns.adjust();

             init_expense();

             $('#expense_convert_helper_modal').on('show.bs.modal',function(){
                var emptyNote = $('#tab_expense').attr('data-empty-note');
                var emptyName = $('#tab_expense').attr('data-empty-name');
                if(emptyNote == '1' && emptyName == '1') {
                    $('#inc_field_wrapper').addClass('hide');
                } else {
                    $('#inc_field_wrapper').removeClass('hide');
                    emptyNote === '1' && $('.inc_note').addClass('hide') || $('.inc_note').removeClass('hide')
                    emptyName === '1' && $('.inc_name').addClass('hide') || $('.inc_name').removeClass('hide')
                }
             });

             $('body').on('click','#expense_confirm_convert',function(){
              var parameters = new Array();
              if($('input[name="expense_convert_invoice_type"]:checked').val() == 'save_as_draft_true'){
                parameters['save_as_draft'] = 'true';
              }
              parameters['include_name'] = $('#inc_name').prop('checked');
              parameters['include_note'] = $('#inc_note').prop('checked');
              window.location.href = buildUrl(admin_url+'expenses/convert_to_invoice/'+$('body').find('.expense_convert_btn').attr('data-id'), parameters);
            });
           });
</script>
</body>
</html>
