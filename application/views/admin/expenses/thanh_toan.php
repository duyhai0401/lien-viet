<?php init_head(); ?>
<script type="text/javascript">
//<![CDATA[
function Pager(tableName, itemsPerPage) {
    this.tableName = tableName;
    this.itemsPerPage = itemsPerPage;
    this.currentPage = 1;
    this.pages = 0;
    this.inited = false;
   
    this.showRecords = function(from, to) {      
        var rows = document.getElementById(tableName).rows;
        // i starts from 1 to skip table header row
        for (var i = 1; i < rows.length; i++) {
            if (i < from || i > to)
                rows[i].style.display = 'none';
            else
                rows[i].style.display = '';
        }
    }
   
    this.showPage = function(pageNumber) {
          if (! this.inited) {
                   alert("not inited");
                   return;
          }
        var oldPageAnchor = document.getElementById('pg'+this.currentPage);
        oldPageAnchor.className = 'pg-normal';
       
        this.currentPage = pageNumber;
        var newPageAnchor = document.getElementById('pg'+this.currentPage);
        newPageAnchor.className = 'pg-selected';
       
        var from = (pageNumber - 1) * itemsPerPage + 1;
        var to = from + itemsPerPage - 1;
        this.showRecords(from, to);
    }  
   
    this.prev = function() {
        if (this.currentPage > 1)
            this.showPage(this.currentPage - 1);
    }
   
    this.next = function() {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);
        }
    }                      
   
    this.init = function() {
        var rows = document.getElementById(tableName).rows;
        var records = (rows.length - 1);
        this.pages = Math.ceil(records / itemsPerPage);
        this.inited = true;
    }
    this.showPageNav = function(pagerName, positionId) {
          if (! this.inited) {
                   alert("not inited");
                   return;
          }
          var element = document.getElementById(positionId);
         
          var pagerHtml = '<li  id="DataTables_Table_0_previous" onclick="' + pagerName + '.prev();" class="pg-normal paginate_button previous "> <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">Lùi lại</a> </li>  ';
        for (var page = 1; page <= this.pages; page++)
            pagerHtml += '<li id="pg' + page + '" class="pg-normal paginate_button previous " onclick="' + pagerName + '.showPage(' + page + ');"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">' + page + '</a></li>  ';
        pagerHtml += '<li onclick="'+pagerName+'.next();" class="pg-normal paginate_button previous "> <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">Đi tới</a></li>';          
       
        element.innerHTML = pagerHtml;
    }
}
//]]>
</script>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if(has_permission('expenses','','create')){ ?>
                  <a href="<?php echo admin_url('expenses/hopdong'); ?>" class="btn btn-info">Thêm mới Hợp đồng thuê</a>
                  <?php } ?>
                 <!--  <?php $this->load->view('admin/expenses/filter_by_template'); ?>
                  <a href="#" onclick="slideToggle('#stats-top'); return false;" class="pull-right btn btn-default mleft5 btn-with-tooltip" data-toggle="tooltip" title="<?php echo _l('view_stats_tooltip'); ?>"><i class="fa fa-bar-chart"></i></a>
                  <a href="#" class="btn btn-default pull-right btn-with-tooltip toggle-small-view hidden-xs" onclick="toggle_small_view('.table-expenses','#expense'); return false;" data-toggle="tooltip" title="<?php echo _l('invoices_toggle_table_tooltip'); ?>"><i class="fa fa-angle-double-left"></i></a> -->
                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="expenses_total"></div>
                  </div>
                  <?php
                  $nam = $_GET['nam'];
$thang = $_GET['thang'];
$build = $_GET['filter-build'];
$status = $_GET['filter-status'];
$year=date('Y');
                  ?>
                   <div class="row" style="margin-top: 10px;">

                  <form  action="" id="invoice-form" class="_transaction_form invoice-form" method="get" accept-charset="utf-8" novalidate="novalidate">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                       <div class="item-input col-md-3" style="margin-bottom: 20px;">
                  <label>Tòa nhà</label>
                   <?php
                        $html=' <option  value="" selected disabled>Chọn tòa nhà</option>';
                       if(sizeof($builds)>0){
                        foreach ($builds as $key => $value) {
                        $html.='<option ';
                        if($build!="" && $build==$value['id']){
                          $html.=' selected';
                        }
                        $html.=' value="'.$value['id'].'">'.$value['name'].'</option>';
                        }
                      }
                       ?>
                      <select class="selectpicker" name="filter-build">
                       <?php
                       echo $html;
                       ?>
                      </select>
                    </div>
                        <div class="item-input col-md-3" style="margin-bottom: 20px;">
                  <label>Trạng thái</label>
                      <select class="selectpicker" name="filter-status">
                        <option selected="" disabled="" value="">Chọn trạng thái</option>
                        <option value="0" <?php if($status=='0'){ echo "selected";} ?>>Chưa thanh toán</option>
                        <option value="1" <?php if($status=='1'){ echo "selected";} ?>>Đã thanh toán</option>
                        <option value="2" <?php if($status=='2'){ echo "selected";} ?>>Thanh toán một nửa</option>
                      </select>

                    </div>
                    <div class="item-input col-md-3" style="margin-bottom: 20px;">
                    <label>Chọn tháng</label>
                      <select class="selectpicker" name="thang">
                        <option selected="" disabled="" value="">Chọn tháng</option>
                        <option id="1" <?php if($_GET['thang']=='1'){ echo "selected";} ?> value="1">1</option>
                        <option id="2" <?php if($_GET['thang']=='2'){ echo "selected";} ?> value="2">2</option>
                        <option id="3" <?php if($_GET['thang']=='3'){ echo "selected";} ?> value="3">3</option>
                        <option id="4" <?php if($_GET['thang']=='4'){ echo "selected";} ?> value="4">4</option>
                        <option id="5" <?php if($_GET['thang']=='5'){ echo "selected";} ?> value="5">5</option>
                        <option id="6" <?php if($_GET['thang']=='6'){ echo "selected";} ?> value="6">6</option>
                        <option id="7" <?php if($_GET['thang']=='7'){ echo "selected";} ?> value="7">7</option>
                        <option id="8" <?php if($_GET['thang']=='8'){ echo "selected";} ?> value="8">8</option>
                        <option id="9" <?php if($_GET['thang']=='9'){ echo "selected";} ?> value="9">9</option>
                        <option id="10" <?php if($_GET['thang']=='10'){ echo "selected";} ?> value="10">10</option>
                        <option id="11" <?php if($_GET['thang']=='11'){ echo "selected";} ?> value="11">11</option>
                        <option id="12" <?php if($_GET['thang']=='12'){ echo "selected";} ?> value="12">12</option>
                      </select>
                    </div>
                    <div class="item-input col-md-3" style="margin-bottom: 20px;">
                    <label>Chọn năm</label>
                      <select class="selectpicker" name="nam">
                        <option selected="" disabled="" value="">Chọn năm</option>
                       
                        <option id="2019" <?php if($_GET['nam']=='2019'){ echo "selected";} ?> value="2019">2019</option>
                        <option id="2020" <?php if($_GET['nam']=='2020'){ echo "selected";} ?> value="2020">2020</option>
                        <option id="2021" <?php if($_GET['nam']=='2021'){ echo "selected";} ?> value="2021">2021</option>
                        <option id="2022" <?php if($_GET['nam']=='2022'){ echo "selected";} ?> value="2022">2022</option>
                        <option id="2023" <?php if($_GET['nam']=='2023'){ echo "selected";} ?> value="2023">2023</option>
                        <option id="2024" <?php if($_GET['nam']=='2024'){ echo "selected";} ?> value="2024">2024</option>
                        <option id="2025" <?php if($_GET['nam']=='2025'){ echo "selected";} ?> value="2025">2025</option>
                        <option id="2026" <?php if($_GET['nam']=='2026'){ echo "selected";} ?> value="2026">2026</option>
                        <option id="2027" <?php if($_GET['nam']=='2027'){ echo "selected";} ?> value="2027">2027</option>
                      </select>

                    </div>
             
                    <button style="float: right;" class="btn-info btn">Xem</button>
                  </form>
                  </div>

               </div>
            </div>
            <div class="row">

               <div class="col-md-12" id="small-table">
                  <div class="panel_s">
                     <div class="panel-body">
                        <div class="clearfix"></div>
                        <!-- if expenseid found in url -->
         
<p class="mbot20 text-info">Mục báo cáo chỉ được tạo từ chi phí sau thuế.</p>      

<table class="table table-bordered table-hover expenses-report" id="expenses-report-table">
      <thead>
          <tr>
            <th class="bold">Tên tòa nhà</th>
            <th class="bold">Dịch vụ</th>                                             
            <th class="bold"> Thành tiền</th>
            <th class="bold"> Thanh toán</th>
            <th class="bold"> Chưa thanh toán</th>
            <th class="bold"> Trạng thái</th>
            <th class="bold"> Hành động</th>
          </tr>
            </thead>
            <tbody>
<?php 
$conn = mysqli_connect("localhost", "root","RedbeeX0605","lienviet");
mysqli_set_charset($conn,"utf8");


$this->db->select('*')->from('tbl_hd_thue')->join('tblitems_groups','`tbl_hd_thue`.`building`=`tblitems_groups`.`id`','left');


if($nam!=""){
$this->db->where('nam',$nam);
}else{
$this->db->where('nam',$year);
}

if($thang!=""){
$this->db->where('thang',$thang);

}
if($build!=""){
$this->db->where('building',$build);

}

$contracts=$this->db->get()->result_array();


$data_n = mysqli_query($conn,$sql);
$tong =0;
foreach ($contracts as $key => $value_d) 
 {

  $id = $value_d['id_thue'];

  $sql_sv = "SELECT * FROM `tbl_item_hd` LEFT JOIN `tlbdich_vu_chi` on `tbl_item_hd`.`item_hd`=`tlbdich_vu_chi`.`id_chi` LEFT JOIN `tblitems_groups` on `tbl_item_hd`.`item_hd`=`tblitems_groups`.`id` where `id_hd`=$id ";
$data = mysqli_query($conn,$sql_sv);
$data1 = mysqli_query($conn,$sql_sv);
$data2 = mysqli_query($conn,$sql_sv);
$data3 = mysqli_query($conn,$sql_sv);
$sql_sv1 = "SELECT COUNT(`id_hd`) FROM `tbl_item_hd` WHERE `id_hd`=$id";
$data31 = mysqli_query($conn,$sql_sv1);
$value1 = mysqli_fetch_assoc($data31);
$row = $value1['COUNT(`id_hd`)']+1;
?>
<form action="" method="post">
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
              <tr>
                
                <td rowspan="<?php echo $row; ?>"><?php echo $value_d['name'] ?></td>
              </tr>
               <?php
                  $tien = 0;
                  while ( $value = mysqli_fetch_assoc($data)) {
                    $tien = $tien + ($value['rate_im']*$value['qty_im']);
                   $val_tien = $value['rate_im']*$value['qty_im'];
                    $max_num = ($value['rate_im']*$value['qty_im'])-$value['thanh_toan'];
                    if($status!=""){
                     if($status==0 && $val_tien==$max_num){

                  ?>
                  <tr>
                   
                    <td><?php 
                    if($value['description_chi']){
                     echo $value['description_chi']; echo "&nbsp;:&nbsp;"; echo $value['qty_im']; }else {
                      echo "Tiền thuê phòng: "; echo "&nbsp;:&nbsp;"; echo $value['qty_im'];
                     }
                     
                     ?></td>
                   
                    <td><?php echo number_format($value['rate_im']*$value['qty_im'],2)  ?></td>
                    <td><input type="number" name="thanh_toan[]" class="form-control" max="<?php echo $max_num; ?>"></td>
                    <td><?php echo number_format($max_num,2)  ?><input type="text" name="id[]" class="hidden" value="<?php echo  $value['id_item']; ?>"></td>
                    <td>
                   <span class="label label-danger  s-status invoice-status-1">Chưa thanh toán</span>
                    
                    </td>
                    <td></td>
                    
                  </tr>
                
                <?php 
} elseif($status==1 && $max_num==0){
  ?>
    <tr>
                    <td><?php 
                    if($value['description_chi']){
                     echo $value['description_chi']; echo "&nbsp;:&nbsp;"; echo $value['qty_im']; }else {
                      echo "Tiền thuê phòng: "; echo "&nbsp;:&nbsp;"; echo $value['qty_im'];
                     }
                     
                     ?></td>
                   
                    <td><?php echo number_format($value['rate_im']*$value['qty_im'],2)  ?></td>
                    <td><input type="number" name="thanh_toan[]" class="form-control" max="<?php echo $max_num; ?>"></td>
                    <td><?php echo number_format($max_num,2)  ?><input type="text" name="id[]" class="hidden" value="<?php echo  $value['id_item']; ?>"></td>
                    <td>
                      <span class="label label-success  s-status invoice-status-2">Đã thanh toán</span>
                    </td>
                    <td></td>
                    
                  </tr>
  <?php
}elseif($status== 2){
  ?>
  <tr>
                    <td><?php 
                    if($value['description_chi']){
                     echo $value['description_chi']; echo "&nbsp;:&nbsp;"; echo $value['qty_im']; }else {
                      echo "Tiền thuê phòng: "; echo "&nbsp;:&nbsp;"; echo $value['qty_im'];
                     }
                     
                     ?></td>
                   
                    <td><?php echo number_format($value['rate_im']*$value['qty_im'],2)  ?></td>
                    <td><input type="number" name="thanh_toan[]" class="form-control" max="<?php echo $max_num; ?>"></td>
                    <td><?php echo number_format($max_num,2)  ?><input type="text" name="id[]" class="hidden" value="<?php echo  $value['id_item']; ?>"></td>
                    <td>
                     <span class="label label-warning  s-status invoice-status-3">Đã thanh toán một phần</span>
                    </td>
                    <td></td>
                    
                  </tr>
  <?php
}
              }else{
                ?>
                  <tr>
                   
                    <td><?php 
                    if($value['description_chi']){
                     echo $value['description_chi']; echo "&nbsp;:&nbsp;"; echo $value['qty_im']; }else {
                      echo "Tiền thuê phòng: "; echo "&nbsp;:&nbsp;"; echo $value['qty_im'];
                     }
                     
                     ?></td>
                   
                    <td><?php echo number_format($value['rate_im']*$value['qty_im'],2)  ?></td>
                    <td><input type="number" name="thanh_toan[]" class="form-control" max="<?php echo $max_num; ?>"></td>
                    <td><?php echo number_format($max_num,2)  ?><input type="text" name="id[]" class="hidden" value="<?php echo  $value['id_item']; ?>"></td>
                    <td>
                      <?php 
                    if($val_tien==$max_num){
                      echo '<span class="label label-danger  s-status invoice-status-1">Chưa thanh toán</span>';
                    }elseif ($max_num==0) {
                     echo '<span class="label label-success  s-status invoice-status-2">Đã thanh toán</span>';
                    }else{
                      echo '<span class="label label-warning  s-status invoice-status-3">Đã thanh toán một phần</span>';
                    }


                      ?>
                    
                    
                    </td>
                    <td></td>
                    
                  </tr>
                <?php
              }
            }
              ?>
            
              <tr>
                <td colspan="6">Tổng Tiền:<?php  $tong = $tong + $tien; echo number_format($tien,2);?></td>
                <td><button style="font-size: 20px;" class="btn-info">Lưu</button></td>
              </tr>
              </form>
           <?php 
         }

         ?>
            </tbody>
        </table>

        <div>
        
         
          <table>
            <tr>
              <th></th>
              <th></th>
              <th></th>
            </tr>
          <?php  $sql_chi = "SELECT * FROM `tlbdich_vu_chi`";
             $data_chi = mysqli_query($conn,$sql_chi);
             if($nam!=""&&$thang!=""){
              $wh = "and `nam_im`=$nam and `thang_im`=$thang";
             }else{
              $wh = "";
             }
             while ( $value_chi = mysqli_fetch_assoc($data_chi)) {
              $item = $value_chi['id_chi'];
              $tientong = "SELECT SUM(`rate_im`*`qty_im`) FROM `tbl_item_hd` where `item_hd` =$item $wh  ";
              $tientt = "SELECT SUM(`thanh_toan`) FROM `tbl_item_hd` where `item_hd` =$item  $wh ";
              $data_t = mysqli_query($conn,$tientong);
              $data_tt = mysqli_query($conn,$tientt);
              $value_t = mysqli_fetch_assoc($data_t);
              $value_tt = mysqli_fetch_assoc($data_tt);
               echo '<tr><td style="padding:5px;width:25%"> <span style="font-size: 14px;font-weight:bold;margin-left: ">Tổng '.$value_chi['description_chi'].' :'.number_format($value_t['SUM(`rate_im`*`qty_im`)'],0).'đ</span></td>';
              echo ' <td style="padding:5px;width:25%"><span style="font-size: 14px;font-weight:bold;margin-left:11%; ">Tổng '.$value_chi['description_chi'].' thanh toán:'.number_format($value_tt['SUM(`thanh_toan`)'],0).'đ</span></td>';
              echo '<td style="padding:5px;width:25%"> <span style="font-size: 14px;font-weight:bold;margin-left:11%; ">Tổng '.$value_chi['description_chi'].' Còn nợ:'.number_format($value_t['SUM(`rate_im`*`qty_im`)']-$value_tt['SUM(`thanh_toan`)'],0).'đ</span></td></tr>';
             }
          ?>
        
          </table>

           <span style="font-size: 20px;font-weight:bold;">Tổng Tiền Phải Trả:<?php echo number_format($tong,0); ?>đ</span>
        </div>
         <ul class="pagination" id="pageNavPosition">
            
                  </ul>
                              </div>
                  </div>
               </div>
               <div class="col-md-7 small-table-right-col">
                  <div id="expense" class="hide">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="expense_convert_helper_modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo _l('additional_action_required'); ?></h4>
         </div>
         <div class="modal-body">
            <div class="radio radio-primary">
               <input type="radio" checked id="expense_convert_invoice_type_1" value="save_as_draft_false" name="expense_convert_invoice_type">
               <label for="expense_convert_invoice_type_1"><?php echo _l('convert'); ?></label>
            </div>
            <div class="radio radio-primary">
               <input type="radio" id="expense_convert_invoice_type_2" value="save_as_draft_true" name="expense_convert_invoice_type">
               <label for="expense_convert_invoice_type_2"><?php echo _l('convert_and_save_as_draft'); ?></label>
            </div>
            <div id="inc_field_wrapper">
               <hr />
               <p><?php echo _l('expense_include_additional_data_on_convert'); ?></p>
               <p><b><?php echo _l('expense_add_edit_description'); ?> +</b></p>
               <div class="checkbox checkbox-primary inc_note">
                  <input type="checkbox" id="inc_note">
                  <label for="inc_note"><?php echo _l('expense'); ?> <?php echo _l('expense_add_edit_note'); ?></label>
               </div>
               <div class="checkbox checkbox-primary inc_name">
                  <input type="checkbox" id="inc_name">
                  <label for="inc_name"><?php echo _l('expense'); ?> <?php echo _l('expense_name'); ?></label>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-info" id="expense_confirm_convert"><?php echo _l('confirm'); ?></button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<style type="text/css">
  .pg-normal {
       color: red;
       font-weight: normal;
       text-decoration: none;  
       cursor: pointer;  
}

.pg-selected {
       color: black;
       font-weight: bold;     
       text-decoration: underline;
       cursor: pointer;
}
</style>
<script type="text/javascript"><!--
       var pager = new Pager('expenses-report-table', 20);
       pager.init();
       pager.showPageNav('pager', 'pageNavPosition');
       pager.showPage(1);
//--></script>
<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail(); ?>
<script>
   Dropzone.autoDiscover = false;
   $(function(){
             // Expenses additional server params
             var Expenses_ServerParams = {};
             $.each($('._hidden_inputs._filters input'),function(){
               Expenses_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
             });
             initDataTable('.table-expenses', admin_url+'expenses/table', 'undefined', 'undefined', Expenses_ServerParams, <?php echo do_action('expenses_table_default_order',json_encode(array(5,'desc'))); ?>).column(0).visible(false, false).columns.adjust();

             init_expense();

             $('#expense_convert_helper_modal').on('show.bs.modal',function(){
                var emptyNote = $('#tab_expense').attr('data-empty-note');
                var emptyName = $('#tab_expense').attr('data-empty-name');
                if(emptyNote == '1' && emptyName == '1') {
                    $('#inc_field_wrapper').addClass('hide');
                } else {
                    $('#inc_field_wrapper').removeClass('hide');
                    emptyNote === '1' && $('.inc_note').addClass('hide') || $('.inc_note').removeClass('hide')
                    emptyName === '1' && $('.inc_name').addClass('hide') || $('.inc_name').removeClass('hide')
                }
             });

             $('body').on('click','#expense_confirm_convert',function(){
              var parameters = new Array();
              if($('input[name="expense_convert_invoice_type"]:checked').val() == 'save_as_draft_true'){
                parameters['save_as_draft'] = 'true';
              }
              parameters['include_name'] = $('#inc_name').prop('checked');
              parameters['include_note'] = $('#inc_note').prop('checked');
              window.location.href = buildUrl(admin_url+'expenses/convert_to_invoice/'+$('body').find('.expense_convert_btn').attr('data-id'), parameters);
            });
           });
</script>
</body>
</html>
