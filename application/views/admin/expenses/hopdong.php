<?php init_head(); ?>

<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
         
            <div class="row">

               <div class="col-md-12" id="small-table">
                  <div class="panel_s">
                     <div class="panel-body">
                        <div class="clearfix"></div>
  <form method="post" action="">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
<div class="table-content col-md-6">
  <div>
   
      <label>Tòa Nhà</label>
                  <select onchange="group_in()" id="group" name="group" class="form-control">
                        <option value=""></option>
                      <?php foreach ($group as $value) {
                        if($data[0]['group_building']==$value['id']){
                        echo '<option selected value="'.$value['id'].'">'.$value['name'].'</option>';
                      }else{
                        echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                      }
                     
                      }?>
                     </select>
  </div>
 
    <div>
        <label>Thuế</label>
            <select id="thue" name="thue" class="form-control">
                        <option value=""></option>
                     <?php
foreach ($taxd as $key => $value) {
  if($data[0]['thue']){
    echo '<option selected  value="' . $value['taxrate'] . '">' . $value['name'] . '</option>';
  }else{
    echo '<option  value="' . $value['taxrate'] . '">' . $value['name'] . '</option>';
  }
  
}

?>

                     </select>
      </div>
    <div>
        <label>Địa chỉ tòa nhà</label>
        <input type="text" class="form-control" name="address" value="<?php echo $data[0]['address'] ?>">
      </div>
      <div>
        <label>Giá thuê hợp đồng/ tháng</label>
        <input type="text" class="form-control" id="gia_tri" name="gia_tri" value="<?php echo number_format($data[0]['gia_tri'],2) ?>" onchange="set_num()" politespace data-grouplength="3" data-delimiter="," data-reverse >
      </div>
      <div>
        <label>Tiền cọc</label>
        <input type="text" class="form-control" id="tien_coc" name="tien_coc" value="<?php echo number_format($data[0]['tien_coc'],2) ?>" onchange="set_num1()" politespace data-grouplength="3" data-delimiter="," data-reverse >
      </div>
      <div class="row">
                     <div class="col-md-6">
                        
                        <?php echo render_date_input('start_date', 'contract_start_date', $data[0]['start_date']); ?>
                     </div>
                     <div class="col-md-6">
                        <?php echo render_date_input('end_date', 'contract_end_date', $data[0]['end_date']); ?>
                     </div>
                  </div>
   </div>
    <div class="col-md-6">
      <div>
        <label>Tên Chủ Nhà</label>
        <input type="text" class="form-control" value="<?php echo $data[0]['name_chu'] ?>" name="name">
      </div>
      <div>
        <label>Địa Chỉ</label>
        <input type="text" class="form-control" name="dia_chi" value="<?php echo $data[0]['dia_chi'] ?>">
      </div>
      <div>
        <label>Số điện thoại</label>
        <input type="text" class="form-control" name="phone" value="<?php echo $data[0]['phone'] ?>">
      </div>
      <div>
        <label>Số CMND</label>
        <input type="text" class="form-control" name="cmnd" value="<?php echo $data[0]['cmnd'] ?>">
      </div>
      <div>
        <label>Kỳ thanh toán</label>
         <select class="selectpicker" data-width="100%" name="recurring" data-none-selected-text="Không có mục nào được chọn" tabindex="-98">
<option <?php if ($data[0]['recurring'] == 1) {
  echo "selected";
}
?> value="1">Mỗi 1 tháng</option>
<option <?php if ($data[0]['recurring'] == 2) {
  echo "selected";
}
?> value="2">Mỗi 2 tháng</option>
<option <?php if ($data[0]['recurring'] == 3) {
  echo "selected";
}
?> value="3">Mỗi 3 tháng</option>
<option <?php if ($data[0]['recurring'] == 4) {
  echo "selected";
}
?> value="4">Mỗi 4 tháng</option>
<option <?php if ($data[0]['recurring'] == 5) {
  echo "selected";
}
?> value="5">Mỗi 5 tháng</option>
<option <?php if ($data[0]['recurring'] == 6) {
  echo "selected";
}
?> value="6">Mỗi 6 tháng</option>
<option <?php if ($data[0]['recurring'] == 7) {
  echo "selected";
}
?> value="7">Mỗi 7 tháng</option>
<option <?php if ($data[0]['recurring'] == 8) {
  echo "selected";
}
?> value="8">Mỗi 8 tháng</option>
<option <?php if ($data[0]['recurring'] == 9) {
  echo "selected";
}
?> value="9">Mỗi 9 tháng</option>
<option <?php if ($data[0]['recurring'] == 10) {
  echo "selected";
}
?> value="10">Mỗi 10 tháng</option>
<option <?php if ($data[0]['recurring'] == 11) {
  echo "selected";
}
?> value="11">Mỗi 11 tháng</option>
<option <?php if ($data[0]['recurring'] == 12) {
  echo "selected";
}
?> value="12">Mỗi 12 tháng</option>
                                </select>
      </div>
    </div>
        <button style="float: right;margin-top: 60px;" class="btn-info btn">Lưu</button>
  </form>
     
                              </div>
                  </div>
               </div>
             
            </div>
         </div>
      </div>
</div>
</div>
<?php init_tail(); ?>
<script src="../../assets/js/number.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

<script >
  function set_num(){
  var a = $("#gia_tri").val();
  var number = numeral(Math.abs(a));
var string = number.format('0,0');
   $("#gia_tri").val(string);
  }
  function set_num1(){
    var ba = $("#tien_coc").val();
  var number1 = numeral(Math.abs(ba));
var string1 = number1.format('0,0');
   $("#tien_coc").val(string1);
  }
  function group_in(){
   var group = $('select[id="group"]').val();

   requestGetJSON("contracts/group_in/"+group).done(function (re) {
        var ten = re['invoi'];
         var dem = ten.length;
         console.log(dem);
         option=[];
         option =['"<option value="">Chọn văn phòng</option>"'];
     for (var i = 0; i < dem; i++) {
          op = ten[i]['description'];
          option += "<option value="+ten[i]['id']+">"+ten[i]['description']+"</option>";
         $('#name_building').html(option);
        
     }
       
     
    
   });


}
var ac = "<?php echo $data[0]['name_building']; ?>";
requestGetJSON("invoice_items/get_item_by_id/"+ac).done(function (re) {
  console.log(re);
  option = "<option  value="+re.itemid+">"+re.description+"</option>";
         $('#name_building').html(option);
});

jQuery(document).trigger("enhance");
</script>


</body>
</html>
