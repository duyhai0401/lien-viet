<?php init_head(); ?>
 <?php
$CI = &get_instance();
    $userid=$CI->session->userdata('staff_user_id');
    $user=$this->db->query("select * FROM `tblstaff` WHERE staffid=".$userid)->row_array();
if($user['is_not_staff']==1){
      ?>
<script type="text/javascript">
//<![CDATA[
function Pager(tableName, itemsPerPage) {
    this.tableName = tableName;
    this.itemsPerPage = itemsPerPage;
    this.currentPage = 1;
    this.pages = 0;
    this.inited = false;
   
    this.showRecords = function(from, to) {      
        var rows = document.getElementById(tableName).rows;
        // i starts from 1 to skip table header row
        for (var i = 1; i < rows.length; i++) {
            if (i < from || i > to)
                rows[i].style.display = 'none';
            else
                rows[i].style.display = '';
        }
    }
   
    this.showPage = function(pageNumber) {
          if (! this.inited) {
                   alert("not inited");
                   return;
          }
        var oldPageAnchor = document.getElementById('pg'+this.currentPage);
        oldPageAnchor.className = 'pg-normal';
       
        this.currentPage = pageNumber;
        var newPageAnchor = document.getElementById('pg'+this.currentPage);
        newPageAnchor.className = 'pg-selected';
       
        var from = (pageNumber - 1) * itemsPerPage + 1;
        var to = from + itemsPerPage - 1;
        this.showRecords(from, to);
    }  
   
    this.prev = function() {
        if (this.currentPage > 1)
            this.showPage(this.currentPage - 1);
    }
   
    this.next = function() {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);
        }
    }                      
   
    this.init = function() {
        var rows = document.getElementById(tableName).rows;
        var records = (rows.length - 1);
        this.pages = Math.ceil(records / itemsPerPage);
        this.inited = true;
    }
    this.showPageNav = function(pagerName, positionId) {
          if (! this.inited) {
                   alert("not inited");
                   return;
          }
          var element = document.getElementById(positionId);
         
          var pagerHtml = '<li  id="DataTables_Table_0_previous" onclick="' + pagerName + '.prev();" class="pg-normal paginate_button previous "> <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">Lùi lại</a> </li>  ';
        for (var page = 1; page <= this.pages; page++)
            pagerHtml += '<li id="pg' + page + '" class="pg-normal paginate_button previous " onclick="' + pagerName + '.showPage(' + page + ');"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">' + page + '</a></li>  ';
        pagerHtml += '<li onclick="'+pagerName+'.next();" class="pg-normal paginate_button previous "> <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">Đi tới</a></li>';          
       
        element.innerHTML = pagerHtml;
    }
}
//]]>
</script>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if(has_permission('expenses','','create')){ ?>
                  <a href="<?php echo admin_url('expenses/chiphi'); ?>" class="btn btn-info">Thêm mới chi phí</a>
                  <style type="text/css">
                    #invoice-form{
                      padding-right: 130px;
                      position: relative;
                      margin-top: 30px;
                    }
                    #invoice-form .input-content{
                      margin: 0 -15px;
                    }
                    #invoice-form .input-content:before{
                      content: '';
                      display: table;
                    }
                    #invoice-form .input-content:after{
                      content: '';
                      display: table;
                      clear: both;
                    }
                    .item-input{
                      width: 33.33%;
                      float: left;
                      padding:  0 15px;
                      height: 61px;
                    }
                    .item-input .dropdown.bootstrap-select{
                      width: 100% !important;
                    }
                    #invoice-form .btn-info.btn{
                      width: 100px;
                      position: absolute;
                      top: 25px;
                      right: 0;
                      height: 34px;
                      text-align: center;
                    }
                    @media only screen and (max-width:767px){
                       #invoice-form{
                        max-width: 500px;
                        margin-left: auto;
                        margin-right: auto;
                        padding-right: 0;
                       }
                      .item-input{
                        width: 100%;
                        float: left;
                        padding:  0 15px;
                        height: 61px;
                      }
                      #invoice-form .btn-info.btn{
                        width: 100%;
                        position: relative;
                        top: 0px;
                        right: 0;
                        height: 34px;
                      }
                    }
                  </style>
                  <?php } ?>
                 <!--  <?php $this->load->view('admin/expenses/filter_by_template'); ?>
                  <a href="#" onclick="slideToggle('#stats-top'); return false;" class="pull-right btn btn-default mleft5 btn-with-tooltip" data-toggle="tooltip" title="<?php echo _l('view_stats_tooltip'); ?>"><i class="fa fa-bar-chart"></i></a>
                  <a href="#" class="btn btn-default pull-right btn-with-tooltip toggle-small-view hidden-xs" onclick="toggle_small_view('.table-expenses','#expense'); return false;" data-toggle="tooltip" title="<?php echo _l('invoices_toggle_table_tooltip'); ?>"><i class="fa fa-angle-double-left"></i></a> -->
                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="expenses_total"></div>
                  </div>
                  <form action="" id="invoice-form" class="_transaction_form invoice-form" method="get" accept-charset="utf-8" novalidate="novalidate">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                  <div class="input-content">
                    <div class="item-input" style="margin-bottom: 20px;">
                     <label>Chọn Tòa Nhà</label>
                  <select id="name_building" name="name_building"  class="selectpicker _select_input_group" data-width="100%" data-none-selected-text="Không có mục nào được chọn" data-live-search="true" tabindex="-98">
                        <option value=""></option>
                         <?php
                    foreach ($items_groups as $key => $value) {
                      if ($value['id'] == $_GET['name_building']) {
                        echo '<option selected value="' . $value['id'] . '">' . $value['name'] . '</option>';
                      } else {
                        echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                      }

                    }

                    ?>
                      </select>
                    </div>
                    <div class="item-input" style="margin-bottom: 20px;">
                  <label>Chọn Năm </label>
                   <?php
                    $year=date('Y');
                    ?>
                      <select class="selectpicker" name="nam">
                       <?php
                       for($i=2019;$i<=2030;$i++){
                       ?>
                       <option id="<?php echo $i; ?>" <?php if($_GET['nam']== $i || $i==$year){ echo "selected";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                       <?php
                     }
                       ?>
                        
                      </select>

                    </div>
                    <div class="item-input" style="margin-bottom: 20px;">
                    <label>Chọn Tháng</label>
                    <?php
                    $month=date('m',strtotime(date('d-m-Y')." -1 months"));
                    
                    ?>
                      <select class="selectpicker" name="thang">
                       <?php
                       for($i=1;$i<=12;$i++){
                       ?>
                       <option id="<?php echo $i; ?>"  
                        <?php
                          if(isset($_GET['thang'])){
                            if($_GET['thang']== $i ){ echo "selected";}
                          }else{
                            if($i==$month){
                               echo "selected";
                            }
                          }
                        ?>

                       value="<?php echo $i; ?>"><?php echo $i; ?></option>
                       <?php
                     }
                       ?>
                        
                        
                      </select>
                    </div>
                  </div>
                  
                    <button class="btn-info btn">Xem</button>
                  </form>
               </div>
            </div>
            <div class="row">

               <div class="col-md-12" id="small-table">
                  <div class="panel_s">
                     <div class="panel-body">
                        <div class="clearfix"></div>
                        <!-- if expenseid found in url -->
                        <?php echo form_hidden('expenseid',$expenseid); ?>
                         <?php
$conn = mysqli_connect("localhost", "root","RedbeeX0605@","lienviet");
mysqli_set_charset($conn,"utf8");
$nam = $_GET['nam'];
$thang = $_GET['thang'];
if($thang==1){
$nam = $_GET['nam'];
$thang = $_GET['thang'];
  $thang_truoc = 12;
  $nam_truoc = $nam-1;
}else{
  $thang_truoc = $thang -1;
  $nam_truoc = $nam;
}
$date = date('m');
$date_d = date('d');
$date_y = date('Y');

if($date_d>05){
if($date>=$_GET['thang']){

if($date_y>=$_GET['nam']){
  $bien = "readonly";
}
}
if($date<$_GET['thang']){
  if($date_y>$_GET['nam']){
    $bien = "readonly";
  }
}
}
$name_building = $_GET['name_building'];
$sql_name = "SELECT * FROM `tblitems` where `group_id`=$name_building";

$data_name = mysqli_query($conn,$sql_name);

      ?>
     
<form action="" method="post">
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">

<input type="text" class="hidden" name="nam" value="<?php echo $_GET['nam']?>">
<input type="text" class="hidden" name="thang" value="<?php echo $_GET['thang']?>">
<style type="text/css">
  .table-content{
    overflow: auto;
  }
  #expenses-report-table.table>thead>tr>th{
    white-space: nowrap;
  }

</style>
<div class="table-content">
  <table class="table table-bordered table-hover expenses-report" id="expenses-report-table">
      <thead>
          <tr>
            <th  class="bold">Tên Tòa nhà</th>
            <th  class="bold">Tên dịch vụ</th>
            <th  class="bold"> Tháng trước</th>   
            <th  class="bold"> Tháng này</th>   
          </tr>
            </thead>
            <tbody>
          
          <?php
          while ($data1_name = mysqli_fetch_assoc($data_name)) {
           $id= $data1_name['id'];
           $sql = "SELECT * FROM `tblmoney_sv` where 1 and `nam`='$nam' and `thang`='$thang' and `id_name_building`='$id' order by id desc limit 1";
 $sql_truoc = "SELECT * FROM `tblmoney_sv` where 1 and `nam`='$nam_truoc' and `thang`='$thang_truoc' and `id_name_building`='$id' order by id desc limit 1";
      $data = mysqli_query($conn,$sql);
$data_truoc = mysqli_query($conn,$sql_truoc);
$data1_truoc = mysqli_fetch_assoc($data_truoc);
$data1 = mysqli_fetch_assoc($data);  


          ?>                             
            <tr class="bg-odd">
             
              <td rowspan="2" class="bold text-info"><?php echo $data1_name['description']?></td>
              <td  class="bold text-info">Điện  <input type="text" class="hidden" name="id_name_building[]" value="<?php echo $data1_name['id']?>"></td>
              <td class="bold"><input type="number" class="form-control" readonly value="<?php echo $data1_truoc['dien']?>" ></td>
              <td class="bold"><input type="number"  class="form-control" name="dien[]" value="<?php echo $data1['dien']?>" ></td>
            
            </tr>
             <tr class="bg-odd">
               <td  class="bold text-info">Nước</td>
             <td class="bold"><input type="number"  class="form-control" readonly value="<?php echo $data1_truoc['nuoc']?>" ></td>
              <td class="bold"><input type="number"  class="form-control" name="nuoc[]" value="<?php echo $data1['nuoc']?>" ></td>  
            </tr>
           
<?php } ?>
            </tbody>
        </table>
   </div>
      
        <button style="float: right;" class="btn-info btn">Lưu</button>
        </form>   
         <ul class="pagination" id="pageNavPosition">
            
                  </ul>
                              </div>
                  </div>
               </div>
               <div class="col-md-7 small-table-right-col">
                  <div id="expense" class="hide">
                  </div>
               </div>
            </div>
         </div>
      </div>
</div>
</div>


<!-- /.modal -->
<style type="text/css">
  .pg-normal {
       color: red;
       font-weight: normal;
       text-decoration: none;  
       cursor: pointer;  
}

.pg-selected {
       color: black;
       font-weight: bold;     
       text-decoration: underline;
       cursor: pointer;
}
</style>
<script type="text/javascript"><!--
       var pager = new Pager('expenses-report-table', 15);
       pager.init();
       pager.showPageNav('pager', 'pageNavPosition');
       pager.showPage(1);
//--></script>
<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail(); ?>
<script>
   Dropzone.autoDiscover = false;
   $(function(){
             // Expenses additional server params
             var Expenses_ServerParams = {};
             $.each($('._hidden_inputs._filters input'),function(){
               Expenses_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
             });
             initDataTable('.table-expenses', admin_url+'expenses/table', 'undefined', 'undefined', Expenses_ServerParams, <?php echo do_action('expenses_table_default_order',json_encode(array(5,'desc'))); ?>).column(0).visible(false, false).columns.adjust();

             init_expense();

             $('#expense_convert_helper_modal').on('show.bs.modal',function(){
                var emptyNote = $('#tab_expense').attr('data-empty-note');
                var emptyName = $('#tab_expense').attr('data-empty-name');
                if(emptyNote == '1' && emptyName == '1') {
                    $('#inc_field_wrapper').addClass('hide');
                } else {
                    $('#inc_field_wrapper').removeClass('hide');
                    emptyNote === '1' && $('.inc_note').addClass('hide') || $('.inc_note').removeClass('hide')
                    emptyName === '1' && $('.inc_name').addClass('hide') || $('.inc_name').removeClass('hide')
                }
             });

             $('body').on('click','#expense_confirm_convert',function(){
              var parameters = new Array();
              if($('input[name="expense_convert_invoice_type"]:checked').val() == 'save_as_draft_true'){
                parameters['save_as_draft'] = 'true';
              }
              parameters['include_name'] = $('#inc_name').prop('checked');
              parameters['include_note'] = $('#inc_note').prop('checked');
              window.location.href = buildUrl(admin_url+'expenses/convert_to_invoice/'+$('body').find('.expense_convert_btn').attr('data-id'), parameters);
            });
           });
</script>
</body>
</html>
<?php
}
?>
