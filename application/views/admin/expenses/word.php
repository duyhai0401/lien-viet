
    <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>HĐ_Sàn tầng 3 xuân thủy_Vũ Thị Trang_NVKD Diện</title>
   
</head>
<style type="text/css">
    .hidden{
        display: none;
    }
    #button{
        color: #fff;
    text-transform: uppercase;
    font-size: 13.5px;
    outline-offset: 0;
    border: 1px solid transparent;
    background-color: #03a9f4;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

    }
	.hd-container{width:1000px;margin:auto;padding:0 15px}.hd-row:after,.hd-row:before{content:"";display:table}.hd-row:after{clear:both}.hd-row .hd-logo{width:155px;float:left}.hd-row .hd-logo img{width:100%}.hd-row .hd-chx{text-align:center;float:left;width:838px}.hd-row .hd-chx .hd-ch{font-weight:400}.hd-row .hd-chx .hd-dl{font-weight:700;text-decoration:underline}.hd-row .hd-chx .hd-hn{text-align:right;padding-right:160px}.hd-row .hd-hdctvp,.hd-row .hd-hdtp{text-align:center}.hd-row .hd-hdctvp{margin:0}.hd-row .hd-hdtp{font-weight:700;font-size:14px}.hd-row .hd-styleText{font-style:italic;text-indent:30px}.hd-row .hd-flex{display:-webkit-box;display:-ms-flexbox;display:flex}.hd-row .text-right{text-align:right}.hd-row .text-left{text-align:left;padding-left:107px}
    .btn{
            color: #fff;
    background-color: #026e9e;
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    }
    .btn-defaut{

    }
</style>
<body class="index">
    <main id="print">
        <div class="hd-container" contenteditable="true">
            <div class="hd-row">
                <div>
                </div>
                <div class="hd-logo">
                    <img src="../../../assets/images/logo.gif" alt="">
                </div>
                <div class="hd-chx">
                    <h2 class="hd-ch">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h2>
                    <p class="hd-dl">Độc lập – Tự do – Hạnh phúc</p>
                    <p class="hd-hn">Hà Nội, ngày ........tháng........năm.....</p>
                </div>
            </div>
            <button id="button" onclick="myFunction()">In hợp đồng</button>
            <div class="hd-row">
                <h2 class="hd-hdctvp">
                    HỢP ĐỒNG CHO THUÊ VĂN PHÒNG
                </h2>
             
                <p class="hd-hdtp">(Số: 120119/HĐTP-LVHG)</p>
                <p style="margin-left: 20%" class="hd-styleText">Hôm nay, ngày ….tháng .....năm ........., tại địa chỉ: Số 139, đường Xuân Thủy,<br /> tổ 19, phường Dịch Vọng Hậu, quận Cầu Giấy, thành phố Hà Nội.
                </p>
                <p>
                    Chúng tôi gồm:
                    <b>BÊN CHO THUÊ VĂN PHÒNG (BÊN A):</b><br>
                    <b>CÔNG TY CỔ PHẦN QUẢN LÝ BẤT ĐỘNG SẢN LIÊN VIỆT HOÀNG GIA</b>
                </p>
                <p>
                    - Địa chỉ : Số 05, ngách 52, ngõ 255, đường Nguyễn Khang, phường Yên Hòa, quận Cầu Giấy, thành phố Hà Nội.
                </p>
                <p>
                    - Mã số thuế : 0107483053 Ngày cấp: 23/06/2016 Nơi cấp: Sở KH đầu tư Hà Nội.</p>
                <p> - Điện thoại : 02432272549 – CSKH Hotline : 0898589669</p>
                <p> - Số tài khoản : 12610000899596 - Tên TK : Công ty cổ phần quản lý BĐS Liên Việt Hoàng Gia </p>
                <p>- Mở tại ngân hàng BIDV chi nhánh Ba Đình.</p>
                <p>- Đại diện là (Ông/bà): <b>Trần Văn Nam</b> - Chức vụ: Tổng Giám đốc</p>
                <p>- Số căn cước công dân: 035082001012</p>
                <p>- Nơi cấp: Cục CS ĐKQL cư trú và DLQG về Dân cư - Ngày cấp: 07/06/2016</p>
                <p>- Số Tài khoản : - BIDV: 12510000633317 - Ngân hàng BIDV chi nhánh Đông Đô, Hà Nội– Chủ TK : Trần Văn Nam;</p>
                <p>
                    - Vietcom bank: 0021000447462 - Ngân hàng Vietcombank chi nhánh Hà Nội – Chủ TK : Trần Văn Nam.
                </p>
            </div>
          <div class="hd-row">
                <b>BÊN THUÊ VĂN PHÒNG (BÊN B):</b>
                <div class="hd-flex">
                    <p style="padding-right: 62px;">- Đại diện là (Ông/Bà): <b> <?php echo $data[0]['company'] ?></b></p>
                    <p> Điện thoại: <b><?php echo $data[0]['phonenumber'] ?></b></p>
                </div>
                <div class="hd-flex">
                    <p style="padding-right: 62px;">
                        - Số Mã số thuế/CMND: <b><?php echo $data[0]['vat'] ?></b>
                    </p>
                    <p style="padding-right: 62px;">Cấp ngày: <b></b></p>
                    <p> Tại: <?php echo $data[0]['city'] ?></p>
                </div>
                <p>
                    - Địa chỉ: <b><?php echo $client[0]['address'] ?>.</b>
                </p>
                <p class="hd-styleText">
                    <b>Sau khi bàn bạc hai bên thống nhất đi đến ký hợp đồng thuê văn phòng với nội dung sau:</b>
                </p>
            </div>
            <div class="hd-row">
                <p><b> ĐIỀU 1: ĐỐI TƯỢNG HỢP ĐỒNG CHO THUÊ</b></p>
                <p>.Tài sản sử dụng riêng:</p>
                <p><b> - <?php echo $data[0]['description'] ?>, tại địa chỉ: <?php echo $data[0]['long_description'] ?>.</b></p>
                <p> - Diện tích: ....</p>
                <p>- Thiết bị trong phòng gồm có(tài sản sử dụng riêng):</p>
                <p> + 06 bóng đèn chùm;</p>
                <p>+ 02 điều hòa;</p>
                <p>+ 01 chìa khóa phòng;</p>
                <p>- Thiết bị trong nhà vệ sinh sử dụng (riêng/chung): Hệ thống đèn chiếu sáng (bóng đèn), hệ thống vòi xịt , bồn rửa mặt , bệt vệ sinh và các thiết bị vệ sinh cần thiết….</p>
                <p>1.2. Tài sản sử dụng và bảo quản chung: Thang bộ, thang máy, hệ thống chiếu sáng công cộng, hệ thống thoát nước.</p>
                <p>
                    1.3. Tất cả các tài sản sử dụng riêng khi bên A bàn giao cho bên B đều hoạt động bình thường , trong quá trình sử dụng nếu có hỏng hóc hoặc gặp sự cố, bên B phải hoàn toàn chịu trách nhiệm sửa chữa và thay thế.
                </p>
                <p>1.4. Tất cả các tài sản chung đều bình thường bên B có trách nhiệm bảo quản cùng ban quản lý, nếu hỏng hóc do lỗi của bên B thì bên B phải bỏ tiền sửa chữa, thay thế hoặc bồi thường cho bên A.</p>
                <p>
                    <b>ĐIỀU 2: MỤC ĐÍCH SỬ DỤNG.</b>
                </p>
                <p>2.1. Căn phòng trên bên B sử dụng vào mục đích: Làm văn phòng</p>
                <p>2.2. Số lượng người làm việc: ........ người.</p>
                <p>2.3 Nếu bên B muốn chuyển nhượng Hợp đồng cho người khác phải thông báo cho Bên A và được sự đồng ý của bên A.</p>
                <p><b> ĐIỀU 3: THỜI ĐIỂM GIAO NHẬN PHÒNG VÀ THỜI HẠN CHO THUÊ</b></p>
                <p>3.1. Thời điểm giao nhận phòng: Ngày  <?php
                $date=date_create($data[0]['date_bd']);
                echo date_format($date,"d-m-Y");
                ?></p>
                
                <p>3.3. Thời hạn hợp đồng là: tính từ ngày  <?php
                $date=date_create($data[0]['datestart']);
                echo date_format($date,"d-m-Y");
                ?> đến ngày  <?php
                $date=date_create($data[0]['dateend']);
                echo date_format($date,"d-m-Y");
                ?>.</p>
                <p><b>ĐIỀU 4: GIÁ PHÒNG CHO THUÊ VÀ PHƯƠNG THỨC THANH TOÁN</b></p>
                <p>4.1. Hai bên thỏa thuận như sau:</p>
                <p>- Tiền thuê: <?php echo number_format($data[0]['contract_value'],0) ?> VNĐ/tháng (Bằng chữ: ....)</p>
                <p>- Giá này sẽ được giữ nguyên trong 01(năm). Và được điều chỉnh từ năm thứ 02 với mức tăng không vượt quá 10% giá thuê.</p>
                <p> - Chi phí khác:</p>
                <?php
                foreach ($item as $value) {
               
                    echo '<p>+ '.$value['description'].', giá tính theo giá đã thỏa thuận là: '.number_format($value['rate']).' VNĐ.</p>';
                
                }
                ?>
                <!-- <p>+ Tiền điện, mỗi phòng một đồng hồ riêng, giá tính theo giá đã thỏa thuận là: 4000VNĐ/KW.</p>
                <p> + Tiền nước, Internet: Miễn phí</p> -->
                <p>(Lưu ý: Tất cả giá trên chưa bao gồm các khoản thuế VAT 10% và các khoản thuế, phí khác).</p>
                <p>4.2. Tiền đặt cọc duy trì hợp đồng là: <?php echo number_format($data[0]['deposit'],0) ?> VNĐ (đã kèm theo 01 hợp đồng đặt cọc).
                    Số tiền đặt cọc này không được tính lãi trong suốt quá trình thuê và sẽ được bên A trả lại hoàn toàn cho bên B khi hợp đồng kết thúc đúng hạn và không có phát sinh nào xảy ra. <b>Trước khi thời hạn hợp đồng kết thúc, bên B phải có trách nhiệm thông báo trước cho bên A ít nhất 30 ngày về việc bên B kết thúc hợp đồng hoặc tiếp tục gia hạn hợp đồng.</b>
                </p>
                <p>Nếu bên B vi phạm các nội quy Hợp đồng hoặc đơn phương chấm dứt hợp đồng trước thời hạn thì phải chịu mất hoàn toàn số tiền đã đặt cọc mà không có quyền khiếu nại gì.</p>
                <p>
                    4.3. Phương thức thanh toán:
                </p>
                <p>
                    Bên B thanh toán cho bên A tiền thuê phòng theo phương thức 03 tháng/lần. Kỳ đầu tiên sẽ được thanh toán ngay sau khi kí hợp đồng này. Những kỳ thanh toán tiếp theo sẽ được thu vào ngày thứ nhất đến ngày thứ 3 của kỳ thanh toán. Tiền dịch vụ trả vào ngày 05 của tháng kế tiếp.
                </p>
                <p> Nếu bên B thanh toán chậm tiền nhà và các khoản phí dịch vụ cho Bên A vì bất kỳ lý do gì thì bên A có quyền đơn phương chấm dứt Hợp đồng mà không phải đền bù cho Bên B bất cứ khoản chi phí nào.</p>
                <p><b>ĐIỀU 5: QUYỀN VÀ NGHĨA VỤ BÊN A</b></p>
                <p>5.1. Quyền của bên A:</p>
                <p>a) Yêu cầu bên B trả đủ số tiền thuê nhà và tiền dịch vụ (nếu có) đúng thời hạn ghi trong Hợp đồng.</p>
                <p> b) Yêu cầu bên B có trách nhiệm trong việc sửa chữa phần hư hỏng các thiết bị và nội thất trong diện tích sử dụng.</p>
                <p>
                    c) Đơn phương chấm dứt thực hiện Hợp đồng thuê phòng khi bên B có một trong các hành vi vi phạm Hợp đồng, nội quy tòa nhà, thông báo, hướng dẫn của ban quản lý mà không phải trả lại tiền đặt cọc cho bên B.
                </p>
                <p> 5.2. Nghĩa vụ của bên A:</p>
                <p>a) Giao phòng ở và trang thiết bị gắn liền với mặt bằng (nếu có) cho bên B đúng ngày quy định trong Hợp đồng.</p>
                <p> b) Bảo đảm quyền sử dụng trọn vẹn phần sử dụng riêng của bên B.</p>
                <p> c) Nếu bên A đơn phương chấm dứt hợp đồng trước thời hạn, bên A sẽ trả lại tiền đặt cọc cho bên B và khoản tiền còn lại mà bên B thanh toán chưa sử dụng hết. Đồng thời đền bù thêm 01 tháng tương đương số tiền mà bên B đã đặt cọc .</p>
                <p><b>ĐIỀU 6: QUYỀN VÀ NGHĨA VỤ CỦA BÊN B</b></p>
                <p>6.1. Quyền của bên B:</p>
                <p>a) Nhận phòng ở và trang thiết bị đúng ngày quy định của Hợp đồng này.</p>
                <p> b) Có quyền đơn phương chấm dứt Hợp đồng thuê phòng khi bên A có một trong các hành vi vi phạm quy định của Hợp đồng, bên A sẽ trả lại tiền đặt cọc và trả lại khoản tiền còn lại mà bên B thanh toán chưa sử dụng hết . Đồng thời đền bù thêm 01 tháng tương đương số tiền mà bên B đã đặt cọc .</p>
                <p>6.2. Nghĩa vụ của bên B:</p>
                <p>a) Trả đủ tiền thuê nhà đúng thời hạn theo quy định trong Hợp đồng.</p>
                <p>b) Sử dụng nhà đúng theo mục đích, giữ gìn và có trách nhiệm trong việc sửa chữa hoặc bồi thường hư hỏng do lỗi mình gây ra.</p>
                <p> c) Chấp hành đầy đủ những quy định nội quy và thông báo của đơn vị quản lý nhà ở.</p>
                <p> d) Không tổ chức đánh bạc, tụ tập rượu chè, tàng trữ và sử dụng ma túy, các chất cháy nổ, độc hại và các hành vi vi phạm pháp luật khác trong tòa nhà.</p>
                <p> e) Phòng chống cháy nổ theo quy định của Nhà nước và tòa nhà.
                    Bên B không được phép thắp hương trong tòa nhà , sử dụng an toàn các thiết bị sinh lửa ,dễ gây cháy nổ . Nếu để xảy ra cháy nổ, gây thiệt hại về người và tài sản, bên B phải hoàn toàn chịu trách nhiệm đền bù cho bên A và chịu trách nhiệm trước pháp luật .</p>
                <p>
                    f) Trong mọi trường hợp Bên B bỏ cọc, thanh lý hợp đồng trước thời hạn, Bên B sẽ mất toàn bộ số tiền đã đặt cọc cho Bên A và không được lấy lại số tiền đã thanh toán mà chưa sử dụng hết.
                </p>
                <p><b>ĐIỀU 7: CAM KẾT CÁC BÊN</b></p>
                <p>7.1. Hai bên cam kết thực hiện đúng các nội dung đã ký.</p>
                <p>7.2. Trong quá trình thực hiện hợp đồng, nếu có phát sinh hai bên thông báo cho nhau và giải quyết trên tinh thần hỗ trợ lẫn nhau để đi đến thống nhất chung. Nếu các bên không thống nhất được thì tòa án là nơi giải quyết những tranh chấp của 2 bên, bên thua kiện sẽ phải chịu chi phí cho tòa.</p>
                <p>
                    7.3. Hợp đồng được lập thành 03 bản và có giá trị như nhau. Bên A giữ 02 bản và Bên B giữ 01 bản. Hợp đồng này có giá trị kể từ ngày hai bên ký kết./.
                </p>
                <p class="text-right">Hà Nội, ngày … tháng ….. năm …….</p>
            </div>
            <div class="hd-row">
                <div class="hd-logo">
                    <img src="../../../assets/images/logo.gif" alt="">
                </div>
                <div class="hd-chx">
                    <h2 class="hd-ch">CÔNG TY CỔ PHẦN QUẢN LÝ BẤT ĐỘNG SẢN<br />
                        LIÊN VIỆT HOÀNG GIA<br>
                        ---o0o--
                    </h2>
                    <p class="text-left">Địa chỉ: Số 05, Ngách 52, Ngõ 255 , Đường Nguyễn Khang,<br>
                        Phường Yên Hòa, Quận Cầu Giấy, Thành phố Hà Nội.<br>
                        Hotline: 0243.2272549 - Email: lienviethoanggia@gmail.com<br>
                    </p>
                </div>
            </div>
            <div class="hd-row">
                <h2 class="hd-hdctvp">
                    NỘI QUY TÒA NHÀ
                </h2>
            </div>
            <div class="hd-row">
                <p>
                    <b>Điều 1:</b> Cán bộ công nhân viên và khách hàng có trách nhiệm giữ gìn vệ sinh chung, không gây tiếng ổn làm ảnh hưởng chung đến tòa nhà.
                </p>
                <p>
                    <b>Điều 2:</b> Việc phòng cháy chữa cháy là trách nhiệm của tất cả mọi người trong tòa nhà kể cả khách hàng.
                </p>
                <p>
                    <b> Điều 3:</b> Không buôn bán, tang trữ sử dụng trái phép chất ma túy và hàng hóa mà pháp luật cấm tại phòng và các khu vực khác trong nhà.
                </p>
                <p>
                    <b>Điều 4:</b> Không đánh bạc, hoạt động mại dâm và các hoạt động trái với quy định của pháp luật trong phạm vi tòa nhà.
                </p>
                <p>
                    <b>Điều 5:</b> Không được làm hư hỏng hoặc có hành vi vi phạm đến tài sản chung của tòa nhà. Không được chiếm dụng, sử dụng phần diện tích thuộc sở hữu chung của tòa nhà.
                </p>
                <p>
                    <b>Điều 6:</b> Tuân thủ các quy định về việc dừng, đỗ xe tại nơi được dừng, đỗ xe theo quy định.
                </p>
                <p>
                    <b>Điều 7: </b>Vận chuyển hàng hóa và các thiết bị văn phòng ra hoặc vào phải thông báo với bảo vệ và ban quản lý tòa nhà của công ty.
                </p>
                <p>
                    <b>Điều 8:</b> Không được tự ý sửa chữa các hư hỏng, làm thay đổi kết cấu tòa nhà.
                </p>
                <p>
                    <b>Điều 9: </b>Tự bảo quản đồ dùng riêng và chung trong tòa nhà, đọc kỹ hướng dẫn sử dụng các thiết bị của tòa nhà. Sử dụng thang máy và các thiết bị sử dụng chung theo đúng mục đích, công năng thiết kế sử dụng.
                </p>
                <p>
                    <b>Điều 10:</b> Thời gian làm việc trong giờ hành chính, không được ở lại qua đêm, không làm việc quá khung giờ cho phép của An ninh khu vực và không gây mất trật tư, an ninh tại tòa nhà trong quá trình làm việc.
                </p>
                <p> Nếu khách hàng làm việc ngoài khung giờ 7h00 – 17h30 hàng ngày, Quý khách hàng phải chủ động bảo quản tài sản cá nhân cũng như tài sản chung của tòa nhà:</p>
                <p>
                    - Nếu xảy ra hỏng hóc hay mất mát tài sản riêng của khách hàng thì khách hàng hoàn toàn tự chịu trách nhiệm.
                </p>
                <p>
                    - Nếu để hỏng hóc, mất mát tài sản chung của tòa nhà thì khách hàng hoàn toàn chịu trách nhiệm bồi thường thiệt hại cho Công ty theo quy định.
                </p>
                <p>
                    - Trường hợp khách hàng cần Bảo vệ tòa nhà hỗ trợ ngoài khung giờ hành chính thì khách hàng thỏa thuận trực tiếp với Bảo vệ, mọi tổn thất về tài sản cá nhân Bảo vệ và khách hàng hoàn toàn chịu trách nhiệm.
                </p>
                <p><b>Điều 11:</b> Xử lý các hành vi vi phạm.
                    Khách hàng thuê văn phòng và khách ra vào tòa nhà nếu có hành vi vi phạm các quy định của Bản nội quy này hoặc vi phạm quy định của quy chế quản lý, sử dụng nhà do Bộ Xây dựng ban hành thì tùy theo mức độ vi phạm sẽ bị xem xét, xử lý theo quy định của pháp luật và phải bồi thường thiệt hại do hành vi vi phạm của mình gây ra.</p>
            </div>
        </div>
    </main>
</body>
<script>
function myFunction() {
    var element = document.getElementById("button");
  element.classList.add("hidden");
  window.print();
element.classList.remove("hidden");
}
</script>
</html>