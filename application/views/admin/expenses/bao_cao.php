<?php init_head(); ?>
<script type="text/javascript">
//<![CDATA[
function Pager(tableName, itemsPerPage) {
    this.tableName = tableName;
    this.itemsPerPage = itemsPerPage;
    this.currentPage = 1;
    this.pages = 0;
    this.inited = false;
   
    this.showRecords = function(from, to) {      
        var rows = document.getElementById(tableName).rows;
        // i starts from 1 to skip table header row
        for (var i = 1; i < rows.length; i++) {
            if (i < from || i > to)
                rows[i].style.display = 'none';
            else
                rows[i].style.display = '';
        }
    }
   
    this.showPage = function(pageNumber) {
          if (! this.inited) {
                   alert("not inited");
                   return;
          }
        var oldPageAnchor = document.getElementById('pg'+this.currentPage);
        oldPageAnchor.className = 'pg-normal';
       
        this.currentPage = pageNumber;
        var newPageAnchor = document.getElementById('pg'+this.currentPage);
        newPageAnchor.className = 'pg-selected';
       
        var from = (pageNumber - 1) * itemsPerPage + 1;
        var to = from + itemsPerPage - 1;
        this.showRecords(from, to);
    }  
   
    this.prev = function() {
        if (this.currentPage > 1)
            this.showPage(this.currentPage - 1);
    }
   
    this.next = function() {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);
        }
    }                      
   
    this.init = function() {
        var rows = document.getElementById(tableName).rows;
        var records = (rows.length - 1);
        this.pages = Math.ceil(records / itemsPerPage);
        this.inited = true;
    }
    this.showPageNav = function(pagerName, positionId) {
          if (! this.inited) {
                   alert("not inited");
                   return;
          }
          var element = document.getElementById(positionId);
         
          var pagerHtml = '<li  id="DataTables_Table_0_previous" onclick="' + pagerName + '.prev();" class="pg-normal paginate_button previous "> <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">Lùi lại</a> </li>  ';
        for (var page = 1; page <= this.pages; page++)
            pagerHtml += '<li id="pg' + page + '" class="pg-normal paginate_button previous " onclick="' + pagerName + '.showPage(' + page + ');"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">' + page + '</a></li>  ';
        pagerHtml += '<li onclick="'+pagerName+'.next();" class="pg-normal paginate_button previous "> <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">Đi tới</a></li>';          
       
        element.innerHTML = pagerHtml;
    }
}
//]]>
</script>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if(has_permission('expenses','','create')){ ?>
                  <a href="<?php echo admin_url('expenses/chiphi'); ?>" class="btn btn-info">Thêm mới chi phí</a>
                  <?php } ?>
                 <!--  <?php $this->load->view('admin/expenses/filter_by_template'); ?>
                  <a href="#" onclick="slideToggle('#stats-top'); return false;" class="pull-right btn btn-default mleft5 btn-with-tooltip" data-toggle="tooltip" title="<?php echo _l('view_stats_tooltip'); ?>"><i class="fa fa-bar-chart"></i></a>
                  <a href="#" class="btn btn-default pull-right btn-with-tooltip toggle-small-view hidden-xs" onclick="toggle_small_view('.table-expenses','#expense'); return false;" data-toggle="tooltip" title="<?php echo _l('invoices_toggle_table_tooltip'); ?>"><i class="fa fa-angle-double-left"></i></a> -->
                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="expenses_total"></div>
                  </div>
                  <form style="float: right;" action="https://demo.nobitech.vn/admin/expenses/bao_cao" id="invoice-form" class="_transaction_form invoice-form" method="get" accept-charset="utf-8" novalidate="novalidate">
                  <input type="hidden" name="csrf_token_name" value="20e0a3sa994ea7dd461520513c5608cc" >
                  <label>Chọn năm </label>
                    <div style="margin-bottom: 20px;">
                      <select class="selectpicker" name="nam">
                       
                        <option id="2019" <?php if($_GET['nam']=='2019'){ echo "selected";} ?> value="2019">2019</option>
                        <option id="2020" <?php if($_GET['nam']=='2020'){ echo "selected";} ?> value="2020">2020</option>
                        <option id="2021" <?php if($_GET['nam']=='2021'){ echo "selected";} ?> value="2021">2021</option>
                        <option id="2022" <?php if($_GET['nam']=='2022'){ echo "selected";} ?> value="2022">2022</option>
                        <option id="2023" <?php if($_GET['nam']=='2023'){ echo "selected";} ?> value="2023">2023</option>
                        <option id="2024" <?php if($_GET['nam']=='2024'){ echo "selected";} ?> value="2024">2024</option>
                        <option id="2025" <?php if($_GET['nam']=='2025'){ echo "selected";} ?> value="2025">2025</option>
                        <option id="2026" <?php if($_GET['nam']=='2026'){ echo "selected";} ?> value="2026">2026</option>
                        <option id="2028" <?php if($_GET['nam']=='2027'){ echo "selected";} ?> value="2028">2027</option>
                      </select>
                    </div>
                    <button style="float: right;" class="btn-info btn">Xem</button>
                  </form>
               </div>
            </div>
            <div class="row">

               <div class="col-md-12" id="small-table">
                  <div class="panel_s">
                     <div class="panel-body">
                        <div class="clearfix"></div>
                        <!-- if expenseid found in url -->
                        <?php echo form_hidden('expenseid',$expenseid); ?>
                         <?php
$conn = mysqli_connect("localhost", "laptrin1_demo_nobitech","demo@nobitech","laptrin1_demo_nobitech");
mysqli_set_charset($conn,"utf8");
$nam = $_GET['nam'];
 $sql = "SELECT * FROM `tblitems_groups` where 1";
$data = mysqli_query($conn,$sql);

      ?>
<p class="mbot20 text-info">Mục báo cáo chỉ được tạo từ chi phí sau thuế.</p>      
   
<table class="table table-bordered table-hover expenses-report" id="expenses-report-table">
      <thead>
          <tr>
            <th class="bold">Phân loại</th>
            <th class="bold">Tháng 1</th>  
            <th class="bold">Tháng 2</th>  
            <th class="bold">Tháng 3</th>  
            <th class="bold">Tháng 4</th>  
            <th class="bold">Tháng 5</th>  
            <th class="bold">Tháng 6</th>  
            <th class="bold">Tháng 7</th>  
            <th class="bold">Tháng 8</th>  
            <th class="bold">Tháng 9</th>  
            <th class="bold">Tháng 10</th>  
            <th class="bold">Tháng 11</th>  
            <th class="bold">Tháng 12</th>                                            
            <th class="bold"> Năm (2019)</th>
          </tr>
            </thead>
            <tbody>
            <?php  while ($data1 = mysqli_fetch_assoc($data)) { ?>
          <tr>
            <td class="bold"><?php echo $data1['name']?></td>
            <td><?php
 $sql_01 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."-01%'"; 
$data_01= mysqli_query($conn,$sql_01);        
while ($sum_01 = mysqli_fetch_assoc($data_01)) {
  if($sum_01['SUM(`total_money`)']){
    echo $sum_01['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>                                                
            <td><?php
 $sql_02 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."-02%'"; 
$data_02= mysqli_query($conn,$sql_02);        
while ($sum_02 = mysqli_fetch_assoc($data_02)) {
  if($sum_02['SUM(`total_money`)']){
    echo $sum_02['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>                                                
            <td><?php
 $sql_03 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."-03%'"; 
$data_03= mysqli_query($conn,$sql_03);        
while ($sum_03 = mysqli_fetch_assoc($data_03)) {
  if($sum_03['SUM(`total_money`)']){
    echo $sum_03['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>                                                
            <td><?php
 $sql_04 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."-04%'"; 
$data_04= mysqli_query($conn,$sql_04);        
while ($sum_04 = mysqli_fetch_assoc($data_04)) {
  if($sum_04['SUM(`total_money`)']){
    echo $sum_04['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>                                                
            <td><?php
 $sql_05 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."-05%'"; 
$data_05= mysqli_query($conn,$sql_05);        
while ($sum_05 = mysqli_fetch_assoc($data_05)) {
  if($sum_05['SUM(`total_money`)']){
    echo $sum_05['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>                                                
            <td><?php
 $sql_06 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."-06%'"; 
$data_06= mysqli_query($conn,$sql_06);        
while ($sum_06 = mysqli_fetch_assoc($data_06)) {
  if($sum_06['SUM(`total_money`)']){
    echo $sum_06['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ
              
            </td>                                                
            <td><?php
 $sql_07 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."-07%'"; 
$data_07= mysqli_query($conn,$sql_07);        
while ($sum_07 = mysqli_fetch_assoc($data_07)) {
  if($sum_07['SUM(`total_money`)']){
    echo $sum_07['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>                                                
            <td><?php
 $sql_08 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."-08%'"; 
$data_08= mysqli_query($conn,$sql_08);        
while ($sum_08 = mysqli_fetch_assoc($data_08)) {
  if($sum_08['SUM(`total_money`)']){
    echo $sum_08['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>                                                
            <td><?php
 $sql_09 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."-09%'"; 
$data_09= mysqli_query($conn,$sql_09);        
while ($sum_09 = mysqli_fetch_assoc($data_09)) {
  if($sum_09['SUM(`total_money`)']){
    echo $sum_09['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>                                                
            <td><?php
 $sql_10 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."-10%'"; 
$data_10= mysqli_query($conn,$sql_10);        
while ($sum_10 = mysqli_fetch_assoc($data_10)) {
  if($sum_10['SUM(`total_money`)']){
    echo $sum_10['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>                                                
            <td><?php
 $sql_11 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."-11%'"; 
$data_11= mysqli_query($conn,$sql_11);        
while ($sum_11 = mysqli_fetch_assoc($data_11)) {
  if($sum_11['SUM(`total_money`)']){
    echo $sum_11['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>                                                
            <td><?php
 $sql_12 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."-12%'"; 
$data_12= mysqli_query($conn,$sql_12);        
while ($sum_12 = mysqli_fetch_assoc($data_12)) {
  if($sum_12['SUM(`total_money`)']){
    echo $sum_12['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>                                                                                                
            <td class="bg-odd"><?php
 $sql_13 = "SELECT SUM(`total_money`) FROM `tblexpen` where `group_id`= ".$data1['id']." AND `date` like '%".$nam."%'"; 
$data_13= mysqli_query($conn,$sql_13);        
while ($sum_13 = mysqli_fetch_assoc($data_13)) {
  if($sum_13['SUM(`total_money`)']){
    echo $sum_13['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
                                           
            <tr class="bg-odd">
            <?php }?>
              <!-- <td class="bold text-info">Tổng thuế</td>
              <td class="bold">0.00đ</td><td class="bold">0.00đ</td><td class="bold">0.00đ</td><td class="bold">0.00đ</td><td class="bold">0.00đ</td><td class="bold">9,409,090.91đ</td><td class="bold">0.00đ</td><td class="bold">0.00đ</td><td class="bold">0.00đ</td><td class="bold">0.00đ</td><td class="bold">0.00đ</td><td class="bold">0.00đ</td><td class="bold bg-odd">9,409,090.91đ</td> -->                                                </tr>
            <tr class="bg-odd">
              <td class="bold text-info">Tổng cộng </td>
              <td class="bold"><?php
 $sql_01 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."-01%'"; 
$data_01= mysqli_query($conn,$sql_01);        
while ($sum_01 = mysqli_fetch_assoc($data_01)) {
  if($sum_01['SUM(`total_money`)']){
    echo $sum_01['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
              <td class="bold"><?php
 $sql_02 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."-02%'"; 
$data_02= mysqli_query($conn,$sql_02);        
while ($sum_02 = mysqli_fetch_assoc($data_02)) {
  if($sum_02['SUM(`total_money`)']){
    echo $sum_02['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
              <td class="bold"><?php
 $sql_03 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."-03%'"; 
$data_03= mysqli_query($conn,$sql_03);        
while ($sum_03 = mysqli_fetch_assoc($data_03)) {
  if($sum_03['SUM(`total_money`)']){
    echo $sum_03['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
              <td class="bold"> <?php
 $sql_04 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."-04%'"; 
$data_04= mysqli_query($conn,$sql_04);        
while ($sum_04 = mysqli_fetch_assoc($data_04)) {
  if($sum_04['SUM(`total_money`)']){
    echo $sum_04['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
              <td class="bold"><?php
 $sql_05 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."-05%'"; 
$data_05= mysqli_query($conn,$sql_05);        
while ($sum_05 = mysqli_fetch_assoc($data_05)) {
  if($sum_05['SUM(`total_money`)']){
    echo $sum_05['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
              <td class="bold"><?php
 $sql_06 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."-06%'"; 
$data_06= mysqli_query($conn,$sql_06);        
while ($sum_06 = mysqli_fetch_assoc($data_06)) {
  if($sum_06['SUM(`total_money`)']){
    echo $sum_06['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
              <td class="bold"><?php
 $sql_07 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."-07%'"; 
$data_07= mysqli_query($conn,$sql_07);        
while ($sum_07 = mysqli_fetch_assoc($data_07)) {
  if($sum_07['SUM(`total_money`)']){
    echo $sum_07['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
              <td class="bold"><?php
 $sql_08 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."-08%'"; 
$data_08= mysqli_query($conn,$sql_08);        
while ($sum_08 = mysqli_fetch_assoc($data_08)) {
  if($sum_08['SUM(`total_money`)']){
    echo $sum_08['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
              <td class="bold"><?php
 $sql_09 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."-09%'"; 
$data_09= mysqli_query($conn,$sql_09);        
while ($sum_09 = mysqli_fetch_assoc($data_09)) {
  if($sum_09['SUM(`total_money`)']){
    echo $sum_09['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
              <td class="bold"><?php
 $sql_10 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."-10%'"; 
$data_10= mysqli_query($conn,$sql_10);        
while ($sum_10 = mysqli_fetch_assoc($data_10)) {
  if($sum_10['SUM(`total_money`)']){
    echo $sum_10['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
              <td class="bold"><?php
 $sql_11 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."-11%'"; 
$data_11= mysqli_query($conn,$sql_11);        
while ($sum_11 = mysqli_fetch_assoc($data_11)) {
  if($sum_11['SUM(`total_money`)']){
    echo $sum_11['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
              <td class="bold"><?php
 $sql_12 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."-12%'"; 
$data_12= mysqli_query($conn,$sql_12);        
while ($sum_12 = mysqli_fetch_assoc($data_12)) {
  if($sum_12['SUM(`total_money`)']){
    echo $sum_12['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>
              <td class="bold bg-odd"><?php
 $sql_13 = "SELECT SUM(`total_money`) FROM `tblexpen` where  `date` like '%".$nam."%'"; 
$data_13= mysqli_query($conn,$sql_13);        
while ($sum_13 = mysqli_fetch_assoc($data_13)) {
  if($sum_13['SUM(`total_money`)']){
    echo $sum_13['SUM(`total_money`)']; 
  }else{
    echo "0.00";
  }
 
}
            ?> đ</td>                                                
              </tr>
            </tbody>
        </table>
         <ul class="pagination" id="pageNavPosition">
            
                  </ul>
                              </div>
                  </div>
               </div>
               <div class="col-md-7 small-table-right-col">
                  <div id="expense" class="hide">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="expense_convert_helper_modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo _l('additional_action_required'); ?></h4>
         </div>
         <div class="modal-body">
            <div class="radio radio-primary">
               <input type="radio" checked id="expense_convert_invoice_type_1" value="save_as_draft_false" name="expense_convert_invoice_type">
               <label for="expense_convert_invoice_type_1"><?php echo _l('convert'); ?></label>
            </div>
            <div class="radio radio-primary">
               <input type="radio" id="expense_convert_invoice_type_2" value="save_as_draft_true" name="expense_convert_invoice_type">
               <label for="expense_convert_invoice_type_2"><?php echo _l('convert_and_save_as_draft'); ?></label>
            </div>
            <div id="inc_field_wrapper">
               <hr />
               <p><?php echo _l('expense_include_additional_data_on_convert'); ?></p>
               <p><b><?php echo _l('expense_add_edit_description'); ?> +</b></p>
               <div class="checkbox checkbox-primary inc_note">
                  <input type="checkbox" id="inc_note">
                  <label for="inc_note"><?php echo _l('expense'); ?> <?php echo _l('expense_add_edit_note'); ?></label>
               </div>
               <div class="checkbox checkbox-primary inc_name">
                  <input type="checkbox" id="inc_name">
                  <label for="inc_name"><?php echo _l('expense'); ?> <?php echo _l('expense_name'); ?></label>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-info" id="expense_confirm_convert"><?php echo _l('confirm'); ?></button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<style type="text/css">
  .pg-normal {
       color: red;
       font-weight: normal;
       text-decoration: none;  
       cursor: pointer;  
}

.pg-selected {
       color: black;
       font-weight: bold;     
       text-decoration: underline;
       cursor: pointer;
}
</style>
<script type="text/javascript"><!--
       var pager = new Pager('expenses-report-table', 15);
       pager.init();
       pager.showPageNav('pager', 'pageNavPosition');
       pager.showPage(1);
//--></script>
<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail(); ?>
<script>
   Dropzone.autoDiscover = false;
   $(function(){
             // Expenses additional server params
             var Expenses_ServerParams = {};
             $.each($('._hidden_inputs._filters input'),function(){
               Expenses_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
             });
             initDataTable('.table-expenses', admin_url+'expenses/table', 'undefined', 'undefined', Expenses_ServerParams, <?php echo do_action('expenses_table_default_order',json_encode(array(5,'desc'))); ?>).column(0).visible(false, false).columns.adjust();

             init_expense();

             $('#expense_convert_helper_modal').on('show.bs.modal',function(){
                var emptyNote = $('#tab_expense').attr('data-empty-note');
                var emptyName = $('#tab_expense').attr('data-empty-name');
                if(emptyNote == '1' && emptyName == '1') {
                    $('#inc_field_wrapper').addClass('hide');
                } else {
                    $('#inc_field_wrapper').removeClass('hide');
                    emptyNote === '1' && $('.inc_note').addClass('hide') || $('.inc_note').removeClass('hide')
                    emptyName === '1' && $('.inc_name').addClass('hide') || $('.inc_name').removeClass('hide')
                }
             });

             $('body').on('click','#expense_confirm_convert',function(){
              var parameters = new Array();
              if($('input[name="expense_convert_invoice_type"]:checked').val() == 'save_as_draft_true'){
                parameters['save_as_draft'] = 'true';
              }
              parameters['include_name'] = $('#inc_name').prop('checked');
              parameters['include_note'] = $('#inc_note').prop('checked');
              window.location.href = buildUrl(admin_url+'expenses/convert_to_invoice/'+$('body').find('.expense_convert_btn').attr('data-id'), parameters);
            });
           });
</script>
</body>
</html>
