
    <style>
        @media print {
            th {
                background-color: #09093c !important;
                -webkit-print-color-adjust: exact;
            }

            .pay {
                background-color: #d4d4d4 !important;
                -webkit-print-color-adjust: exact;
            }
            #xuong {page-break-after: always;}
        }

    </style>

<?php
$conn = mysqli_connect("localhost", "root", "RedbeeX0605@", "lienviet");
mysqli_set_charset($conn,"utf8");
foreach ($id as $value1) {
$ids= $value1;
$sql = "SELECT * FROM `tblinvoices` where  `id`= $ids";
$sql_in = "SELECT * FROM `tbl_infor` order by `id` desc limit 1";
$md = mysqli_query($conn, $sql);
$md_in = mysqli_query($conn, $sql_in);
$data_in =mysqli_fetch_assoc($md_in);

while ($value = mysqli_fetch_assoc($md)) {


?>
    <main>
        <section class="container" style="margin: 20px 0px;">
            <div style="line-height: 5px;margin-bottom: 100px;margin-top: 50px;">
                <h3 style="font-weight: bold;text-align: right;"> HÓA ĐƠN</h3>
                <p style="text-align: right;">INV-<?php echo $value['number']; ?></p>
                <p style="text-align: right;">CHƯA THANH TOÁN</p>

            </div>
           <div style="width: 100%">
                <div class="col-md-6" style="width: 50%;float: left;">
                <address>
                        <h4><b><?php echo $data_in['company'] ?></b></h4><b>
                    <br>
                   <?php echo $data_in['address'] ?><br>
                    Mã số thuế: <?php echo $data_in['tax_code'] ?><br>
                    hotline: <?php echo $data_in['phone_number'] ?><br>
                    Email:<?php echo $data_in['email'] ?><br>
                  </b><p></p>

                                    </address>
            </div>
             <div class="col-md-6" style="width: 50%;float: right;">
                <p style="text-align: right;">Người nhận</p>
                <?php
                $cli = $value['clientid'];
                $sql_cl = "SELECT * FROM `tblclients` where `userid`=$cli ";
                $md_cli = mysqli_query($conn, $sql_cl);
                while ($value_cli = mysqli_fetch_assoc($md_cli)) {
                ?>
                <p style="text-align: right;margin-bottom: 30px;"><?php echo $value_cli['company']; ?></p>
                <p style="text-align: right;">Số điện thoại: <?php echo $value_cli['phonenumber']; ?></p>
                <p style="text-align: right;">Mã số thuế/cmnd: <?php echo $value_cli['vat']; ?></p>
                <p style="text-align: right;">Địa chỉ: <?php echo $value_cli['address']; ?></p>
            <?php }?>
                <p style="text-align: right;">Ngày xuất hóa đơn: <?php echo $value['datecreated']; ?></p>
                <p style="text-align: right;">Hạn trả: <?php echo $value['duedate']; ?></p>
            </div>
           </div>
           

            <div class="table1">
                <table style="width: 100%;border-collapse: collapse;">
                    <tr>
                        <th style="text-align: left;color:white;height: 50px;padding: 0 30px;background-color: #09093c;">#</th>
                        <th style="text-align: left;color:white;height: 50px;padding: 0 30px;background-color: #09093c;">Sản phẩm</th>
                        <th style="text-align: right;color:white;height: 50px;padding: 0 30px;background-color: #09093c;">Số lượng</th>
                        <th style="text-align: right;color:white;height: 50px;padding: 0 30px;background-color: #09093c;">Giá</th>
                        <th style="text-align: right;color:white;height: 50px;padding: 0 30px;background-color: #09093c;">Thuế</th>
                        <th style="text-align: right;color:white;height: 50px;padding: 0 30px;background-color: #09093c;">Tổng</th>
                    </tr>
<?php
$rel = $value['id'];
$sql_item = "SELECT * FROM `tblitems_in` where `rel_id`=$rel ";
$md_item = mysqli_query($conn, $sql_item);
$tt=0;
$tien = 0;
while ($value_item = mysqli_fetch_assoc($md_item)) {
    $tt++;
?>

                    <tr>
                        <td style="text-align: left;height: 40px;padding: 0 30px;"><?php echo $tt ?></td>
                        <td style="height: 40px;padding: 0 30px;"><?php echo $value_item['description']; ?></td>
                        <td style="text-align: right;height: 40px;padding: 0 30px;"><?php echo $value_item['qty']; ?></td>
                        <td style="text-align: right;height: 40px;padding: 0 30px;"><?php echo number_format($value_item['rate']); ?></td>
                        <?php
                        $itemid = $value_item['id'];

                        $sql_tax = "SELECT * FROM `tblitemstax` where `itemid`=$itemid and `rel_id`=$rel ";
                        $md_tax = mysqli_query($conn, $sql_tax);
                        $value_tax= mysqli_fetch_assoc($md_tax);
                        
                        if($value_tax){
                        $tien = $tien+($value_item['rate']*$value_item['qty']*0.1);
                           echo '<td>10 %</td>'; 
                       }else{
                        echo '<td>0 %</td>';
                       }
                        
                        ?>
                        <td style="text-align: right;height: 40px;padding: 0 30px;"><?php echo number_format($value_item['qty']*$value_item['rate']); ?></td>
                    </tr>
<?php } ?>               

                </table>

            </div>
            <div>
                <p style="text-align: right;"> <span style="margin-right: 70px;">Chưa qua thuế - Chi phí</span><?php echo number_format($value['subtotal']); ?>đ </p>
                <div class="pay" style="background-color: #d4d4d4;">
                    <p style="text-align: right;"> <span style="margin-right: 70px;">Thành tiền</span><?php echo number_format($value['total']); ?>đ </p>
                    <p style="text-align: right;"> <span style="margin-right: 70px;">VAT (10.00%)</span><?php echo number_format($tien); ?>đ </p>

                    <p style="text-align: right;"> <span style="margin-right: 70px;">Số lượng chưa trả</span><?php
                    $sql_rycod = "SELECT * FROM `tblinvoicepaymentrecords` where `invoiceid`=$rel";
                    $md_rycod = mysqli_query($conn, $sql_rycod);
                    $value_rycod= mysqli_fetch_assoc($md_rycod);
                    $tien_rycod = $value['total']-$value_rycod['amount'];
                     echo number_format($tien_rycod); ?>đ </p>
                </div>



            </div>

            <div style="margin-top: 50px;line-height: 5px;margin-bottom: 30px;">
                <p style="font-weight: bold;">Thanh toán ngoại tuyến</p>
                <p>Ngân Hàng</p>

            </div>
            <div>
                <p>Chữ ký ủy quyền ______________</p>
            </div>
        </section>


<div id="xuong"></div>
    </main>
<?php }
} ?>


