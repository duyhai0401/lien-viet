<?php init_head();?>

<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if (has_permission('expenses', '', 'create')) {?>
                  <a href="<?php echo admin_url('expenses/chiphi'); ?>" class="btn btn-info">Thêm mới chi phí</a>
                  <?php }?>
                 <!--  <?php $this->load->view('admin/expenses/filter_by_template');?>
                  <a href="#" onclick="slideToggle('#stats-top'); return false;" class="pull-right btn btn-default mleft5 btn-with-tooltip" data-toggle="tooltip" title="<?php echo _l('view_stats_tooltip'); ?>"><i class="fa fa-bar-chart"></i></a>
                  <a href="#" class="btn btn-default pull-right btn-with-tooltip toggle-small-view hidden-xs" onclick="toggle_small_view('.table-expenses','#expense'); return false;" data-toggle="tooltip" title="<?php echo _l('invoices_toggle_table_tooltip'); ?>"><i class="fa fa-angle-double-left"></i></a> -->
                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="expenses_total"></div>
                  </div>
                  <form style="float: right;" action="" id="invoice-form" class="_transaction_form invoice-form" method="get" accept-charset="utf-8" novalidate="novalidate">
                  <input type="hidden" name="csrf_token_name" value="20e0a3sa994ea7dd461520513c5608cc" >
                    <label>Loại Tiền</label>
                  <select class="selectpicker" name="currency" data-width="100%" data-none-selected-text="Không có mục nào được chọn" tabindex="-98">
                                                   <option <?php if ($_GET['currency'] == '1') {echo "selected";}?> value="1" selected="">VNĐ</option>
                                                      <option <?php if ($_GET['currency'] == '4') {echo "selected";}?> value="4">USD</option>
                                                      <option <?php if ($_GET['currency'] == '3') {echo "selected";}?> value="3">EUR</option>
                                                   </select>
                  <label>Chọn năm </label>
                    <div style="margin-bottom: 20px;">
                     <select class="selectpicker" name="nam">

                        <option id="2019" <?php if ($_GET['nam'] == '1') {echo "selected";}?> value="1">Tháng này</option>
                        <option id="2020" <?php if ($_GET['nam'] == '2') {echo "selected";}?> value="2">Tháng trước</option>
                        <option id="2021" <?php if ($_GET['nam'] == '3') {echo "selected";}?> value="3">Năm nay</option>
                        <option id="2022" <?php if ($_GET['nam'] == '4') {echo "selected";}?> value="4">Năm trước</option>
                        <option id="2023" <?php if ($_GET['nam'] == '5') {echo "selected";}?> value="5">Từ trước tới nay</option>

                      </select>
                    </div>
                    <button style="float: right;" class="btn-info btn">Xem</button>
                  </form>
               </div>
            </div>
            <div class="row">

               <div class="col-md-12" id="small-table">
                  <div class="panel_s">
                     <div class="panel-body">
                  <div class="relative chart-payment" >
     <div id="chartContainer" style="height: 400px; width: 100%;"></div>
   <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
   <?php
$conn = mysqli_connect("localhost", "root", "", "van_phong2");
$nam = $_GET['nam'];
if ($_GET['currency']) {
	$currency = $_GET['currency'];
} else {
	$currency = 1;
}

$date = getdate();
if ($nam == 1) {
	if ($date['mon'] < 10) {
		$mon = "0" . $date['mon'];
	}
	$time = $date['year'] . "-" . $mon;

}
if ($nam == 2) {

	$mon = $date['mon'] - 1;
	if ($mon < 10) {
		$mon1 = "0" . $mon;
	}
	$time = $date['year'] . "-" . $mon1;

}
if ($nam == 3) {
	$time = $date['year'];

}
if ($nam == 4) {
	$time = $date['year'] - 1;

}if ($nam == 5 || $nam == "") {
	$time = "";

}
?>

      <script>

window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
  animationEnabled: true,
  // width:900,
  theme:  "light1",//"light2", "light2", "dark1", "dark2"
  title:{
    text: ""
  },
  axisY: {
    title: ""
  },
  data: [{
    type: "column",
    showInLegend: true,
    legendMarkerColor: "grey",
    legendText: "",
    dataPoints: [
    <?php
$sql = "SELECT * FROM `tblstaff` where 1 and role = 1";
$md = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_assoc($md)) {
	$sql1 = "SELECT sum(`subtotal`) FROM `tblinvoices` where `sale_agent`= " . $row['staffid'] . " and `date` LIKE '%$time%' and `currency`=$currency ";
	$md1 = mysqli_query($conn, $sql1);
	while ($row1 = mysqli_fetch_assoc($md1)) {
		?>
      { y: <?php if ($row1['sum(`subtotal`)']) {echo $row1['sum(`subtotal`)'];} else {
			echo "0";
		}?>, label: "<?php echo $row['media_path_slug'] ?>" },
      <?php
}
}?>
    ]
  }]
});
chart.render();

}
</script>




  </div>

                              </div>
                  </div>
               </div>
               <div class="col-md-7 small-table-right-col">
                  <div id="expense" class="hide">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="expense_convert_helper_modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo _l('additional_action_required'); ?></h4>
         </div>
         <div class="modal-body">
            <div class="radio radio-primary">
               <input type="radio" checked id="expense_convert_invoice_type_1" value="save_as_draft_false" name="expense_convert_invoice_type">
               <label for="expense_convert_invoice_type_1"><?php echo _l('convert'); ?></label>
            </div>
            <div class="radio radio-primary">
               <input type="radio" id="expense_convert_invoice_type_2" value="save_as_draft_true" name="expense_convert_invoice_type">
               <label for="expense_convert_invoice_type_2"><?php echo _l('convert_and_save_as_draft'); ?></label>
            </div>
            <div id="inc_field_wrapper">
               <hr />
               <p><?php echo _l('expense_include_additional_data_on_convert'); ?></p>
               <p><b><?php echo _l('expense_add_edit_description'); ?> +</b></p>
               <div class="checkbox checkbox-primary inc_note">
                  <input type="checkbox" id="inc_note">
                  <label for="inc_note"><?php echo _l('expense'); ?> <?php echo _l('expense_add_edit_note'); ?></label>
               </div>
               <div class="checkbox checkbox-primary inc_name">
                  <input type="checkbox" id="inc_name">
                  <label for="inc_name"><?php echo _l('expense'); ?> <?php echo _l('expense_name'); ?></label>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-info" id="expense_confirm_convert"><?php echo _l('confirm'); ?></button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail();?>
<script>
   Dropzone.autoDiscover = false;
   $(function(){
             // Expenses additional server params
             var Expenses_ServerParams = {};
             $.each($('._hidden_inputs._filters input'),function(){
               Expenses_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
             });
             initDataTable('.table-expenses', admin_url+'expenses/table', 'undefined', 'undefined', Expenses_ServerParams, <?php echo do_action('expenses_table_default_order', json_encode(array(5, 'desc'))); ?>).column(0).visible(false, false).columns.adjust();

             init_expense();

             $('#expense_convert_helper_modal').on('show.bs.modal',function(){
                var emptyNote = $('#tab_expense').attr('data-empty-note');
                var emptyName = $('#tab_expense').attr('data-empty-name');
                if(emptyNote == '1' && emptyName == '1') {
                    $('#inc_field_wrapper').addClass('hide');
                } else {
                    $('#inc_field_wrapper').removeClass('hide');
                    emptyNote === '1' && $('.inc_note').addClass('hide') || $('.inc_note').removeClass('hide')
                    emptyName === '1' && $('.inc_name').addClass('hide') || $('.inc_name').removeClass('hide')
                }
             });

             $('body').on('click','#expense_confirm_convert',function(){
              var parameters = new Array();
              if($('input[name="expense_convert_invoice_type"]:checked').val() == 'save_as_draft_true'){
                parameters['save_as_draft'] = 'true';
              }
              parameters['include_name'] = $('#inc_name').prop('checked');
              parameters['include_note'] = $('#inc_note').prop('checked');
              window.location.href = buildUrl(admin_url+'expenses/convert_to_invoice/'+$('body').find('.expense_convert_btn').attr('data-id'), parameters);
            });
           });
</script>
</body>
</html>
