<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>HĐ_Sàn tầng 3 xuân thủy_Vũ Thị Trang_NVKD Diện</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
</head>

<body class="index">
    <style>
        .hidden{
        display: none;
    }
    #button{
        color: #fff;
    text-transform: uppercase;
    font-size: 13.5px;
    outline-offset: 0;
    border: 1px solid transparent;
    background-color: #03a9f4;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

    }
        .hd-container{width:1000px;margin:auto;padding:0 15px}.hd-row:after,.hd-row:before{content:"";display:table}.hd-row:after{clear:both}.hd-row .hd-logo{width:155px;float:left}.hd-row .hd-logo img{width:100%}.hd-row .hd-chx{text-align:center;float:left;width:838px}.hd-row .hd-chx .hd-ch{font-weight:400}.hd-row .hd-chx .hd-dl{font-weight:700;text-decoration:underline}.hd-row .hd-chx .hd-hn{text-align:right;padding-right:160px}.hd-row .hd-hdctvp,.hd-row .hd-hdtp{text-align:center}.hd-row .hd-hdctvp{margin:0}.hd-row .hd-hdtp{font-weight:700;font-size:14px}.hd-row .hd-styleText{font-style:italic;text-indent:30px}.hd-row .hd-flex{display:-webkit-box;display:-ms-flexbox;display:flex}.hd-row .text-right{text-align:right}.hd-row .text-left{text-align:left;padding-left:107px}.hd-row input{border:none;font-size:16px}
    </style>
<main id="main">
    <div class="hd-container" contenteditable="true">
        <div class="hd-row">
            <div class="hd-logo">
                <img src="../../../assets/images/logo.gif" alt="">
            </div>
            <div class="hd-chx">
                <h2 class="hd-ch">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h2>
                <p class="hd-dl">Độc lập – Tự do – Hạnh phúc</p>
                <p class="hd-hn">Hà Nội, ngày ........tháng........năm.....</p>
            </div>
        </div>
        <button id="button" onclick="myFunction()">In hợp đồng</button>
        <div class="hd-row">
            <h2 class="hd-hdctvp">
                HỢP ĐỒNG ĐẶT CỌC THUÊ VĂN PHÒNG
            </h2>
            <p class="hd-hdtp">(Số: ……./HĐ-LVHG)</p>
            <p class="hd-styleText">Hôm nay, ngày ….tháng .....năm ........., tại địa chỉ:P302 nhà
            </p>
            <p>
                Chúng tôi gồm:<br />
                <b>BÊN NHẬN ĐẶT CỌC (BÊN A):</b><br>
                <b>CÔNG TY CỔ PHẦN QUẢN LÝ BẤT ĐỘNG SẢN LIÊN VIỆT HOÀNG GIA</b>
            </p>
            <p>
                - Địa chỉ : Số 05, ngách 52, ngõ 255, đường Nguyễn Khang, phường Yên Hòa, quận Cầu Giấy, thành phố Hà Nội.
            </p>
            <p>
                - Mã số thuế : 0107483053 Ngày cấp: 23/06/2016 Nơi cấp: Sở KH đầu tư Hà Nội.</p>
            <p> - Điện thoại : 02432272549 – CSKH Hotline : 0898589669</p>
            <p> - Số tài khoản : 12610000899596 - Tên TK : Công ty cổ phần quản lý BĐS Liên Việt Hoàng Gia </p>
            <p>- Mở tại ngân hàng BIDV chi nhánh Ba Đình.</p>
            <p>- Đại diện là (Ông/bà): <b>Trần Văn Nam</b> - Chức vụ: Tổng Giám đốc</p>
            <p>- Số căn cước công dân: 035082001012</p>
            <p>- Nơi cấp: Cục CS ĐKQL cư trú và DLQG về Dân cư - Ngày cấp: 07/06/2016</p>
            <p>- Số Tài khoản : - BIDV: 12510000633317 - Ngân hàng BIDV chi nhánh Đông Đô, Hà Nội– Chủ TK : Trần Văn Nam;</p>
            <p>
                - Vietcom bank: 0021000447462 - Ngân hàng Vietcombank chi nhánh Hà Nội – Chủ TK : Trần Văn Nam.
            </p>
        </div>
        <div class="hd-row">
            <b>BÊN ĐẶT CỌC (BÊN B):</b>
            <div class="hd-flex">
                <p style="padding-right: 62px;">- Đại diện là: <b>..<?php echo $data[0]['company'] ?>...</b></p>
                <p> Điện thoại: <b>...<?php echo $data[0]['phonenumber'] ?>...</b></p>
            </div>
            <div class="hd-flex">
                <p style="padding-right: 62px;">
                    - Số CMND: <b>.<?php echo $data[0]['vat'] ?>..</b>
                </p>
                <p style="padding-right: 62px;">Cấp ngày: <b>............</b></p>
                <p> Tại: ...............</p>
            </div>
            <p>
                - Địa chỉ: <b>.....<?php echo $data[0]['address'] ?>.......</b>
            </p>
            <p class="hd-styleText">
                <b>Sau khi trao đổi, thỏa thuận, hai bên cùng nhau ký kết Hợp đồng đặt cọc này với nội dung như sau:
                </b>
            </p>
        </div>
        <div class="hd-row">
            <p><b> ĐIỀU 1: TIỀN ĐẶT CỌC, MỤC ĐÍCH & THANH TOÁN</b></p>
            <p><b>1.1.</b> Bên A đồng ý sẽ cho Bên B thuê ............................................................... do mình là đại diện chủ sở hữu tại địa chỉ :</p>
            <p> - Diện tích: ............... (m <sup>2</sup> )</p>
            <p>- Số phòng sử dụng:....................phòng;</p>
            <p> - Số phòng vệ sinh sử dụng: .........................phòng;
            </p>
            <p>- Các đặc điểm khác: hệ thống đèn chiếu sáng, điều hòa.</p>
            <p>
                <b>Với một số thỏa thuận cơ bản sẽ ký kết trong Hợp đồng thuê văn phòng như sau:</b>
            </p>
            <p>- Mục đích thuê để sử dụng làm: ...............................................................
            </p>
            <p>- Hợp đồng có thời hạn: <?php
                $date=date_create($data[0]['ngay_coc']);
                echo date_format($date,"d-m-Y");
                ?> đến ngày  <?php
                $date=date_create($data[0]['han_coc']);
                echo date_format($date,"d-m-Y");
                ?>.
            </p>
            <p>- Giá thuê: …<?php
                foreach ($item as $value) {
                if($value['iditem']>50){
                    echo number_format($value['rate']);
                }
                
                }
                ?>……đồng/tháng (Bằng chữ:
                đồng). Giá này sẽ được giữ nguyên trong ............năm. Và </p>
            <p>được điều chỉnh từ năm thứ 02 với mức tăng ....................................................
            </p>
            <p>- Chi phí khác:
            </p>
            <?php
                foreach ($item as $value) {
               
                    echo '<p>+ '.$value['namedv'].', giá tính theo giá đã thỏa thuận là: '.number_format($value['rate']).' VNĐ.</p>';
                
                }
                ?>
            <p>(<span style="font-style: italic;">Lưu ý</span>: Tất cả giá trên chưa bao gồm các khoản thuế VAT 10% và các khoản thuế, phí khác)
            </p>
            <p>- Phương thức thanh toán: ...................tháng/lần.
            </p>
            <p>- Hình thức thanh toán: Chuyển khoản.
            </p>
            <p><b>1.2.</b> Để bảo đảm việc ký kết Hợp đồng thuê văn phòng vào ngày: .........................<br>
                nay bên B đồng ý đặt cọc cho bên A một số tiền là:..<?php echo number_format($data[0]['tien_coc']) ?>... đồng (Bằng chữ:<br> .............................................................................................................) gọi là tiền đặt cọc.
            </p>
            <p>
                <b>1.3</b>.Mục đích đặt cọc: Bảo đảm thực hiện việc ký kết Hợp đồng thuê văn phòng
            </p>
            <p><b>1.4.</b> Thời gian đặt cọc: Ngay sau khi hai bên cùng ký Hợp đồng đặt cọc này.
            </p>
            <p><b>1.5.</b> Hình thức thanh toán: Chuyển khoản.
            </p>
            <p>
                <b>ĐIỀU 2: THỎA THUẬN VỀ VIỆC GIẢI QUYẾT TIỀN ĐẶT CỌC </b>
            </p>
            <p>- Bên B có trách nhiệm giao tiền đặt cọc cho bên A theo đúng thỏa thuận.</p>
            <p>- Nếu trong thời gian từ khi ký Hợp đồng cọc này đến hết ngày: ......./...../.............. (Là ngày ký Hợp đồng thuê phòng), tất cả các nội dung trong hợp đồng thuê văn phòng đã được thống nhất mà bên B không chủ động liên hệ để ký Hợp đồng thuê văn phòng thì bên B phải chịu mất toàn bộ số tiền đã đặt cọc.
            </p>
            <p>- Ngược lại, nếu đến hết ngày: ........./......./............(Là ngày ký Hợp đồng thuê phòng), các nội dung trong hợp đồng thuê văn phòng đã được thống nhất mà bên A không ký Hợp đồng thuê nhà thì bên A phải trả lại cho bên B toàn bộ số tiền đặt cọc đã nhận và bồi thường cho bên B số tiền tương ứng với số tiền đặt cọc đã nhận từ bên B (tính cả số tiền đã đặt cọc phải trả lại và bồi thường tổng cộng là: .......................đồng. (Bằng chữ: ......................................................................................................... ).
            </p>
            <p>
                <b>ĐIỀU 3: ĐIỀU KHOẢN CHUNG </b>
            </p>
            <p><b>3.1.</b> Hai bên xác định hoàn toàn tự nguyện khi giao kết Hợp đồng này. Những thông tin về nhân thân là hoàn toàn đúng sự thật và cam kết cùng nhau thực hiện nghiêm túc những điều đó thỏa thuận trong hợp đồng này
            </p>
            <p>Bên A cam kết thông tin về ngôi nhà đã ghi trong Hợp đồng này là đúng sự thật, ngôi nhà thuộc trường hợp được cho thuê theo quy định của pháp luật và tại thời điểm giao kết Hợp đồng này ngôi nhà không có tranh chấp, không bị kê biên để bảo đảm thi hành án.
            </p>
            <p><b>3.2.</b> Nếu phát sinh tranh chấp, các bên cùng nhau thương lượng giải quyết trên nguyên tắc hòa giải, cùng có lợi. Nếu không giải quyết được, thì một trong hai bên có quyền khởi kiện để yêu cầu Toà án có thẩm quyền giải quyết theo quy định của Pháp luật.
            </p>
            <p><b>3.3.</b> Hợp đồng này có hiệu lực kể từ khi hai bên cùng ký, hợp đồng có 03 trang được lập thành 03 bản, có giá trị như nhau, Bên A giữ 02 bản, Bên B giữ 01 bản. Hợp đồng chỉ có giá trị khi có đầy đủ chữ ký (và dấu nếu có) của hai bên. Sau khi đọc lại Hợp đồng, hai bên đã hiểu rõ quyền lợi, nghĩa vụ của mình và cùng ký tên dưới đây./.
            </p>
            <p>
                <span style="text-align: left;margin-left:100px"> <b>BÊN NHẬN ĐẶT CỌC (BÊN A)</b></span>
                <span style="text-align: right;float: right;margin-right: 100px;"> <b>BÊN ĐẶT CỌC (BÊN B)</b></span>
            </p>
        </div>
    </div>
</main>

    
    <!-- <script src="js/tool.min.js"></script>
    <script src="js/main.min.js"></script> -->
    
</body>
<script>
function myFunction() {
    var element = document.getElementById("button");
  element.classList.add("hidden");
  window.print();
element.classList.remove("hidden");
}
</script>
</html>