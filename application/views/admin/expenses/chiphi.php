<?php init_head();?>

<div id="wrapper">
   <div class="content">
   <div class="_buttons">
        <a href="#" class="btn btn-info pull-left" data-toggle="modal" data-target="#sales_item_modal">Thêm mới</a>

      </div>
      <div class="row" style="margin-top: 30px;">

<table style="background: white" class="table table-invoice-items dataTable no-footer dtr-inline" id="DataTables_Table_1" role="grid" aria-describedby="DataTables_Table_1_info"><thead><tr role="row">
  <th class="sorting_disabled not-export" rowspan="1" colspan="1" aria-label=" - "><span class="hide"> - </span><div class="checkbox mass_select_all_wrap"><input type="checkbox" id="mass_select_all" data-to-table="invoice-items"><label></label></div></th>
  <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Mô tả kích hoạt để sắp xếp từ trên xuống">Mô tả</th>
  <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Mô tả dài kích hoạt để sắp xếp từ dưới lên">Mô tả dài</th>
  <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Giá kích hoạt để sắp xếp từ dưới lên">Giá</th>

  <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Đơn vị kích hoạt để sắp xếp từ dưới lên">Đơn vị</th>

</tr></thead>
<tbody><tr class="has-row-options odd" role="row">
  <td tabindex="0"><div class="checkbox"><input type="checkbox" value="76"><label></label></div></td>
  <td class="sorting_1"><a href="#" data-toggle="modal" data-target="#sales_item_modal" data-id="76">31 THỌ THÁP</a><div class="row-options"><a href="#" data-toggle="modal" data-target="#sales_item_modal" data-id="76">Chỉnh sửa </a> | <a href="http://localhost/demo/admin/invoice_items/delete/76" class="text-danger _delete">Xóa </a></div></td>
  <td></td>
  <td>3</td>

  <td></td>
</tr>
<tr class="has-row-options even" role="row"><td tabindex="0"><div class="checkbox"><input type="checkbox" value="73"><label></label></div></td>
  <td class="sorting_1"><a href="#" data-toggle="modal" data-target="#sales_item_modal" data-id="73">HOÀNG QUỐC VIỆT</a><div class="row-options"><a href="#" data-toggle="modal" data-target="#sales_item_modal" data-id="73">Chỉnh sửa </a> | <a href="http://localhost/demo/admin/invoice_items/delete/73" class="text-danger _delete">Xóa </a></div></td>
  <td></td>
  <td>26800000</td>

  <td></td>
</tr>
<tr class="has-row-options odd" role="row">
  <td tabindex="0"><div class="checkbox"><input type="checkbox" value="75"><label></label></div></td>
  <td class="sorting_1"><a href="#" data-toggle="modal" data-target="#sales_item_modal" data-id="75">N8B15 HOÀNG MINH GIÁM 2</a><div class="row-options"><a href="#" data-toggle="modal" data-target="#sales_item_modal" data-id="75">Chỉnh sửa </a> | <a href="http://localhost/demo/admin/invoice_items/delete/75" class="text-danger _delete">Xóa </a></div></td>
  <td></td>
  <td>39000000</td>

  <td></td>

</tr>
<tr class="has-row-options even" role="row"><td tabindex="0"><div class="checkbox"><input type="checkbox" value="68"><label></label></div></td>
  <td class="sorting_1"><a href="#" data-toggle="modal" data-target="#sales_item_modal" data-id="68">N8B16 HOÀNG MINH GIÁM 1</a><div class="row-options"><a href="#" data-toggle="modal" data-target="#sales_item_modal" data-id="68">Chỉnh sửa </a> | <a href="http://localhost/demo/admin/invoice_items/delete/68" class="text-danger _delete">Xóa </a></div></td>
  <td></td>
  <td>36000000</td>

  <td></td>

</tr>
</tbody></table>
      </div>
      <div class="btn-bottom-pusher"></div>
   </div>
</div>
<div class="modal fade in" id="sales_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title hide">Chỉnh sửa</span>
                    <span class="add-title">Thêm mục mới</span>
                </h4>
            </div>
            <form action="http://localhost/demo/admin/invoice_items/manage" id="invoice_item_form" method="post" accept-charset="utf-8" novalidate="novalidate" class="dirty">
     <input type="hidden" name="csrf_token_name" value="ad0d33cf398dc98814c0066069e54197">

<input type="hidden" name="itemid" value="">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning affect-warning hide">
                            Chỉnh sửa thông tin sản phẩm không ảnh hưởng đến hóa đơn/báo giá/đề xuất đã tạo.                        </div>
                        <div class="form-group" app-field-wrapper="description"><label for="description" class="control-label"> <small class="req text-danger">* </small>Mô tả</label><input type="text" id="description" name="description" class="form-control" value=""></div>                        <div class="form-group" app-field-wrapper="long_description"><label for="long_description" class="control-label">Mô tả dài</label><textarea id="long_description" name="long_description" class="form-control" rows="4"></textarea></div>                        <div class="form-group">
                        <label for="rate" class="control-label"> <small class="req text-danger">* </small>
                            Tỷ lệ - VNĐ <small>(Base Currency)</small></label>
                            <input type="text" id="rate" name="rate" class="form-control" value="">
                        </div>
                                                        <div class="form-group">
                                    <label for="rate_currency_4" class="control-label">
                                        Tỷ lệ - USD</label>
                                        <input type="text" id="rate_currency_4" name="rate_currency_4" class="form-control" value="">
                                    </div>
                                                             <div class="form-group">
                                    <label for="rate_currency_3" class="control-label">
                                        Tỷ lệ - EUR</label>
                                        <input type="text" id="rate_currency_3" name="rate_currency_3" class="form-control" value="">
                                    </div>


                <div class="form-group" app-field-wrapper="unit"><label for="unit" class="control-label">Đơn vị</label><input type="text" id="unit" name="unit" class="form-control" value=""></div>                <div id="custom_fields_items">
                                    </div>


            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-info">Lưu lại</button>
            </div></form>
</div>
</div>
</div>
<?php init_tail();?>

</body>
</html>
