<?php init_head(); ?>
<style type="text/css">
   .red{
      border-color: red!important;
   }
</style>
<div id="wrapper">
 <div class="content">
  <div class="row">
   <div class="col-md-12">

    <div class="row">

     <div class="col-md-12" id="small-table">
      <div class="panel_s">
       <div class="panel-body">
        <div class="clearfix"></div>
        <div class="form_error">

            <?php 
              if(isset($error)&& $error!==""){
                ?>
          <div class="alert alert-danger"><?php echo $error;?></div>
                <?php
              }
           ?>
        </div>
        <form method="post" action="/admin/expenses/tien_coc">
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
          <div class="table-content col-md-6">
            <div>
            <input type="text" class="hidden" name="id_contract" value="<?php echo $id_contract; ?>">
              <label>Tòa nhà</label>
              <select onchange="group_in()" required name="floor" id="group" class="selectpicker _select_input_group" data-width="100%" data-none-selected-text="Không có mục nào được chọn" data-live-search="true" tabindex="-98">
                <option value=""></option>
                <?php foreach ($group as $value) {
                  if($contract['id_building']==$value['id']){
                   echo '<option selected value="'.$value['id'].'">'.$value['name'].'</option>';
                 }else{
                   echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                 }
               }

               ?>
             </select>
           </div>
           <div class="form-group" app-field-wrapper="subject">
             <label for="subject" class="control-label">Văn phòng</label>

             <select id="name_building" <?php 
             if ($id_contract=="") {echo "required";}
             ?> name="name_building"  class="form-control" onchange="myFunction_chage2()">
              <option value=""></option>


            </select>

            <label for="subject" class="control-label">Dịch vụ</label>
            <input type="text" name="group" id="name_gr" class="hidden">
            <select id="service" name="service" onchange="myFunction_chage()" class="selectpicker _select_input_group" data-width="100%" data-none-selected-text="Không có mục nào được chọn" data-live-search="true" tabindex="-98">
              <option value=""></option>
              <?php
              foreach ($dich_vu as $key => $value) {
                echo '<option value="' . $value['id'] . '">' . $value['description'] . '</option>';
              }
              ?>
            </select>

            <div>
              <table id="p" class="table">
                <tr>
                  <th>Tên</th>
                  <th>Giá</th>
                </tr>
                <?php
                if($im){
                  foreach ($im as $key => $value) {

                   echo "<tr>
                   <td><input type='text' readonly name='name_dv[]' value='".$value['namedv']."' class='form-control'/><input type='text' class='hidden' name='id_item[]' value='".$value['iditem']."' /><input type='text' class='hidden' name='iditem[]' value='".$value['id_coc_item']."' /></td>

                   <td><input type='text' name='rate_dv[]' class='rate' value='".number_format($value['rate'])."'  class='form-control'/></td>
                   <td></td><td><span onclick='delete_item(this,undefined); return false;' class='btn btn-danger pull-left'><i class='fa fa-trash'></i></span></td></tr>"; 
                 }

               }

               ?>
             </table>
           </div>
         </div>
         <div>
          <label>Địa chỉ tòa nhà</label>
          <input type="text" class="form-control" name="address" value="<?php echo $contract['adress'] ?>">
        </div>

        <div>
          <label>Tiền cọc</label>
          <input type="text" class="form-control" id="tien_coc" name="tien_coc" value="<?php echo number_format($contract['tien_coc'],2) ?>" onchange="set_num()" politespace data-grouplength="3" data-delimiter="," data-reverse >
        </div>
        <div class="row">
         <div class="col-md-6">

          <?php echo render_date_input('start_date', 'contract_start_date', $contract['ngay_coc']); ?>
           <div style="color: red;"><?php echo form_error('end_date');?></div>
        </div>
        <div class="col-md-6">
          <?php echo render_date_input('end_date', 'contract_end_date', $contract['han_coc']); ?>
        <div style="color: red;"><?php   echo form_error('start_date');
        ?></div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div>
       <div class="form-group select-placeholder"  id="client-form">
         <label for="clientid" class="control-label"><span class="text-danger"> </span><?php echo _l('contract_client_string'); ?></label><div class="input-group-addon" style="width: 10%;opacity: 1;float: right;"><a href="#" data-toggle="modal" data-target="#sales_item_modal"><i class="fa fa-plus"></i></a></div>
         <select id="clientid" name="client" required data-live-search="true" data-width="100%" class="ajax-search selectpicker _select_input_group" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
           <?php $selected = (isset($contract) ? $contract['khach_hang'] : '');
           if ($selected == '') {
            $selected = (isset($customer_id) ? $customer_id : '');
          }
          if ($selected != '') {
            $rel_data = get_relation_data('customer', $selected);
            $rel_val = get_relation_values($rel_data, 'customer');
            echo '<option value="' . $rel_val['id'] . '" selected>' . $rel_val['name'] . '</option>';
          }?>
        </select>

      </div>
    </div>
    <div>
      <label>Địa Chỉ</label>

      <textarea class="form-control" id="dia_chi" name="dia_chi">
        <?php echo $contract['dia_chi'] ?>
      </textarea>
    </div>
    <div>
      <label>Số điện thoại</label>
      <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $contract['phone'] ?>">
    </div>
    <div>
      <label>Số CMND/Mã Số Thuế</label>
      <input type="text" class="form-control" id="cmnd" name="cmnd" value="<?php echo $contract['cmnd'] ?>">
    </div>
    <?php
    $i = 0;
    $selected = 'selected';
    foreach ($staff as $member) {
     if (isset($contract)) {
      if ($contract['nguoi_ban'] == $member['staffid']) {
       $selected = $member['staffid'];
     }
   }
   $i++;
 }
 echo render_select('sale_agent', $staff, array('staffid', array('firstname', 'lastname')), 'sale_agent_string', $selected);
 ?>
 <div>
        <!-- <label>Trạng thái</label>
         <select class="selectpicker" data-width="100%" name="recurring" data-none-selected-text="Không có mục nào được chọn" tabindex="-98">
<option <?php if ($contract['trang_thai'] == 1) {
  echo "selected";
}
?> value="1">Đang hoạt động</option>
<option <?php if ($contract['trang_thai'] == 2) {
  echo "selected";
}
?> value="2">Không hoạt động</option>
<option <?php if ($contract['trang_thai'] == 3) {
  echo "selected";
}
?> value="3">Đang hoạt động và đã liên kết</option>
<option <?php if ($contract['trang_thai'] == 4) {
  echo "selected";
}
?> value="4">Không hoạt động và đã liên kết</option>
</select> -->
<label>Tên hợp đồng cọc</label>
<input type="text" required name="hopdong" class="form-control" value="<?php echo $contract['hopdong'] ?>">
</div>
</div>
<div class="col-md-12" style="text-align: right; margin-bottom: 20px;">
  <a <?php
  if($contract['id_coc']==''){
    echo 'style="display:none;"';
  }
  ?> class="btn-info btn" target="_blank"  href="../word_coc/<?php echo $contract['id_coc'] ?>">IN</a>
  <?php 
  // print_r( $contract['trang_thai']);die;
if( $contract['trang_thai']!='2'&&$contract['trang_thai']!='3'&&$contract['trang_thai']!='1'){
   ?>
  <button  class="btn-info btn">Lưu</button>
  <a <?php
  if($contract['id_coc']==''){
    echo 'style="display:none;"';
  }
  ?>  onclick="return confirm('Bạn có thực sự muốn chuyển hợp đồng cọc?');" href="../hd_chuyen/<?php echo $contract['id_coc'] ?>" class="btn-default btn khong">Chuyển Hợp Đồng</a>
  <?php
}
  ?>
  

</div>
</form>
<?php
if($contract['id_coc']!='' && $contract['trang_thai']!='2'&& $contract['trang_thai']!='3'&& sizeof($check)==0){
   ?>
<div class="col-md-12" >
  <?php $yes=base_url()."admin/expenses/huy_coc_co/".  $contract['id_coc']; ?>
  <a onclick="return confirm('Bạn có thực sự muốn hủy hợp đồng cọc?');" href=<?php echo $yes; ?> style="float: right;" class="btn-info btn co">Hủy cọc và trả lại tiền</a>
  <a style="float: right;background: coral;margin-right: 10px;" onclick="return confirm('Bạn có thực sự muốn hủy hợp đồng cọc?');" href=<?php echo base_url()."admin/expenses/huy_coc_khong/".  $contract['id_coc']; ?> class="btn-default btn khong">Hủy cọc và không trả lại tiền</a>
</div>
  <?php
}
  ?>

</div>
</div>
</div>

</div>
</div>
</div>
</div>
<div class="modal fade" id="sales_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
          <span class="edit-title"><?php echo _l('invoice_item_edit_heading'); ?></span>/
          <span class="add-title"><?php echo _l('invoice_item_add_heading'); ?></span>
        </h4>
      </div>

      <div class="modal-body">
         <div class="form-group">
          <label>Mã Khách hàng</label>
          <input type="text" id="ms" name="ms" value="" class="form-control">
        </div>
        <label>Tên khách Hàng</label>
        <input id="khach" type="text" name="client" class="form-control">
        <div class="form-group" app-field-wrapper="vat"><label for="vat" class="control-label">Số CMND/Thẻ căn cước</label><input type="text" id="vat" name="vat" class="form-control" value=""></div>
       
        <div class="form-group" app-field-wrapper="address"><label for="address" class="control-label">Địa chỉ</label><textarea id="address" name="address" class="form-control" rows="4"></textarea></div>
        <div class="form-group" app-field-wrapper="phonenumber"><label for="phonenumber" class="control-label">Số điện thoại</label><input type="text" id="phonenumber" name="phonenumber" class="form-control" value=""></div>
       
       <!--  <div class="form-group" app-field-wrapper="billing_street"><label for="billing_street" class="control-label">Địa chỉ</label><textarea id="billing_street" name="billing_street" class="form-control" rows="4"></textarea></div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
        <button type="submit" onclick="submit()" id="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
</div>
<?php init_tail(); ?>
<script src="../../assets/js/number.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

<script >
  $("#clientid").on("change", function() {
    ids = $("#clientid").val();
    requestGetJSON("expenses/get_client/" + ids).done(function (re) {
      console.log(re[0]);
      $("#dia_chi").val(re[0].address);
      $("#phone").val(re[0].phonenumber);
      $("#cmnd").val(re[0].vat);
    });

  });
  function set_num(){
    var a = $("#gia_tri").val();
    var number = numeral(a);
    var string = number.format('0,0');
    $("#gia_tri").val(string);
    var ba = $("#tien_coc").val();
    ba = Math.abs(ba);
    var number1 = numeral(ba);
    var string1 = number1.format('0,0');
    $("#tien_coc").val(string1);
  }
  function group_in(){
   var group = $('select[id="group"]').val();

   requestGetJSON("contracts/group_in/"+group).done(function (re) {
    var ten = re['invoi'];
    var dem = ten.length;
    option=[];
    option =['"<option value="">Chọn văn phòng</option>"'];
    for (var i = 0; i < dem; i++) {
      op = ten[i]['description'];
      option += "<option value="+ten[i]['id']+">"+ten[i]['description']+"</option>";
      $('#name_building').html(option);

    }


    
  });


 }
 function submit(){
var company = $("#khach").val();
var vat = $("#vat").val();
var ms = $("#ms").val();
var address = $("#address").val();
var phonenumber = $("#phonenumber").val();
var website = $("#website").val();
var billing_street = $("#billing_street").val();
if(company!=""&&phonenumber!=""){
   $("#khach").removeClass('red');
   $("#phonenumber").removeClass('red');
 $.ajax({
type: "POST",
url: admin_url+"/clients/create_khach",
data: {
company:company,
vat:vat,
ms:ms,
address:address,
phonenumber:phonenumber,
website:website,
billing_street:billing_street,
},
success: function(data){
// alert(data);
 var response=JSON.parse(data);
  if(response.success == true){
            alert_float('success',response.message);
            if($('body').hasClass('tien_coc') && typeof(response.id) != 'undefined') {
              // console.log('data'+response.id);
                var ctype = $('#clientid');
                ctype.append('<option value="'+response.id+'">'+response.name+'</option>');
                ctype.selectpicker('val',response.id);
                ctype.selectpicker('refresh');
            }
$("#sales_item_modal").modal('hide');

        }

},
error:function(data){
console.log(data);
}
});  
}else{
   $("#khach").addClass('red');
   $("#phonenumber").addClass('red');
}

}
function myFunction_chage(e){
   //alert(2);
   var id = $('select[name="service"]').val();

   requestGetJSON("expenses/get_item_dich_vu/" + id).done(function (re) {
    // console.log(re[0]);
var number = numeral(re[0].rate);
    var string = number.format('0,0');

    $("#p").append("<tr><td><input type='text' readonly name='name_dv[]' value='"+re[0].description+"' class='form-control'/><input type='text' name='id_item[]' value='"+re[0].id+"' class='hidden'/></td><td><input type='text' name='rate_dv[]' min='1' class='rate' value='"+string+"'  class='form-control'/></td><td><span onclick='delete_item(this,undefined); return false;' class='btn btn-danger pull-left'><i class='fa fa-trash'></i></span></td></tr>");
  });
 };
 function myFunction_chage2(e){
   //alert(2);
   var id = $('select[name="name_building"]').val();

   requestGetJSON("invoice_items/get_item_by_id/" + id).done(function (re) {
    console.log(re);

$('input[name="address"]').val(re.long_description);
    $("#name_gr").val(re.itemid);
    var number = numeral(re.rate);
    var string = number.format('0,0');
    $("#p").append("<tr><td><input type='text' readonly name='name_dv[]' value='"+re.description+"' class='form-control'/><input type='text' name='id_item[]' value='"+re.itemid+"' class='hidden'/></td><td><input type='text' name='rate_dv[]' min='1' class='rate' value='"+string+"'  class='form-control'/></td><td><span onclick='delete_item(this,undefined); return false;' class='btn btn-danger pull-left'><i class='fa fa-trash'></i></span></td></tr>");
  });
 };

 function delete_item(e, t) {
  $(e).parents("tr").addClass("animated fadeOut", function () {
    setTimeout(function () {
      $(e).parents("tr").remove(), calculate_total()
    }, 50)
  }), $('input[name="isedit"]').length > 0 && $("#removed-items").append(hidden_input("removed_items[]", t))
}

jQuery(document).trigger("enhance");
</script>


</body>
</html>
